# Makefile for building the libFLAC library
#

NAME                 =  flac
VERS_MAJOR           =  0
VERS_MINOR           =  1

CONFIG_PARAMS        =  --disable-ogg

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

BASE_LIBRARIES       =  

FLAC_ROOT            =  $(PROJECT_ROOT)/external/flac
LIBFLAC_ROOT         =  $(FLAC_ROOT)/src/libFLAC
LIBFLACPP_ROOT       =  $(FLAC_ROOT)/src/libFLAC++
THIS_MODULE         :=  $(PWD)

# Do not build anything by default: everything is overriden below:
export BUILD_LIB_A   =  0
export BUILD_LIB_SO  =  0

include $(SCRIPTDIR)/makelib

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

do_pre_compile: $(LIBFLACPP_ROOT)/Makefile
do_compile: $(BINDIR)/libflac.a $(BINDIR)/libflac++.a
clean: clean_flac

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(BINDIR)/libflac.a: $(LIBFLAC_ROOT)/.libs/libFLAC-static.a
	ln -sf $< $@

$(BINDIR)/libflac++.a: $(LIBFLACPP_ROOT)/.libs/libFLAC++-static.a
	ln -sf $< $@

$(LIBFLAC_ROOT)/.libs/libFLAC-static.a:
	@cd $(LIBFLAC_ROOT) || exit; \
	echo "    - Building FLAC (C) library..."; \
	echo "      Note: it may take long time, be patient!"; \
	$(MAKE) >$(THIS_MODULE)/flac-build.log 2>&1 || { \
		echo "Building libFLAC is failed:" >&2; \
		tail -25 $(THIS_MODULE)/flac-build.log; \
		echo "See the log file at '$(THIS_MODULE)/flac-build.log' for more details."; \
		exit 1; \
	}
	@cd $(LIBFLAC_ROOT)/.libs || exit; \
	$(AR) rcs $@ *.o

$(LIBFLACPP_ROOT)/.libs/libFLAC++-static.a: $(LIBFLAC_ROOT)/.libs/libFLAC-static.a
	@cd $(LIBFLACPP_ROOT) || exit; \
	echo "    - Building FLAC++ (C++) library..."; \
	echo "      Note: it may take long time, be patient!"; \
	$(MAKE) >$(THIS_MODULE)/flac++-build.log 2>&1 || { \
		echo "Building libFLAC++ is failed:" >&2; \
		tail -25 $(THIS_MODULE)/flac++-build.log; \
		echo "See the log file at '$(THIS_MODULE)/flac++-build.log' for more details."; \
		exit 1; \
	}
	@cd $(LIBFLACPP_ROOT)/.libs || exit; \
	$(AR) rcs $@ *.o

$(FLAC_ROOT)/configure:
	@cd $(FLAC_ROOT) || exit; \
	test -e Makefile && exit; \
	echo "    - Autogen FLAC library..."; \
	echo "      Note: it may take long time, be patient!"; \
	./autogen.sh >$(THIS_MODULE)/flac-autogen.log 2>&1 || { \
		echo "autogen failed:" >&2; \
		tail -25 $(THIS_MODULE)/flac-autogen.log >&2; \
		echo "See the log file at '$(THIS_MODULE)/flac-autogen.log' for more details."; \
		exit 1; \
	}

$(LIBFLACPP_ROOT)/Makefile: $(FLAC_ROOT)/configure
	@cd $(FLAC_ROOT) || exit; \
	echo "    - Configuring FLAC library..."; \
	echo "      Note: it may take long time, be patient!"; \
	./configure $(CONFIG_PARAMS) >$(THIS_MODULE)/flac-configure.log 2>&1 || { \
		echo "configure failed:" >&2; \
		tail -25 $(THIS_MODULE)/flac-autogen.log >&2; \
		echo "See the log file at '$(THIS_MODULE)/flac-configure.log' for more details."; \
		exit 1; \
	}

.PHONY: clean_flac
clean_flac:
	@rm -f $(BINDIR)/libflac.a $(BINDIR)/libflac++.a
	@echo "    - Cleaning FLAC library..."
	$(MAKE) -C $(LIBFLAC_ROOT) clean distclean >$(THIS_MODULE)/flac-clean.log 2>&1 || true
	$(MAKE) -C $(LIBFLACPP_ROOT) clean distclean >$(THIS_MODULE)/flac++-clean.log 2>&1 || true
	@rm -f $(FLAC_ROOT)/Makefile $(FLAC_ROOT)/configure $(FLAC_ROOT)/config.h* $(FLAC_ROOT)/*.log $(FLAC_ROOT)/Makefile.in
	@echo "      See the log files at '$(THIS_MODULE)/flac*-clean.log' for details."

