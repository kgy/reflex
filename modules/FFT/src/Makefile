# Makefile for building the FFT library
#

NAME                 =  fft
VERS_MAJOR           =  0
VERS_MINOR           =  1

CONFIG_PARAMS        =  --disable-doc --enable-float

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

BASE_LIBRARIES       =  

FFTW_ROOT            =  $(PROJECT_ROOT)/external/fftw
THIS_MODULE         :=  $(PWD)

# Do not build anything by default: everything is overriden below:
export BUILD_LIB_A   =  0
export BUILD_LIB_SO  =  0

include $(SCRIPTDIR)/makelib

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

do_pre_compile: $(FFTW_ROOT)/Makefile

do_compile: $(BINDIR)/fftw3f.a $(FFTW_ROOT)/Makefile

clean: clean_fftw

$(BINDIR)/fftw3f.a: $(FFTW_ROOT)/.libs/libfftw3.a
	cd $(FFTW_ROOT)/.libs; \
	ln -sf `pwd`/libfftw3f.a $(BINDIR)/fftw3f.a; \
	ln -sf fftw3f.a $(BINDIR)/fft.a

$(FFTW_ROOT)/.libs/libfftw3.a:
	@cd $(FFTW_ROOT) || exit; \
	echo "    - Building FFTW library..."; \
	echo "      Note: it may take long time, be patient!"; \
	$(MAKE) >$(THIS_MODULE)/fftw-build.log 2>&1 || { \
		echo "build failed:" >&2; \
		tail -25 $(THIS_MODULE)/fftw-build.log 2>&1; \
		echo "See the log file at '$(THIS_MODULE)/fftw-build.log' for more details."; \
		exit 1; \
	}

$(FFTW_ROOT)/Makefile:
	@cd $(FFTW_ROOT) || exit; \
	test -e Makefile && exit; \
	echo "    - Configuring FFTW library..."; \
	echo "      Note: it may take long time, be patient!"; \
	./configure $(CONFIG_PARAMS) >$(THIS_MODULE)/fftw-configure.log 2>&1 || { \
		echo "configure failed:" >&2; \
		tail -25 $(THIS_MODULE)/fftw-configure.log 2>&1; \
		echo "See the log file at '$(THIS_MODULE)/fftw-configure.log' for more details."; \
		exit 1; \
	}


.PHONY: clean_fftw
clean_fftw:
	@rm -f $(BINDIR)/fft.a $(BINDIR)/fftw3f.a
	@echo "    - Cleaning FFTW library..."
	$(MAKE) -C $(FFTW_ROOT) clean distclean >$(THIS_MODULE)/fftw-clean.log 2>&1 || true
	@rm -f $(FFTW_ROOT)/Makefile
	@echo "      See the log file at '$(THIS_MODULE)/fftw-clean.log' for details."

