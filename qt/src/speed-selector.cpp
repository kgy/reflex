/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "speed-selector.h"

#include <selector-interface.h>

#include <Base/Parser.h>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class SpeedSelectorBase:                                                      *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void SpeedSelectorBase::setSpeeds(const std::vector<unsigned int> & speeds)
{
 SYS_DEBUG_MEMBER(DM_QT);

 my_speeds = speeds;

 clear();

 addItem(QString("Maximum"), QVariant::fromValue(0U));

 for (auto i = speeds.begin(); i != speeds.end(); ++i) {
    addItem(QString("%1").arg(*i), QVariant::fromValue(*i));
 }
}

unsigned int SpeedSelectorBase::getSelectedRate(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 const char * text = currentText().toUtf8().data();

 if (strcmp(text, "Maximum")) {
    return (unsigned int)Parser::StrtolSafe(text);
 }

 // Get the maximum speed:
 unsigned int max = 0;
 for (auto i = my_speeds.begin(); i != my_speeds.end(); ++i) {
    if (max < *i) {
        max = *i;
    }
 }

 return max;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class InputSpeedSelector:                                                     *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

InputSpeedSelector::InputSpeedSelector(SpeedSelectorInterface & parent):
    SpeedSelectorBase(parent, "/HW/soundcard/input/speed")
{
 SYS_DEBUG_MEMBER(DM_QT);

 connect(this, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int index)
 {
    parent.InputSpeedSelected(itemData(index).value<unsigned int>());
 });
}

InputSpeedSelector::~InputSpeedSelector()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void InputSpeedSelector::ConfigLoaded(const char *, const ConfExpression & value)
{
 SYS_DEBUG_MEMBER(DM_QT);

 const char * conf = value.GetString().c_str();
 setCurrentText(conf);
}

void InputSpeedSelector::SaveConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 SaveConfigValue(currentText().toUtf8().data());
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class OutputSpeedSelector:                                                    *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

OutputSpeedSelector::OutputSpeedSelector(SpeedSelectorInterface & parent):
    SpeedSelectorBase(parent, "/HW/soundcard/output/speed")
{
 SYS_DEBUG_MEMBER(DM_QT);

 connect(this, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int index)
 {
    parent.OutputSpeedSelected(itemData(index).value<unsigned int>());
 });
}

OutputSpeedSelector::~OutputSpeedSelector()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void OutputSpeedSelector::ConfigLoaded(const char *, const ConfExpression & value)
{
 SYS_DEBUG_MEMBER(DM_QT);

 const char * conf = value.GetString().c_str();
 setCurrentText(conf);
}

void OutputSpeedSelector::SaveConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 SaveConfigValue(currentText().toUtf8().data());
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
