/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_MY_APP_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_MY_APP_H_INCLUDED__

#include <QApplication>

class MyApplication: public QApplication
{
    Q_OBJECT

 public:
    MyApplication(int argc, char ** argv):
        QApplication(argc, argv)
    {
    }

 signals:
    void initMessage(const std::string & s);

}; // class QApplication

#endif /* __SPEAKER_TEST_QT_SRC_MY_APP_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
