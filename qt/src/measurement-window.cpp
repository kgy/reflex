/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measurement-window.h"

#include <main-window.h>
#include <measure-response-fft.h>
#include <measures/analyze-base.h>
#include <measures/mouse-info.h>
#include <start-stop-widget.h>

#include <QVBoxLayout>
#include <QWidget>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MyMeasurementWindow:                                                      *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MyMeasurementWindow::MyMeasurementWindow(MainWindow & parent):
    MeasureController(parent),
    parent(parent),
    start_stop_widget(new StartStopWidget(*this))
{
 SYS_DEBUG_MEMBER(DM_QT);

 QVBoxLayout * layout = new QVBoxLayout;
 layout->setMargin(5);

 layout->addLayout(start_stop_widget);
 layout->setStretchFactor(start_stop_widget, 0);

 {
    auto f = [=](PLOT::AnalyzeBase & mb) {
        MyMeasurementWidget * plot = new MyMeasurementWidget(mb, *this);
        mb.addWindow(plot);
        layout->addWidget(plot);
        layout->setStretchFactor(plot, 1);
    };
    PLOT::AnalyzeBase::Scan(f); // Assigns MeasurementWidget instance for each existing measurement type
 }

 setLayout(layout);
}

MyMeasurementWindow::~MyMeasurementWindow()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MyMeasurementWindow::leave(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 stopButton();
}

void MyMeasurementWindow::enter(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 start_stop_widget->setFocus();
}

void MyMeasurementWindow::HardwareTypeSelected(MeasurementTypes /*type*/)
{
 SYS_DEBUG_MEMBER(DM_QT);

}

void MyMeasurementWindow::MeasureStarted(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 // The measure is just started
}

void MyMeasurementWindow::MeasureStopped(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 start_stop_widget->Stopped();
 MeasureStop();
}

SND::DeviceInfo & MyMeasurementWindow::getDeviceInfo(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 return parent.getDeviceInfo();
}

GENERATOR::GeneratorType MyMeasurementWindow::getGeneratorType(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 if (!start_stop_widget->isGeneratorNeeded()) {
    return GENERATOR::GENERATOR_EXTERNAL;
 }

 if (!measure_analyzer) {
    return GENERATOR::GENERATOR_NONE;
 }

 return measure_analyzer->getGeneratorType();
}

bool MyMeasurementWindow::startButton(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 bool succeeded = MeasureStart(false);

 if (!succeeded) {
    stopButton();
 }

 return succeeded;
}

void MyMeasurementWindow::stopButton(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 MeasureStop();
}

void MyMeasurementWindow::ProcessInput(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[])
{
 measure_analyzer->DoMeasure(data, p, channel_map);
}

void MyMeasurementWindow::CalculateResult(PLOT::PlotData & p, unsigned int sampling_rate)
{
 measure_analyzer->MeasurementResult(p, sampling_rate);
}

void MyMeasurementWindow::externalException(const char * module, const char * message)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.externalException(module, message);
}

float MyMeasurementWindow::getGeneratorLevel(void) const
{
 return parent.getGeneratorLevel();
}

const PLOT::Params * MyMeasurementWindow::getPlotParameters(void) const
{
 return measure_widget ? &measure_widget->getPlotParameters() : nullptr;
}

std::string MyMeasurementWindow::getInputLoadFileName(void) const
{
 return start_stop_widget->getInputLoadFileName();
}

std::string MyMeasurementWindow::getInputSaveFileName(void) const
{
 return start_stop_widget->getInputSaveFileName();
}

std::string MyMeasurementWindow::getOutputLoadFileName(void) const
{
 return start_stop_widget->getOutputLoadFileName();
}

std::string MyMeasurementWindow::getOutputSaveFileName(void) const
{
 return start_stop_widget->getOutputSaveFileName();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MyMeasurementWindow::MyMeasurementWidget:                                 *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MyMeasurementWindow::MyMeasurementWidget::MyMeasurementWidget(PLOT::AnalyzeBase & mb, MeasureController & parent):
    MeasurementWidget(mb, parent)
{
 SYS_DEBUG_MEMBER(DM_QT);

 QVBoxLayout * layout = new QVBoxLayout;

 switch (mb.getPlotParameters().type) {
    case MEASURE_WINDOW_SPEAKER:
        // Note: Speaker Measurement does not have any filters
    break;

    case MEASURE_WINDOW_AMPLIFIER:
    {
        FilterSelectorAmplifier * filter = new FilterSelectorAmplifier(this);
        layout->addLayout(filter);
        layout->setStretchFactor(filter, 0);
    }
    break;

    default:
        // Unhandled window type: do nothing
        return;
    break;
 }

 Initialize(layout);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
