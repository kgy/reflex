/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_MEASURE_MEASURE_CONTROLLER_H_INCLUDED__
#define __QT_SRC_MEASURE_MEASURE_CONTROLLER_H_INCLUDED__

#include <measure.h>
#include <measures/params.h>

#include <QWidget>

namespace PLOT
{
    class AnalyzeBase;
}

struct MouseInfo;
class MainWindow;
class MeasurementWidget;

class MeasureController: public QWidget, public MeasureInterface
{
 public:
    virtual ~MeasureController();

    void SelectType(PLOT::AnalyzeBase * analyzer, MeasurementWidget * widget);
    void Replot(void);
    void cursorMove(double freq, const MouseInfo & info);

 protected:
    MeasureController(MainWindow & parent, QWidget * w = nullptr);

    virtual void ProcessInput(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[]) =0;
    virtual void CalculateResult(PLOT::PlotData & p, unsigned int sampling_rate) =0;

    virtual void calculateAudioFrame(const FFT::FFTBuffer & data, const int * channel_map) override;
    virtual std::string getInputLoadFileName(void) const override;
    virtual std::string getInputSaveFileName(void) const override;
    virtual std::string getOutputLoadFileName(void) const override;
    virtual std::string getOutputSaveFileName(void) const override;
    virtual bool MeasureStart(bool is_calibration) override;
    virtual void MeasureStop(void) override;

    virtual const PLOT::Params * getPlotParameters(void) const override
    {
        return plot_parameters;
    }

    virtual void MeasurementTypeSelected(void)  // Signals the type selection
    {
    }

    inline void setOutputLevel(float level)
    {
        if (actual_measure) {
            actual_measure->setOutputLevel(level);
        }
    }

    MainWindow & parent;

    PLOT::AnalyzeBase * measure_analyzer;

    MeasurementWidget * measure_widget;

    const PLOT::Params * plot_parameters;

    MeasurePtr actual_measure;

    PLOT::PlotDataPtr actual_plot;

    unsigned int data_rate;

    double bandwidth;

 private:
    SYS_DEFINE_CLASS_NAME("MeasureController");

}; // class MeasureController

#endif /* __QT_SRC_MEASURE_MEASURE_CONTROLLER_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
