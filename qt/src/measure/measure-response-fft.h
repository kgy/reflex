/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_MEASURE_MEASURE_RESPONSE_FFT_H_INCLUDED__
#define __QT_SRC_MEASURE_MEASURE_RESPONSE_FFT_H_INCLUDED__

#include <measure.h>
#include <main-window.h>
#include <fft-base.h>

/// This class measures the frequency response
/*! Generates white noise and measures the spectrum of the response. */
class MeasureResponseFFT: public Measure
{
 public:
    MeasureResponseFFT(MeasureInterface & parent);
    virtual ~MeasureResponseFFT();

 protected:
    MeasureInterface & parent;

    FFT::FFTBuffer data;

    virtual void MeasureStarted(void) override;
    virtual void MeasureStopped(void) override;
    virtual bool frameCaptured(SND::SoundFrameBufferPtr & buf) override;
    virtual void except(const char * module, const char * reason) override;

 private:
    SYS_DEFINE_CLASS_NAME("MeasureResponseFFT");

}; // class MeasureResponseFFT

#endif /* __QT_SRC_MEASURE_MEASURE_RESPONSE_FFT_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
