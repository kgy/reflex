/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see File 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measure-controller.h"

#include <measurement-widget.h>
#include <measures/analyze-base.h>
#include <measure-response-fft.h>
#include <measures/mouse-info.h>

MeasureController::MeasureController(MainWindow & parent, QWidget * w):
    QWidget(w),
    parent(parent),
    measure_analyzer(nullptr),
    measure_widget(nullptr),
    plot_parameters(nullptr),
    data_rate(0),
    bandwidth(0.0)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

MeasureController::~MeasureController()
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 MeasureStop();
}

void MeasureController::cursorMove(double freq, const MouseInfo & info)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (!measure_widget) {
    return;
 }

 if (info.section != MouseInfo::DRAW_SECTION_DIAGRAMS) {
    return;
 }

 auto & params = measure_widget->getPlotParameters();

 std::string y_labels[params.plots];

 if (actual_plot) {
    for (unsigned int i = 0; i < params.plots; ++i) {
        y_labels[i] = actual_plot->getLabel(i, freq, params.plot[i].unit);
    }
 } else {
    for (unsigned int i = 0; i < params.plots; ++i) {
        y_labels[i] = "--";
    }
 }

 char xlabel[40];
 if (freq >= 10000.0) {
    snprintf(xlabel, sizeof(xlabel), "%.3f kHz", freq/1000.0);
 } else if (freq >= 1000.0) {
    snprintf(xlabel, sizeof(xlabel), "%.4f kHz", freq/1000.0);
 } else if (freq >= 100.0) {
    snprintf(xlabel, sizeof(xlabel), "%.2f Hz", freq);
 } else {
    snprintf(xlabel, sizeof(xlabel), "%.3f Hz", freq);
 }

 measure_widget->updateLabels(xlabel, y_labels, !actual_measure);
}

void MeasureController::SelectType(PLOT::AnalyzeBase * analyzer, MeasurementWidget * widget)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 measure_analyzer = analyzer;
 measure_widget = widget;
 plot_parameters = &analyzer->getPlotParameters();

 MeasurementTypeSelected();
}

bool MeasureController::MeasureStart(bool is_calibration)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 SND::DeviceInfo & parameters = getDeviceInfo();

 if (!parameters.generator.device || !parameters.recorder.device || !measure_analyzer) {
    return false;
 }

 actual_measure.reset();    // Delete previous measurement (if any)
 actual_plot.reset();       // ...also for the plot data

 parent.ClearException();   // Allow the next exception to be displayed

 std::string msg;

 try {
    measure_analyzer->Start(is_calibration);
    actual_measure.reset(new MeasureResponseFFT(*this));
    actual_measure->Start();
    return true;

 } catch (std::exception & ex) {
    msg = ex.what();
 } catch (...) {
    msg = "unknown exception";
 }

 externalException("MeasureController", msg.c_str());

 return false;
}

void MeasureController::MeasureStop(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 MeasurePtr p;
 p.swap(actual_measure);
 if (p) {
    SYS_DEBUG(DL_INFO3, "stopping measurement...");
    p->Stop();
 }
}

void MeasureController::Replot(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (actual_measure || !data_rate || !measure_widget || !actual_plot) {
    return;
 }

 CalculateResult(*actual_plot, data_rate);

 if (measure_widget) {
    measure_widget->Replot(actual_plot);
 }
}

/*! \note   This function is called from the sound frame analyzer thread. */
void MeasureController::calculateAudioFrame(const FFT::FFTBuffer & data, const int * channel_map)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (!measure_analyzer || !plot_parameters || !actual_measure) {
    return;
 }

 data_rate = data.rate();
 bandwidth = (double)(data_rate / 2);

 std::string error_message;

 const char * calling = "unknown";

 try {
    unsigned int FFTSize = data.samples() / 2;

    if (!actual_plot) {
        actual_plot = std::make_shared<PLOT::PlotData>(plot_parameters->plots, FFTSize);
    }

    calling = "ProcessInput";
    ProcessInput(data, *actual_plot, channel_map);
    calling = "FillX";
    measure_analyzer->FillX(actual_plot->x, FFTSize, bandwidth);
    calling = "CalculateResult";
    CalculateResult(*actual_plot, data_rate);

    if (measure_widget) {
        calling = "Replot";
        measure_widget->Replot(actual_plot);    // Call to the graphic thread only for drawing
    }

    return; // Measurement process is ready

 } catch (std::exception & ex) {
    std::cerr << "Error occured in " << __FUNCTION__ << "() calling " << calling << "(): " << ex.what() << std::endl;
    error_message = ex.what();
 } catch (...) {
    std::cerr << "Unknown error occured in " << __FUNCTION__ << std::endl;
    error_message = "unknown exception";
 }

 externalException("measurement", error_message.c_str());
}

/* Some default values: */

std::string MeasureController::getInputLoadFileName(void) const
{
    return "";
}

std::string MeasureController::getInputSaveFileName(void) const
{
    return "";
}

std::string MeasureController::getOutputLoadFileName(void) const
{
    return "";
}

std::string MeasureController::getOutputSaveFileName(void) const
{
    return "";
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
