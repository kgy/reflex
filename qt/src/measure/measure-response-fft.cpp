/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measure-response-fft.h"

#include <math.h>

MeasureResponseFFT::MeasureResponseFFT(MeasureInterface & parent):
    Measure(parent),
    parent(parent)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

MeasureResponseFFT::~MeasureResponseFFT()
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

void MeasureResponseFFT::MeasureStarted(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.MeasureStarted();
}

void MeasureResponseFFT::MeasureStopped(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.MeasureStopped();
}

/*! This function is called from the analyzer thread (\ref SND::RecorderBase::analyzer_thread).<br>
    \note   The analyzer thread is independent from the recorder, and has its own fifo, so this
            function can wait for the process completion.
 */
bool MeasureResponseFFT::frameCaptured(SND::SoundFrameBufferPtr & buf)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 SND::SoundFrameBufferPtr p;
 p.swap(buf);
 data.push_back(p);

 if (data.Trim(16)) {
    parent.calculateAudioFrame(data, caller.getDeviceInfo().recorder.channels);  // noexcept
 }

 return true;
}

void MeasureResponseFFT::except(const char * module, const char * reason)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.externalException(module, reason);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
