/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "main.h"
#include "main-window.h"
#include "my-app.h"

#include <Exceptions/Exceptions.h>
#include <Debug/Debug.h>

#include <QSplashScreen>

#include <xpm/splash.xpm>

SYS_DEFINE_MODULE(DM_QT);
SYS_DECLARE_MODULE(DM_MEASURE_AMP);

static inline void init_debug_system(void)
{
 // SYS_DEBUG_ALL_MODULES_ON;
 SYS_DEBUG_MODULE_ON(DM_MEASURE_AMP);
 SYS_DEBUGLEVEL(DL_ALL);
}

int main(int argc, char **argv)
{
 init_debug_system();

 SYS_DEBUG_FUNCTION(DM_QT);

 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

 std::cout << "Welcome to Refleks - Speaker Measurement Software" << std::endl;

 try {
    MyApplication app(argc, argv);

    QCoreApplication::setOrganizationName("Name");
    QCoreApplication::setApplicationName("Refleks");
    QCoreApplication::setApplicationVersion("0.1");

    QPixmap splash_pixmap(splash_xpm);
    QSplashScreen splash(splash_pixmap);
    splash.show();
    app.processEvents();

    QObject::connect(&app, &MyApplication::initMessage, [&](const std::string & message) {
        splash.showMessage(QString("Scanning: %1").arg(message.c_str()), Qt::AlignBottom, QColor(20, 90, 15));
    });

    MainWindow mainWin(app);

    mainWin.LoadConfig();
    mainWin.show();
    splash.finish(&mainWin);
    int retval = app.exec();
    mainWin.SaveConfig();

    return retval;

 } catch (EX::BaseException & ex) {
    std::cerr << "ERROR: got internal exception: " << ex.what() << std::endl;
 } catch (std::exception & ex) {
    std::cerr << "ERROR: got standard exception: " << ex.what() << std::endl;
 } catch (...) {
    std::cerr << "ERROR: got unknown exception." << std::endl;
 }

 return 1;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
