/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_MEASUREMENT_WIDGET_H_INCLUDED__
#define __QT_SRC_MEASUREMENT_WIDGET_H_INCLUDED__

#include <measures/params.h>
#include <measures/analyze-base.h>
#include <fft-base.h>
#include <my-customplot.h>
#include <filter-selector.h>

#include <QWidget>

class MeasureController;

class MeasurementWidget: public QWidget, public PLOT::Window, public PLOT::MeasureWidgetInterface
{
 public:
    MeasurementWidget(PLOT::AnalyzeBase & mb, MeasureController & parent);
    virtual ~MeasurementWidget();

    inline const PLOT::Params & getPlotParameters(void) const
    {
        return mb.getPlotParameters();
    }

    inline void Replot(PLOT::PlotDataPtr & p)
    {
        myplot->Replot(p);
    }

    inline void updateLabels(const char * xLabel, const std::string yLabels[], bool update)
    {
        myplot->updateLabels(xLabel, yLabels, update);
    }

    virtual void On(PLOT::AnalyzeBase * measure, bool isOn) override;

 protected:
    PLOT::AnalyzeBase & mb;

    MeasureController & parent;

    PLOT::MyCustomPlot * myplot;

    void Initialize(QVBoxLayout * layout = new QVBoxLayout);

 private:
    SYS_DEFINE_CLASS_NAME("MeasurementWidget");

    virtual void cursorMove(double freq, const MouseInfo & info) override;
    virtual void setFilterType(FILTER::FilterTypes t) override;
    virtual void setAlignmentMode(FILTER::AlignmentModes m) override;
    virtual void setSmoothFactor(int s) override;
    virtual void setGeneratorType(GENERATOR::GeneratorType t) override;
    virtual void setFFTMode(bool mode) override;

}; // class MeasurementWidget

#endif /* __QT_SRC_MEASUREMENT_WIDGET_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
