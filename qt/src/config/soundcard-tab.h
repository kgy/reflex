/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_SOUNDCARD_TAB_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_SOUNDCARD_TAB_H_INCLUDED__

#include <scene-view.h>

#include <Debug/Debug.h>

#include <QTabWidget>

SYS_DECLARE_MODULE(DM_CONFIGSCENE);

class ConfigScene;
class AudioSocket;

class SoundcardTab: public QTabWidget, public SceneView
{
 public:
    SoundcardTab(ConfigScene & parent);
    virtual ~SoundcardTab();

    void setPlayerChannels(unsigned int channels);
    void setRecorderChannels(unsigned int channels);
    AudioSocket * getPlayerSocket(int index);
    AudioSocket * getRecorderSocket(int index);
    AudioSocket * getNearestSocket(qreal x, qreal y, bool is_input);
    void SaveConfig();

 protected:
    void LoadConfig();

    ConfigScene & parent;

    struct {
        AudioSocket * players[6];

        AudioSocket * recorders[6];

    } sockets;

 private:
    SYS_DEFINE_CLASS_NAME("SoundcardTab");

}; // class SoundcardTab

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_SOUNDCARD_TAB_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
