/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_WINDOW_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_WINDOW_H_INCLUDED__

#include <sound-io-connection.h>
#include <selector-interface.h>
#include <device-interface.h>

#include <Debug/Debug.h>

#include <QWidget>

class ConfigScene;
class MainWindow;
class JackWire;

class MyConfigWindow: public QWidget, public SelectorInterface, public SND::DeviceCaller
{
 public:
    MyConfigWindow(MainWindow & parent);
    virtual ~MyConfigWindow();

    void LoadConfig(void);
    void SaveConfig(void);
    void UpdateChannelTable(void);
    JackWire * getJack(int index);
    const JackWire * getJack(int index) const;
    void externalExceptionSignal(const char * module, const char * message);
    void HardwareTypeSelected(MeasurementTypes type);
    void enter(void);
    void leave(void);

 protected:
    MainWindow & parent;

    SND::DeviceInfo & parameters;

    ConfigScene * config_scene;

    InputSelector * in_dev;
    InputSpeedSelector * in_speed_select;
    OutputSelector * out_dev;
    OutputSpeedSelector * out_speed_select;

    Auton<SND::DeviceHandler> dev;

    virtual void deviceScanning(const std::string & name) override;
    virtual void highlightedDevice(const SND::Device * dev) override;
    virtual void selectedPlayerDevice(SND::Device * dev) override;
    virtual void selectedRecorderDevice(SND::Device * dev) override;
    virtual void InputSpeedSelected(unsigned int speed) override;
    virtual void OutputSpeedSelected(unsigned int speed) override;

 private:
    SYS_DEFINE_CLASS_NAME("MyConfigWindow");

    void PlayerDeviceSpeedRates(const std::vector<unsigned int> & rates);
    void RecorderDeviceSpeedRates(const std::vector<unsigned int> & rates);
    void PlayerDeviceMaxChannels(unsigned int channels);
    void RecorderDeviceMaxChannels(unsigned int channels);

}; // class MyConfigWindow

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_WINDOW_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
