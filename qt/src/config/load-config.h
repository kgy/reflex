/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_LOAD_CONFIG_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_LOAD_CONFIG_H_INCLUDED__

#include <Config/MainConfig.h>
#include <Debug/Debug.h>

#include <string>

SYS_DECLARE_MODULE(DM_LOADCONFIG);

class ConfigLoader
{
 public:
    ConfigLoader(const char * config_path);
    virtual ~ConfigLoader();

    void LoadConfig(void);
    void SaveConfigValue(const char * value);
    void SaveConfigValue(int value);
    void SaveConfigValue(float value);
    void SaveConfigValue(double value);

 protected:
    std::string config_path;

    virtual void ConfigLoaded(const char * key, const ConfExpression & value) =0;

 private:
    SYS_DEFINE_CLASS_NAME("ConfigLoader");

}; // class ConfigLoader

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_LOAD_CONFIG_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
