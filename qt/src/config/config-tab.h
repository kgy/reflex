/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_TAB_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_TAB_H_INCLUDED__

#include <scene-view.h>

#include <sound-io-connection.h>
#include <Debug/Debug.h>

#include <QTabWidget>

#include <vector>

SYS_DECLARE_MODULE(DM_CONFIGSCENE);

class ConfigScene;
class JackWire;
class AudioSocket;

class ConfigTab: public QTabWidget, public SceneView
{
 public:
    ConfigTab(ConfigScene & parent);
    virtual ~ConfigTab();

    bool pixmapPosition(int index, qreal & x, qreal & y);
    void pixmapPress(int index, bool press);
    AudioSocket * getNearestSocket(qreal x, qreal y, bool is_input);
    AudioSocket * getPlayerSocket(int index);
    AudioSocket * getRecorderSocket(int index);
    bool isSocketInUse(const AudioSocket * sock) const;
    JackWire * getJack(int index);
    void plugOut(const AudioSocket * sock);
    void SaveConfig();
    SoundIoConnection getConnectionType(int index) const;

    inline const JackWire * getJack(int index) const
    {
        return const_cast<ConfigTab*>(this)->getJack(index);
    }

 protected:
    void LoadConfig();

    ConfigScene & parent;

    std::vector<JackWire*> wires;

 private:
    SYS_DEFINE_CLASS_NAME("ConfigTab");

}; // class ConfigTab

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_TAB_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
