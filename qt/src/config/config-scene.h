/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_SCENE_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_SCENE_H_INCLUDED__

#include <sound-io-connection.h>
#include <Debug/Debug.h>

#include <QHBoxLayout>

SYS_DECLARE_MODULE(DM_CONFIGSCENE);

class SoundcardTab;
class ConfigTab;
class HardwareTab;
class AudioSocket;
class JackWire;
class MyConfigWindow;

class ConfigScene: public QHBoxLayout
{
 public:
    ConfigScene(MyConfigWindow & parent);
    virtual ~ConfigScene();

    void PlayerDeviceMaxChannels(unsigned int channels);
    void RecordrDeviceMaxChannels(unsigned int channels);
    AudioSocket * getPlayerSocket(int index);
    AudioSocket * getRecorderSocket(int index);
    AudioSocket * getNearestSocket(qreal x, qreal y, bool is_input);
    JackWire * getJack(int index);
    const JackWire * getJack(int index) const;
    void plugOut(const AudioSocket * sock);
    void LoadConfig();
    void SaveConfig();
    SoundIoConnection getConnectionType(int index) const;
    void HardwareTypeSelected(MeasurementTypes type);

 protected:
    void inline addMyWidget(QWidget * widget, qreal factor = 0.0)
    {
        addWidget(widget);
        setStretchFactor(widget, factor);
    }

    MyConfigWindow & parent;

    SoundcardTab * soundcard;

    ConfigTab * config;

    HardwareTab * hardware;

 private:
    SYS_DEFINE_CLASS_NAME("ConfigScene");

}; // class ConfigScene

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_CONFIG_SCENE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
