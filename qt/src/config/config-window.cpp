/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <main-window.h>
#include <config-window.h>
#include <config-scene.h>
#include <jack-wire.h>

#include <QGraphicsView>
#include <QVBoxLayout>
#include <QLabel>
#include <QStatusBar>

SYS_DECLARE_MODULE(DM_QT);

MyConfigWindow::MyConfigWindow(MainWindow & parent):
    parent(parent),
    parameters(parent.getDeviceInfo()),
    config_scene(nullptr),
    in_dev(nullptr),
    in_speed_select(nullptr),
    out_dev(nullptr),
    out_speed_select(nullptr)
{
 SYS_DEBUG_MEMBER(DM_QT);

 config_scene = new ConfigScene(*this); // Necessary for the config window, but must be created befor device scan

 dev->Scan(*this);  // Creates the device list

 // Note:   The speed selector must be created before its
 //         device selector because the speed selector is
 //         updated by the device.
 out_speed_select = new OutputSpeedSelector(*this);
 out_dev = new OutputSelector(*this);
 in_speed_select = new InputSpeedSelector(*this);
 in_dev = new InputSelector(*this);

 QVBoxLayout * layout = new QVBoxLayout;

 {
    QHBoxLayout * lay1 = new QHBoxLayout;

    lay1->addWidget(new QLabel(tr("Input:")));
    lay1->addWidget(in_dev);
    lay1->addWidget(new QLabel(tr("Rate:")));
    lay1->addWidget(in_speed_select);

    lay1->addWidget(new QLabel(tr("Output:")));
    lay1->addWidget(out_dev);
    lay1->addWidget(new QLabel(tr("Rate:")));
    lay1->addWidget(out_speed_select);

    QWidget * filler = new QWidget;
    filler->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    lay1->addWidget(filler);

    layout->addLayout(lay1);
    layout->setStretchFactor(lay1, 0);
 }

 layout->addLayout(config_scene);
 layout->setStretchFactor(config_scene, 0);

 QWidget * filler = new QWidget;
 filler->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
 layout->addWidget(filler);
 layout->setStretchFactor(filler, 1);

 setLayout(layout);
}

MyConfigWindow::~MyConfigWindow()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MyConfigWindow::HardwareTypeSelected(MeasurementTypes type)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.HardwareTypeSelected(type);
}

void MyConfigWindow::enter(void)
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MyConfigWindow::leave(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 UpdateChannelTable();
}

void MyConfigWindow::LoadConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 config_scene->LoadConfig();

 // Note:   The configuration update order is important: the device
 //         must be updated first, and then the speed. The construction
 //         order must be the opposite, that's why it is not in their
 //         constructors.
 out_dev->LoadConfig();
 out_speed_select->LoadConfig();
 in_dev->LoadConfig();
 in_speed_select->LoadConfig();

 // Use the new channel table:
 UpdateChannelTable();
}

void MyConfigWindow::SaveConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 config_scene->SaveConfig();
 in_dev->SaveConfig();
 in_speed_select->SaveConfig();
 out_dev->SaveConfig();
 out_speed_select->SaveConfig();
}

void MyConfigWindow::UpdateChannelTable(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 memset(parameters.generator.channels, 0xff, sizeof(parameters.generator.channels));
 memset(parameters.recorder.channels, 0xff, sizeof(parameters.recorder.channels));

 for (int i = 0; i < 6; ++i) {
    const JackWire * jack = getJack(i);
    if (!jack) {
        continue;
    }
    SoundIoConnection conn = jack->getConnectionType();
    int index = jack->getAudioIndex();
    if (i) {
        // Input:
        parameters.recorder.channels[conn] = index;
    } else {
        // Output:
        parameters.generator.channels[conn] = index;
    }
 }
}

JackWire * MyConfigWindow::getJack(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return config_scene->getJack(index);
}

const JackWire * MyConfigWindow::getJack(int index) const
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return config_scene->getJack(index);
}

void MyConfigWindow::deviceScanning(const std::string & name)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.deviceScanning(name);
}

void MyConfigWindow::InputSpeedSelected(unsigned int speed)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parameters.recorder.sampling_rate = speed;
}

void MyConfigWindow::OutputSpeedSelected(unsigned int speed)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parameters.generator.sampling_rate = speed;
}

void MyConfigWindow::selectedPlayerDevice(SND::Device * dev)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parameters.generator.device = dev;

 SYS_DEBUG(DL_INFO1, "selected device " << *parameters.generator.device << " as player");

 parent.statusBar()->showMessage(tr("Device \"%1\" selected as output channel (%2)").arg(parameters.generator.device->getName().c_str(), parameters.generator.device->getDescription().c_str()));

 PlayerDeviceSpeedRates(parameters.generator.device->getAvailableRates());
 PlayerDeviceMaxChannels(parameters.generator.device->getChannelsMax());
}

void MyConfigWindow::selectedRecorderDevice(SND::Device * dev)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parameters.recorder.device = dev;

 SYS_DEBUG(DL_INFO1, "selected device " << *parameters.recorder.device << " as recorder");

 parent.statusBar()->showMessage(tr("Device \"%1\" selected as input channel (%2)").arg(parameters.recorder.device->getName().c_str(), parameters.recorder.device->getDescription().c_str()));

 RecorderDeviceSpeedRates(parameters.recorder.device->getAvailableRates());
 RecorderDeviceMaxChannels(parameters.recorder.device->getChannelsMax());
}

void MyConfigWindow::PlayerDeviceMaxChannels(unsigned int channels)
{
 SYS_DEBUG_MEMBER(DM_QT);

 SYS_DEBUG(DL_INFO1, "Player has " << channels << " channels");

 ASSERT_DBG(config_scene, "no config scene instance - internal error");

 config_scene->PlayerDeviceMaxChannels(channels);
}

void MyConfigWindow::RecorderDeviceMaxChannels(unsigned int channels)
{
 SYS_DEBUG_MEMBER(DM_QT);

 SYS_DEBUG(DL_INFO1, "Recorder has " << channels << " channels");

 ASSERT_DBG(config_scene, "no config scene instance - internal error");

 config_scene->RecordrDeviceMaxChannels(channels);
}

void MyConfigWindow::PlayerDeviceSpeedRates(const std::vector<unsigned int> & rates)
{
 SYS_DEBUG_MEMBER(DM_QT);

 ASSERT_DBG(out_speed_select, "no output speed selector instance - internal error");

 out_speed_select->setSpeeds(rates);
}

void MyConfigWindow::RecorderDeviceSpeedRates(const std::vector<unsigned int> & rates)
{
 SYS_DEBUG_MEMBER(DM_QT);

 ASSERT_DBG(in_speed_select, "no input speed selector instance - internal error");

 in_speed_select->setSpeeds(rates);
}

void MyConfigWindow::highlightedDevice(const SND::Device * dev)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.highlightedDevice(dev);
}

void MyConfigWindow::externalExceptionSignal(const char * module, const char * message)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.externalExceptionSignal(module, message);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
