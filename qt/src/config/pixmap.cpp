/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "pixmap.h"

#include <config-tab.h>

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QPixmap>

namespace {
#   include <xpm/jack-blue.xpm>
#   include <xpm/jack-green.xpm>
#   include <xpm/jack-red.xpm>
#   include <xpm/jack-yellow.xpm>

    const char ** xpms[] = {
        jack_blue_xpm,
        jack_green_xpm,
        jack_red_xpm,
        jack_yellow_xpm
    };
}

SYS_DEFINE_MODULE(DM_PIXMAPITEM);

PixmapItem::PixmapItem(int index, ConfigTab & parent, const QPointF & pos):
    QGraphicsPixmapItem(QPixmap(getXpmData(index))),
    parent(parent),
    index(index)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 parent.getScene()->addItem(this);
 setFlag(QGraphicsItem::ItemIsMovable);
 super::setPos(pos);
}

PixmapItem::~PixmapItem()
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);
}

const char ** PixmapItem::getXpmData(int index)
{
 SYS_DEBUG_STATIC(DM_PIXMAPITEM);

 if (index < 0 || index >= (int)(sizeof(xpms)/sizeof(xpms[0]))) {
    return xpms[0]; // Cannot throw
 }

 return xpms[index];
}

void PixmapItem::setPos(qreal x, qreal y)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 super::setPos(x, y);
 parent.pixmapPosition(index, x, y);
}

void PixmapItem::setPos(const QPointF & pos)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 super::setPos(pos);

 qreal x = pos.x();
 qreal y = pos.y();
 parent.pixmapPosition(index, x, y);
}

void PixmapItem::mousePressEvent(QGraphicsSceneMouseEvent * event)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 super::mousePressEvent(event);

 parent.pixmapPress(index, true);
}

void PixmapItem::mouseReleaseEvent(QGraphicsSceneMouseEvent * event)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 super::mouseReleaseEvent(event);

 parent.pixmapPress(index, false);
}

void PixmapItem::mouseMoveEvent(QGraphicsSceneMouseEvent * event)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 super::mouseMoveEvent(event);

 qreal x = pos().x();
 qreal y = pos().y();

 if (parent.pixmapPosition(index, x, y)) {
    super::setPos(x, y);
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
