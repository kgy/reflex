/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace {
#   include <xpm/soundcard.xpm>
}

#include "soundcard-tab.h"

#include <audio-socket.h>
#include <config-scene.h>

#include <Config/MainConfig.h>
#include <Exceptions/Exceptions.h>

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QPixmap>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         class SoundcardTab:                                                           *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

SoundcardTab::SoundcardTab(ConfigScene & parent):
    SceneView(&parent),
    parent(parent),
    sockets{{nullptr},{nullptr}}
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 scene->setSceneRect(QRect(0, 0, 200, 400));

 scene->addItem(new QGraphicsPixmapItem(QPixmap(soundcard_xpm)));

 {
    double x = 200.0;
    double y = 27.0;
    for (int i = 0; i < 6; ++i, y += 28.0) {
        sockets.players[i] = new AudioSocket(*this, QPointF(x, y), i);
    }
 }

 {
    double x = 200.0;
    double y = 215.0;
    for (int i = 0; i < 6; ++i, y += 28.0) {
        sockets.recorders[i] = new AudioSocket(*this, QPointF(x, y), i);
    }
 }

 addTab(view, tr("Soundcard"));

 LoadConfig();
}

SoundcardTab::~SoundcardTab()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);
}

void SoundcardTab::LoadConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

}

void SoundcardTab::SaveConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

}

void SoundcardTab::setPlayerChannels(unsigned int channels)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 for (unsigned int i = 0; i < 6; ++i) {
    ASSERT_DBG(sockets.players[i], "no socket class created - internal error");
    bool visible = channels > i;
    sockets.players[i]->setVisible(visible);
    if (!visible) {
        parent.plugOut(sockets.players[i]);
    }
 }
}

void SoundcardTab::setRecorderChannels(unsigned int channels)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 for (unsigned int i = 0; i < 6; ++i) {
    ASSERT_DBG(sockets.recorders[i], "no socket class created - internal error");
    bool visible = channels > i;
    sockets.recorders[i]->setVisible(visible);
    if (!visible) {
        parent.plugOut(sockets.recorders[i]);
    }
 }
}

AudioSocket * SoundcardTab::getPlayerSocket(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 if (index < 0 || index >= 6) {
    return nullptr;
 }

 return sockets.players[index];
}

AudioSocket * SoundcardTab::getRecorderSocket(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 if (index < 0 || index >= 6) {
    return nullptr;
 }

 return sockets.recorders[index];
}

AudioSocket * SoundcardTab::getNearestSocket(qreal x, qreal y, bool is_input)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 qreal distance = 1e6;  // must be big enough
 AudioSocket * result = nullptr;
 AudioSocket ** socks = is_input ? sockets.recorders : sockets.players;

 for (unsigned int i = 0; i < 6; ++i) {
    ASSERT_DBG(socks[i], "no socket class created at " << (is_input ? "input" : "output") << "[" << i << "] - internal error");
    qreal d = socks[i]->distance(x, y);
    if (d < distance) {
        result = socks[i];
        distance = d;
    }
 }

 return result;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
