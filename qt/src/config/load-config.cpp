/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "load-config.h"

#include <sstream>

SYS_DEFINE_MODULE(DM_LOADCONFIG);

ConfigLoader::ConfigLoader(const char * config_path):
    config_path(config_path)
{
 SYS_DEBUG_MEMBER(DM_LOADCONFIG);
}

ConfigLoader::~ConfigLoader()
{
 SYS_DEBUG_MEMBER(DM_LOADCONFIG);
}

void ConfigLoader::LoadConfig(void)
{
 SYS_DEBUG_MEMBER(DM_LOADCONFIG);

 ConfigValue cnf = MainConfig::GetConfig(config_path);
 if (cnf) {
    ConfigLoaded(config_path.c_str(), *cnf);
 }
}

void ConfigLoader::SaveConfigValue(const char * value)
{
 SYS_DEBUG_MEMBER(DM_LOADCONFIG);

 MainConfig::SetConfig(config_path, value);
}

void ConfigLoader::SaveConfigValue(int value)
{
 SYS_DEBUG_MEMBER(DM_LOADCONFIG);

 std::ostringstream os;
 os << value;
 MainConfig::SetConfig(config_path, os.str());
}

void ConfigLoader::SaveConfigValue(float value)
{
 SYS_DEBUG_MEMBER(DM_LOADCONFIG);

 std::ostringstream os;
 os << value;
 MainConfig::SetConfig(config_path, os.str());
}

void ConfigLoader::SaveConfigValue(double value)
{
 SYS_DEBUG_MEMBER(DM_LOADCONFIG);

 std::ostringstream os;
 os << value;
 MainConfig::SetConfig(config_path, os.str());
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
