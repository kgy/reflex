/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "jack-wire.h"

#include <config-tab.h>
#include <audio-socket.h>

#include <QString>
#include <QPointF>
#include <QPen>
#include <QColor>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QGraphicsScene>

#include <math.h>

SYS_DEFINE_MODULE(DM_JACK);

JackWire::JackWire(int index, ConfigTab & parent, bool is_input, const QPointF & pos, const QPointF & endpos, const QColor & wire_colour):
    ConfigLoader(QString("/HW/wire/%1/connection").arg(index).toUtf8().data()),
    parent(parent),
    is_input(is_input),
    homepos(pos),
    endpos(endpos),
    plug(new PixmapItem(index, parent, pos)),
    path(parent.getScene()->addPath(QPainterPath(), QPen(wire_colour, 3.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin))),
    socket(nullptr),
    last_pos{false, 0, 0}
{
 SYS_DEBUG_MEMBER(DM_JACK);

 qreal x = pos.x();
 qreal y = pos.y();
 wireMove(x, y);
}

JackWire::~JackWire()
{
 SYS_DEBUG_MEMBER(DM_JACK);
}

void JackWire::ConfigLoaded(const char *, const ConfExpression & value)
{
 SYS_DEBUG_MEMBER(DM_JACK);

 const int * i = value.ToInt();
 if (i) {
    connectWire(*i);
 }
}

void JackWire::SaveConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 SaveConfigValue(getAudioIndex());
}

void JackWire::connectWire(int index)
{
 SYS_DEBUG_MEMBER(DM_JACK);

 if (index < 0) {
    return;
 }

 SYS_DEBUG(DL_INFO1, "Wire " << getIndex() << " connecting to slot " << index);

 connectWire(isInput() ? parent.getRecorderSocket(index) : parent.getPlayerSocket(index));
}

void JackWire::connectWire(AudioSocket * sock)
{
 SYS_DEBUG_MEMBER(DM_JACK);

 if (!sock) {
    return;
 }

 socket = sock;
 qreal x = socket->pos().x() - 222.0f;
 qreal y = socket->pos().y() +   4.0f;
 plug->setPos(x, y);
 wireMove(x, y);
}

bool JackWire::wireMove(qreal & x, qreal & y)
{
 SYS_DEBUG_MEMBER(DM_JACK);

 SYS_DEBUG(DL_INFO1, "moved to " << x << ":" << y);

 QPainterPath new_path;
 new_path.moveTo(x+48, y+6.5);
 new_path.cubicTo(x+100, y+6.5, endpos.x()-52, endpos.y(), endpos.x(), endpos.y());

 path->setPath(new_path);

 last_pos.x = x;
 last_pos.y = y;
 last_pos.valid = true;

 return false;  // not changed
}

void JackWire::Pressed(bool press)
{
 SYS_DEBUG_MEMBER(DM_JACK);

 SYS_DEBUG(DL_INFO2, "Jack is " << (press ? "pressed" : "released"));

 if (press) {
    // Start drag:
    last_pos.valid = false;
    socket = nullptr;

 } else {
    // End drag:
    if (last_pos.valid) {
        qreal x = last_pos.x + 222.0f;
        qreal y = last_pos.y -   4.0f;
        AudioSocket * sock = parent.getNearestSocket(x, y, isInput());
        socket = nullptr;
        if (sock && !parent.isSocketInUse(sock) && sock->isVisible()) {
            x = sock->pos().x() - 222.0f;
            y = sock->pos().y() +   4.0f;
            qreal d = distance(x, y);
            if (d < 15) {
                // Found a socket:
                connectWire(sock);
                return;
            }
        }
    }
    // Go to the home position:
    plugOut();
 }
}

void JackWire::plugOut(void)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 // Set the wire to its initial position:
 plug->setPos(homepos);
 qreal x = homepos.x();
 qreal y = homepos.y();
 wireMove(x, y);
}

qreal JackWire::distance(qreal x, qreal y)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 QPointF diff = plug->pos() - QPointF(x, y);

 return sqrt(diff.x() * diff.x() + diff.y() * diff.y());
}

SoundIoConnection JackWire::getConnectionType(void) const
{
 SYS_DEBUG_MEMBER(DM_JACK);

 return parent.getConnectionType(plug->getIndex());
}

int JackWire::getAudioIndex(void) const
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 if (!socket) {
    return -1;
 }

 return socket->getIndex();
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
