/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_HARDWARE_TAB_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_HARDWARE_TAB_H_INCLUDED__

#include <sound-io-connection.h>
#include <load-config.h>
#include <Debug/Debug.h>

#include <QTabWidget>

#include <string>
#include <vector>

SYS_DECLARE_MODULE(DM_CONFIGSCENE);

class ConfigScene;

class HardwareTab: public ConfigLoader, public QTabWidget
{
 public:
    HardwareTab(ConfigScene & parent);
    virtual ~HardwareTab();

    void SaveConfig();
    SoundIoConnection getConnectionType(int index) const;

 protected:
    void currentChanged(int index);
    void addMyTab(const char ** pixmap, const QString & label, const QString & help);
    virtual void ConfigLoaded(const char * key, const ConfExpression & value) override;

    ConfigScene & parent;

    static struct ConnectionInfo {
        MeasurementTypes type;
        const char * name;
        SoundIoConnection path[4];
    } connection_types[];

 private:
    SYS_DEFINE_CLASS_NAME("HardwareTab");

}; // class HardwareTab

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_HARDWARE_TAB_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
