/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "config-scene.h"

#include <soundcard-tab.h>
#include <config-tab.h>
#include <hardware-tab.h>
#include <config-window.h>

#include <Config/MainConfig.h>
#include <Exceptions/Exceptions.h>

SYS_DEFINE_MODULE(DM_CONFIGSCENE);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         class ConfigScene:                                                            *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

ConfigScene::ConfigScene(MyConfigWindow & parent):
    parent(parent),
    soundcard(new SoundcardTab(*this)),
    config(new ConfigTab(*this)),
    hardware(new HardwareTab(*this))
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 setSpacing(0);

 addMyWidget(soundcard);
 addMyWidget(config);
 addMyWidget(hardware);

 QWidget * filler = new QWidget;
 filler->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
 addMyWidget(filler, 1.0);
}

ConfigScene::~ConfigScene()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);
}

void ConfigScene::HardwareTypeSelected(MeasurementTypes type)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 parent.HardwareTypeSelected(type);
}

SoundIoConnection ConfigScene::getConnectionType(int index) const
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return hardware->getConnectionType(index);
}

void ConfigScene::LoadConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);
}

void ConfigScene::SaveConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 soundcard->SaveConfig();
 config->SaveConfig();
 hardware->SaveConfig();
}

void ConfigScene::PlayerDeviceMaxChannels(unsigned int channels)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 SYS_DEBUG(DL_INFO2, "Player device has " << channels << " channels");

 ASSERT_DBG(soundcard, "no soundcard instance created - internal error");
 soundcard->setPlayerChannels(channels);
}

void ConfigScene::RecordrDeviceMaxChannels(unsigned int channels)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 SYS_DEBUG(DL_INFO2, "Recorder device has " << channels << " channels");

 ASSERT_DBG(soundcard, "no soundcard instance created - internal error");
 soundcard->setRecorderChannels(channels);
}

AudioSocket * ConfigScene::getPlayerSocket(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return soundcard->getPlayerSocket(index);
}

AudioSocket * ConfigScene::getRecorderSocket(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return soundcard->getRecorderSocket(index);
}

AudioSocket * ConfigScene::getNearestSocket(qreal x, qreal y, bool is_input)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 ASSERT_DBG(soundcard, "no soundcard instance created - internal error");
 return soundcard->getNearestSocket(x, y, is_input);
}

JackWire * ConfigScene::getJack(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return config->getJack(index);
}

const JackWire * ConfigScene::getJack(int index) const
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return config->getJack(index);
}

void ConfigScene::plugOut(const AudioSocket * sock)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 config->plugOut(sock);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
