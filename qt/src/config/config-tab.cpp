/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "config-tab.h"

#include <config-scene.h>
#include <jack-wire.h>

#include <Config/MainConfig.h>

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QString>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         class ConfigTab:                                                              *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

ConfigTab::ConfigTab(ConfigScene & parent):
    SceneView(&parent),
    parent(parent)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 scene->setSceneRect(QRect(0, 0, 200, 400));

 wires.resize(4);   // 4 plugs used now

 wires[0] = new JackWire(0, *this, false, QPointF(70,  50), QPointF(200,  42.5));   // Output
 wires[1] = new JackWire(1, *this, true,  QPointF(70, 150), QPointF(200, 176.5));   // Input
 wires[2] = new JackWire(2, *this, true,  QPointF(70, 250), QPointF(200, 256.5));   // Input
 wires[3] = new JackWire(3, *this, true,  QPointF(70, 350), QPointF(200, 386.5));   // Input

 SYS_DEBUG(DL_INFO3, "wires: " << wires.size() << " elements");

 addTab(view, tr("Configuration"));

 LoadConfig();
}

ConfigTab::~ConfigTab()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);
}

SoundIoConnection ConfigTab::getConnectionType(int index) const
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return parent.getConnectionType(index);
}

void ConfigTab::LoadConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 for (auto i = wires.begin(); i != wires.end(); ++i) {
    (*i)->LoadConfig();
 }
}

void ConfigTab::SaveConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 for (auto i = wires.begin(); i != wires.end(); ++i) {
    (*i)->SaveConfig();
 }
}

AudioSocket * ConfigTab::getPlayerSocket(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return parent.getPlayerSocket(index);
}

AudioSocket * ConfigTab::getRecorderSocket(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 return parent.getRecorderSocket(index);
}

AudioSocket * ConfigTab::getNearestSocket(qreal x, qreal y, bool is_input)
{
 return parent.getNearestSocket(x, y, is_input);
}

bool ConfigTab::isSocketInUse(const AudioSocket * sock) const
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 for (auto i = wires.begin(); i != wires.end(); ++i) {
    if ((*i)->isSocketInUse(sock)) {
        SYS_DEBUG(DL_INFO1, "In use.");
        return true;
    }
 }

 SYS_DEBUG(DL_INFO1, "Free.");
 return false;
}

void ConfigTab::plugOut(const AudioSocket * sock)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 for (auto i = wires.begin(); i != wires.end(); ++i) {
    (*i)->plugOut(sock);
 }
}

bool ConfigTab::pixmapPosition(int index, qreal & x, qreal & y)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 SYS_DEBUG(DL_INFO1, "Pixmap " << index << " is at " << x << ":" << y);

 if (index < 0 || index >= (int)wires.size()) {
    SYS_DEBUG(DL_WARNING, "Index " << index << " is out of range: 0..." << wires.size());
    return false;
 }

 bool changed = false;

 if (x < -25.0) {
    x = -25.0;
    changed = true;
 } else if (x > 150.0) {
    x = 150.0;
    changed = true;
 }

 if (y < 0.0) {
    y = 0.0;
    changed = true;
 } else if (y > 388.0) {
    y = 388.0;
    changed = true;
 }

 if (wires[index]->wireMove(x, y)) {
    changed = true;
 }

 return changed;
}

JackWire * ConfigTab::getJack(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 if (index < 0 || index >= (int)wires.size()) {
    return nullptr;
 }

 return wires[index];
}

void ConfigTab::pixmapPress(int index, bool press)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 if (index >= 0 && index < (int)wires.size()) {
    wires[index]->Pressed(press);
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
