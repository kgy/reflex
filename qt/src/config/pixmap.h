/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_PIXMAP_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_PIXMAP_H_INCLUDED__

#include <QGraphicsPixmapItem>

#include <Debug/Debug.h>

SYS_DECLARE_MODULE(DM_PIXMAPITEM);

class ConfigTab;

class PixmapItem: public QGraphicsPixmapItem
{
    using super = QGraphicsPixmapItem;

 public:
    PixmapItem(int index, ConfigTab & parent, const QPointF &pos);
    virtual ~PixmapItem();

    void setPos(qreal x, qreal y);
    void setPos(const QPointF & pos);

    inline int getIndex(void) const
    {
        return index;
    }

 protected:
    ConfigTab & parent;

    int index;

    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent * event) override;
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * event) override;
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * event) override;

 private:
    SYS_DEFINE_CLASS_NAME("PixmapItem");

    static const char ** getXpmData(int index);

}; // class PixmapItem

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_PIXMAP_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
