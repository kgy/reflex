/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace {
#   include <xpm/hardware-advanced.xpm>
#   include <xpm/hardware-simple.xpm>
#   include <xpm/hardware-amplifier.xpm>
}

#include "hardware-tab.h"

#include <config-scene.h>

#include <QLabel>
#include <QPixmap>

HardwareTab::ConnectionInfo HardwareTab::connection_types[] = {
    {   MEASUREMENT_SPEAKER_1,  "Speaker 1",    {   SOUND_OUT_SIGNAL,       SOUND_IN_FEEDBACK,          SOUND_IN_SPEAKER_VOLTAGE,   SOUND_IN_MICROPHONE     } },
    {   MEASUREMENT_SPEAKER_2,  "Speaker 2",    {   SOUND_OUT_SIGNAL,       SOUND_IN_SPEAKER_VOLTAGE,   SOUND_IN_SPEAKER_CURRENT,   SOUND_IN_MICROPHONE     } },
    {   MEASUREMENT_AMPLIFIER,  "Amplifier",    {   SOUND_OUT_SIGNAL,       SOUND_IN_FEEDBACK,          SOUND_IN_CROSSTALK,         SOUND_IN_AMPLIFIER_OUT  } }
};

HardwareTab::HardwareTab(ConfigScene & parent):
    ConfigLoader("/HW/hardware"),
    parent(parent)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 addMyTab(hardware_simple_xpm, tr("Speaker 1"), tr("Simple speaker measurement with\nthe possible fewest components"));
 addMyTab(hardware_advanced_xpm, tr("Speaker 2"), tr("Enhanced speaker measurement with\namplifier and voltage/current output"));
 addMyTab(hardware_amplifier_xpm, tr("Amplifier"), tr("Amplifier measurement"));

 connect(this, &QTabWidget::currentChanged, this, &HardwareTab::currentChanged);

 LoadConfig();

 // If the index 0 is selected, the notebook widget does not emit signal.
 // So, it must be sent directly:
 if (currentIndex() == 0) {
    currentChanged(0);
 }
}

HardwareTab::~HardwareTab()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);
}

void HardwareTab::currentChanged(int index)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 ASSERT_DBG(index >= 0 && index <= _MEASUREMENT_SIZE, "wrong hardware tab index: " << index);

 parent.HardwareTypeSelected(connection_types[index].type);
}

SoundIoConnection HardwareTab::getConnectionType(int index) const
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 ASSERT_DBG(index >= 0 && index < 4, "wrong wire index: " << index);

 int hwtype = currentIndex();
 ASSERT_DBG(hwtype >= 0 && hwtype < (int)(sizeof(connection_types)/sizeof(connection_types[0])), "wrong hardware type index: " << index);

 return connection_types[hwtype].path[index];
}

void HardwareTab::addMyTab(const char ** pixmap, const QString & label, const QString & help)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 QLabel * label1 = new QLabel(this);
 label1->setPixmap(QPixmap(pixmap));
 int index = addTab(label1, label);
 setTabWhatsThis(index, help);
}

void HardwareTab::ConfigLoaded(const char *, const ConfExpression & value)
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 SYS_DEBUG(DL_INFO1, "Tab '" << value << "' is set in config");

 ASSERT_DBG(count() == _MEASUREMENT_SIZE, "wrong number of tabs: " << count() << " instead of " << (int)_MEASUREMENT_SIZE);

 for (int i = 0; i < count(); ++i) {
    if (value.GetString() == connection_types[i].name) {
        setCurrentIndex(i);
        SYS_DEBUG(DL_INFO1, "Tab '" << i << "' is selected by config");
        break;
    }
 }
}

void HardwareTab::SaveConfig()
{
 SYS_DEBUG_MEMBER(DM_CONFIGSCENE);

 SaveConfigValue(connection_types[currentIndex()].name);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
