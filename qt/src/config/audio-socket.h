/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_AUDIO_SOCKET_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_AUDIO_SOCKET_H_INCLUDED__

#include <list>
#include <QPixmap>

#include <Debug/Debug.h>

SYS_DECLARE_MODULE(DM_PIXMAPITEM);

class SoundcardTab;
class QGraphicsPixmapItem;
class QGraphicsItem;
class QPointF;

class AudioSocket
{
 public:
    AudioSocket(SoundcardTab & parent, const QPointF & position, int index);
    virtual ~AudioSocket();

    qreal distance(qreal x, qreal y);

    inline int getIndex(void) const
    {
        return index;
    }

    void setVisible(bool v);

    inline bool isVisible(void) const
    {
        return visible;
    }

    inline const QPointF & pos(void) const
    {
        return my_pos;
    }

 protected:
    SoundcardTab & parent;

    int index;

    QPointF my_pos;

    QPointF next_pos;

    bool visible;

    std::list<QGraphicsItem*> items;

    void addItem(const QPointF & pos, QGraphicsItem * item);
    void addItem(QGraphicsItem * item, qreal y_offset = 0.0);

 private:
    SYS_DEFINE_CLASS_NAME("AudioSocket");

}; // class AudioSocket

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_AUDIO_SOCKET_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
