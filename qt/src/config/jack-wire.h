/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_CONFIG_JACK_WIRE_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_CONFIG_JACK_WIRE_H_INCLUDED__

#include "pixmap.h"

#include <sound-io-connection.h>
#include <load-config.h>

class ConfigTab;
class QPointF;
class QGraphicsPathItem;
class AudioSocket;

SYS_DECLARE_MODULE(DM_JACK);

class JackWire: public ConfigLoader
{
 public:
    JackWire(int index, ConfigTab & parent, bool is_input, const QPointF & pos, const QPointF & endpos, const QColor & wire_colour = QColor(0, 0, 0));
    virtual ~JackWire();

    bool wireMove(qreal & x, qreal & y);
    void Pressed(bool press);
    qreal distance(qreal x, qreal y);
    int getAudioIndex(void) const;
    void connectWire(int index);
    void connectWire(AudioSocket * sock);
    void plugOut(void);
    void SaveConfig(void);
    SoundIoConnection getConnectionType(void) const;

    inline AudioSocket * getSocket(void)
    {
        return socket;
    }

    inline void plugOut(const AudioSocket * sock)
    {
        SYS_DEBUG_MEMBER(DM_JACK);
        if (sock == socket) {
            plugOut();
        }
    }

    inline bool isInput(void) const
    {
        SYS_DEBUG_MEMBER(DM_JACK);
        return is_input;
    }

    inline int getIndex(void) const
    {
        return plug->getIndex();
    }

    inline bool isSocketInUse(const AudioSocket * sock) const
    {
        SYS_DEBUG_MEMBER(DM_JACK);
        return sock && (sock == socket);
    }

 protected:
    virtual void ConfigLoaded(const char * key, const ConfExpression & value) override;

    ConfigTab & parent;

    bool is_input;

    QPointF homepos;

    QPointF endpos;

    PixmapItem * plug;

    QGraphicsPathItem * path;

    AudioSocket * socket;

    struct {
        bool valid;
        qreal x;
        qreal y;

    } last_pos;

 private:
    SYS_DEFINE_CLASS_NAME("JackWire");

}; // class JackWire

#endif /* __SPEAKER_TEST_QT_SRC_CONFIG_JACK_WIRE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
