/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace {
#   include <xpm/audio-socket.xpm>
}

#include "audio-socket.h"

#include <soundcard-tab.h>

#include <QGraphicsScene>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>
#include <QFont>

#include <math.h>

AudioSocket::AudioSocket(SoundcardTab & parent, const QPointF & position, int index):
    parent(parent),
    index(index),
    my_pos(position),
    next_pos(position),
    visible(true)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 addItem(new QGraphicsPixmapItem(QPixmap(audio_socket_xpm)));

 const char * label = "Ch%1";
 switch (index) {
    case 0:
        label = "(left) Ch%1";
    break;
    case 1:
        label = "(right) Ch%1";
    break;
 }
 QGraphicsTextItem * text = new QGraphicsTextItem(QString(label).arg(index+1));
 text->setFont(QFont("Helvetica", 7));
 addItem(text, 4);
}

AudioSocket::~AudioSocket()
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);
}

void AudioSocket::addItem(const QPointF & pos, QGraphicsItem * item)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 parent.getScene()->addItem(item);
 item->setPos(pos);
 items.push_back(item);
}

void AudioSocket::addItem(QGraphicsItem * item, qreal y_offset)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 next_pos -= QPointF(item->boundingRect().width(), y_offset);
 addItem(next_pos, item);
}

void AudioSocket::setVisible(bool v)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 visible = v;

 for (auto i: items) {
    i->setVisible(visible);
 }
}

qreal AudioSocket::distance(qreal x, qreal y)
{
 SYS_DEBUG_MEMBER(DM_PIXMAPITEM);

 QPointF diff = pos() - QPointF(x, y);

 return sqrt(diff.x() * diff.x() + diff.y() * diff.y());
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
