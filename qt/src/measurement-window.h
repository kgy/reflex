/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_MEASUREMENT_WINDOW_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_MEASUREMENT_WINDOW_H_INCLUDED__

#include <sound-io-connection.h>
#include <my-customplot.h>
#include <measure-controller.h>
#include <measures/analyze-base.h>
#include <fft-base.h>
#include <Memory/Memory.h>
#include <Debug/Debug.h>

#include <measurement-widget.h>

#include <vector>

#include <QWidget>

class MainWindow;
class StartStopWidget;
class QVBoxLayout;
struct MouseInfo;

class MyMeasurementWindow: public MeasureController
{
    Q_OBJECT

 public:
    MyMeasurementWindow(MainWindow & parent);
    virtual ~MyMeasurementWindow();

    void HardwareTypeSelected(MeasurementTypes type);
    bool startButton(void);
    void stopButton(void);
    void enter(void);
    void leave(void);

 protected:
    class MyMeasurementWidget: public MeasurementWidget
    {
     public:
        MyMeasurementWidget(PLOT::AnalyzeBase & mb, MeasureController & parent);

     private:
        SYS_DEFINE_CLASS_NAME("MyMeasurementWindow::MyMeasurementWidget");

    }; // class MyMeasurementWindow::MyMeasurementWidget

    virtual SND::DeviceInfo & getDeviceInfo(void) override;
    virtual const PLOT::Params * getPlotParameters(void) const override;
    virtual void MeasureStarted(void) override;
    virtual void MeasureStopped(void) override;
    virtual GENERATOR::GeneratorType getGeneratorType(void) const override;
    virtual std::string getInputLoadFileName(void) const override;
    virtual std::string getInputSaveFileName(void) const override;
    virtual std::string getOutputLoadFileName(void) const override;
    virtual std::string getOutputSaveFileName(void) const override;
    virtual void externalException(const char * module, const char * message) override;
    virtual float getGeneratorLevel(void) const override;
    virtual void ProcessInput(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[]) override;
    virtual void CalculateResult(PLOT::PlotData & p, unsigned int sampling_rate) override;

    MainWindow & parent;

    StartStopWidget * start_stop_widget;

 private:
    SYS_DEFINE_CLASS_NAME("MyMeasurementWindow");

}; // class MyMeasurementWindow

#endif /* __SPEAKER_TEST_QT_SRC_MEASUREMENT_WINDOW_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
