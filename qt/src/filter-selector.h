/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_FILTER_SELECTOR_H_INCLUDED__
#define __QT_SRC_FILTER_SELECTOR_H_INCLUDED__

#include <filters/extended-filters.h>
#include <my-customplot.h>

#include <QHBoxLayout>

class QComboBox;
class QPushButton;
class QState;
class QStateMachine;

class FilterSelectorBase: public QHBoxLayout
{
 public:
    virtual inline ~FilterSelectorBase()
    {
    }

 protected:
    inline FilterSelectorBase(PLOT::MeasureWidgetInterface * parent):
        parent(parent)
    {
    }

    PLOT::MeasureWidgetInterface * parent;

    void setGeneratorType(int new_type);

    inline void setFilterType(FILTER::FilterTypes t)
    {
        parent->setFilterType(t);
    }

    inline void setFFTMode(bool fft)
    {
        parent->setFFTMode(fft);
    }

 private:
    SYS_DEFINE_CLASS_NAME("FilterSelectorBase");

}; // class FilterSelectorBase

class FilterSelectorAmplifier: public FilterSelectorBase
{
 public:
    FilterSelectorAmplifier(PLOT::MeasureWidgetInterface * parent);

 protected:
    void inversion(bool is_inverted);
    void correction_selected(int new_correction);
    void alignment_selected(int new_alignment);
    void smoothing_selected(int new_smoothing);
    void update_filter(void);

    int getState(void) const;

    QComboBox * mode;
    QComboBox * type;
    QComboBox * corr;
    QComboBox * alignment;
    QComboBox * smoothing;
    QPushButton * button;
    QStateMachine * st;
    QState * s1;
    QState * s2;

    bool inverted;
    unsigned int correction;

 private:
    SYS_DEFINE_CLASS_NAME("FilterSelectorAmplifier");

}; // class FilterSelectorAmplifier

#endif /* __QT_SRC_FILTER_SELECTOR_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
