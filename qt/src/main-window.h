/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_MAIN_WINDOW_H_INCLUDED__
#define __QT_SRC_MAIN_WINDOW_H_INCLUDED__

#include <sound-io-connection.h>
#include <device-selector.h>
#include <speed-selector.h>
#include "my-app.h"

#include <Debug/Debug.h>

#include <QMainWindow>
#include <QMessageBox>

#include <string>

SYS_DECLARE_MODULE(DM_QT);

Q_DECLARE_OPAQUE_POINTER(SND::BufferPtr);
Q_DECLARE_METATYPE(SND::BufferPtr);
Q_DECLARE_OPAQUE_POINTER(SND::Buffer1DPtr);
Q_DECLARE_METATYPE(SND::Buffer1DPtr);
Q_DECLARE_OPAQUE_POINTER(SND::SoundFrameBufferPtr);
Q_DECLARE_METATYPE(SND::SoundFrameBufferPtr);

class MyConfigWindow;
class MyMeasurementWindow;
class MyCalibrationWindow;
class ConfigScene;

class MainWindow : public QMainWindow
{
    Q_OBJECT

 public:
    MainWindow(MyApplication & parent);
    virtual ~MainWindow();

    inline SND::DeviceInfo & getDeviceInfo(void)
    {
        return parameters;
    }

    inline void ClearException(void)
    {
        was_exception = false;  // Re-enable next exception to be displayed
    }

    float getGeneratorLevel(void) const;
    void externalException(const char * module, const char * message);
    void highlightedDevice(const SND::Device * dev);
    void LoadConfig(void);
    void SaveConfig(void);
    void deviceScanning(const std::string & name);
    void HardwareTypeSelected(MeasurementTypes type);

 protected:
    MyApplication & parent;

    ConfigScene * config_scene;

    MyConfigWindow * config_window;

    MyCalibrationWindow * calibration_window;

    MyMeasurementWindow * measurement_window;

    QWidget * analyze_window;

    int actual_tab;

    SND::DeviceInfo parameters;

    bool was_exception;

 private slots:
    void gotException(const char * module, const char * message);
    void tabChanged(int index);

 signals:
    void externalExceptionSignal(const char * module, const char * message);

 private:
    SYS_DEFINE_CLASS_NAME("MainWindow");

    std::string exception_message;

}; // class MainWindow

class ErrorMessage: public QMessageBox
{
 public:
    inline ErrorMessage(QWidget * parent):
        QMessageBox(parent)
    {
        setObjectName(tr("ERROR"));
        setIcon(QMessageBox::Critical);
        setInformativeText(tr("The current processing aborted"));
    }

}; // class MainWindow::ErrorMessage

#endif /* __QT_SRC_MAIN_WINDOW_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
