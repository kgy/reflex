/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    The class QCustomPlot is used here. It can be downloadef from
 *              their website at http://www.qcustomplot.com, see more details there.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "my-customplot.h"

#include <measure-response-fft.h>
#include <measures/mouse-info.h>

#include <QVector>
#include <QPushButton>

#include <math.h>

#include <sstream>

using PLOT::MyCustomPlot;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MyCustomPlot:                                                             *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MyCustomPlot::MyCustomPlot(MeasureWidgetInterface & parent, const Params & params):
    parent(parent),
    params(params)
{
 SYS_DEBUG_MEMBER(DM_QT);

 setInteraction(QCP::iRangeDrag, true);
 axisRects().at(0)->setRangeDrag(Qt::Horizontal);
 setInteraction(QCP::iRangeZoom, true);
 axisRects().at(0)->setRangeZoom(Qt::Horizontal);
 axisRects().at(0)->setRangeZoomFactor(sqrt(sqrt(0.5)), 0.0);  // Double the range with four steps

 xAxis->setLabel(params.label);
 xAxis->setScaleType(params.logarithmic ? QCPAxis::stLogarithmic : QCPAxis::stLinear);
 xAxis->setNumberFormat("eb");
 xAxis->setRange(params.left, params.right);

 if (params.logarithmic) {
    TickerPtr logTicker = MyTicker::Create();
    xAxis->setTicker(logTicker);
    xAxis->setSubTicks(true);
    xAxis->setNumberPrecision(0);
 }

 xAxis->grid()->setSubGridVisible(true);

 if (params.y[0].visible) {
    std::stringstream os;
    os << params.y[0].label << " [" << params.y[0].unit << ']';
    yAxis->setLabel(os.str().c_str());
    yAxis->setVisible(true);
    yAxis->grid()->setSubGridVisible(params.y[0].subgrid);
    drag_info.signal.min = params.y[0].min;
    drag_info.signal.max = params.y[0].max;
    setupSignalScale(0, 0);
 }

 if (params.y[1].visible) {
    std::stringstream os;
    os << params.y[1].label << " [" << params.y[1].unit << ']';
    yAxis2->setLabel(os.str().c_str());
    yAxis2->setVisible(true);
    yAxis2->grid()->setSubGridVisible(params.y[1].subgrid);
    drag_info.angle.min = params.y[1].min;
    drag_info.angle.max = params.y[1].max;
    setupAngleScale(0, 0);
 }

 setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

 for (unsigned int i = 0; graphCount() < (int)params.plots; ++i) {
    auto * g = addGraph(xAxis, params.plot[i].yAxis ? yAxis2 : yAxis);
    g->setName(params.plot[i].name);
    QColor c(params.plot[i].colour.r, params.plot[i].colour.g, params.plot[i].colour.b, params.plot[i].colour.a);
    g->setPen(QPen(c));
 }

 legend->setVisible(params.legend);
 if (params.legend) {
    QCPLayoutGrid * subLayout = new QCPLayoutGrid;
    plotLayout()->addElement(1, 0, subLayout);
    subLayout->setMargins(QMargins(5, 0, 5, 5));
    subLayout->addElement(0, 0, legend);
    // change the fill order of the legend, so it's filled left to right in columns:
    legend->setFillOrder(QCPLegend::foColumnsFirst);
    // set legend's row stretch factor very small so it ends up with minimum height:
    plotLayout()->setRowStretchFactor(1, 0.001);
    //
    for (unsigned int i = 0; i < params.plots; ++i) {
        QColor c(params.plot[i].colour.r, params.plot[i].colour.g, params.plot[i].colour.b, 255);
        auto p = legend->item(i);
        p->setTextColor(c);
        legend2graph[p] = graph(i);
    }
 }

 connect(xAxis, static_cast<void (QCPAxis::*)(const QCPRange &)>(&QCPAxis::rangeChanged), this, &MyCustomPlot::xAxisChanged);
 connect(this, &QCustomPlot::legendClick, this, &MyCustomPlot::legendClicked);
 connect(this, &MyCustomPlot::Replot, [=](PLOT::PlotDataPtr & p) { doReplot(p); });
}

MyCustomPlot::~MyCustomPlot()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MyCustomPlot::setupAngleScale(int zoom, int drag)
{
 SYS_DEBUG_MEMBER(DM_QT);

 double height = (double)yAxis2->axisRect()->height();
 double min = drag_info.angle.min;
 double max = drag_info.angle.max;

 if (drag) {
    double delta = (drag_info.angle.max-drag_info.angle.min) * ((double)drag/height);
    min += delta;
    max += delta;
 } else if (zoom) {
    double pos = (double)drag_info.y / height;
    double size = drag_info.angle.max-drag_info.angle.min;
    if (zoom > 0) {
        double new_size = size / sqrt(sqrt(2.0));
        min -= (new_size-size) * (1.0-pos);
        max += (new_size-size) * pos;
    } else {
        double new_size = size * sqrt(sqrt(2.0));
        min -= (new_size-size) * (1.0-pos);
        max += (new_size-size) * pos;
    }
 }

 if (min > -180.0) {
    drag_info.angle.min = min;
 }
 if (max < 180.0) {
    drag_info.angle.max = max;
 }

 yAxis2->setRange(drag_info.angle.min, drag_info.angle.max);

 replot(RefreshPriority::rpQueuedReplot);
}

void MyCustomPlot::setupSignalScale(int zoom, int drag)
{
 SYS_DEBUG_MEMBER(DM_QT);

 double height = (double)yAxis->axisRect()->height();
 double min = drag_info.signal.min;
 double max = drag_info.signal.max;

 if (drag) {
    double delta = (drag_info.signal.max-drag_info.signal.min) * ((double)drag/height);
    min += delta;
    max += delta;
 } else if (zoom) {
    double pos = (double)drag_info.y / height;
    double size = drag_info.signal.max-drag_info.signal.min;
    if (zoom > 0) {
        double new_size = size / sqrt(sqrt(2.0));
        min -= (new_size-size) * (1.0-pos);
        max += (new_size-size) * pos;
    } else {
        double new_size = size * sqrt(sqrt(2.0));
        min -= (new_size-size) * (1.0-pos);
        max += (new_size-size) * pos;
    }
 }

 if (min > -180.0) {
    drag_info.signal.min = min;
 }
 if (max < 180.0) {
    drag_info.signal.max = max;
 }

 yAxis->setRange(drag_info.signal.min, drag_info.signal.max);

 replot(RefreshPriority::rpQueuedReplot);
}

/// Cursor event on the left side area (Signal Strength)
void MyCustomPlot::cursorMoveLeft(const MouseInfo & info)
{
 SYS_DEBUG_MEMBER(DM_QT);

 int drag = 0;

 if (info.left) {
    if (drag_info.y >= 0) {
        drag = info.y - drag_info.y;
    }
 }

 drag_info.y = info.y;

 if (info.wheel || drag) {
    setupSignalScale(info.wheel, drag);
 }
}

/// Cursor event on the right side area (Angle)
void MyCustomPlot::cursorMoveRight(const MouseInfo & info)
{
 SYS_DEBUG_MEMBER(DM_QT);

 int drag = 0;

 if (info.left) {
    if (drag_info.y >= 0) {
        drag = info.y - drag_info.y;
    }
 }

 drag_info.y = info.y;

 if (info.wheel || drag) {
    setupAngleScale(info.wheel, drag);
 }
}

void MyCustomPlot::legendClicked(QCPLegend *, QCPAbstractLegendItem * item, QMouseEvent *)
{
 SYS_DEBUG_MEMBER(DM_QT);

 auto graph = legend2graph.find(item);

 if (graph != legend2graph.end()) {
    // Toggle visibility:
    graph->second->setVisible(!graph->second->visible());
 }

 replot(RefreshPriority::rpQueuedReplot);
}

void MyCustomPlot::xAxisChanged(const QCPRange & newRange)
{
 SYS_DEBUG_MEMBER(DM_QT);

 xAxis->setRange(newRange.bounded(params.left, params.right));
}

void MyCustomPlot::doReplot(PLOT::PlotDataPtr & p)
{
 SYS_DEBUG_MEMBER(DM_QT);

 PLOT::PlotDataPtr p2 = p;

 const std::vector<double> & x = p2->x;

 if (x.empty()) {
    return;
 }

 unsigned int graphs = p2->y.size();

 for (unsigned int ch = 0; ch < graphs; ++ch) {
    auto g = graph(ch);
    if (g) {
        g->setData(x, p2->y[ch], x.size(), 1U, true); // Skip the index [0]
    }
 }

 replot(RefreshPriority::rpQueuedReplot);
}

void MyCustomPlot::wheelEvent(QWheelEvent * event)
{
 SYS_DEBUG_MEMBER(DM_QT);

 MouseInfo info;

 info.x = event->x();
 info.y = event->y();
 info.wheel = event->angleDelta().y() < 0 ? -1 : 1;

 mouseEvent(event->buttons(), info);

 if (info.section == MouseInfo::DRAW_SECTION_DIAGRAMS) {
    super::wheelEvent(event);
 }
}

void MyCustomPlot::mousePressEvent(QMouseEvent * event)
{
 SYS_DEBUG_MEMBER(DM_QT);

 MouseInfo info;

 info.x = event->x();
 info.y = event->y();

 mouseEvent(event->buttons(), info);

 super::mousePressEvent(event);
}

void MyCustomPlot::mouseMoveEvent(QMouseEvent * event)
{
 SYS_DEBUG_MEMBER(DM_QT);

 MouseInfo info;

 info.x = event->x();
 info.y = event->y();

 mouseEvent(event->buttons(), info);

 if (info.section == MouseInfo::DRAW_SECTION_DIAGRAMS) {
    super::mouseMoveEvent(event);
 }
}

void MyCustomPlot::mouseEvent(Qt::MouseButtons buttons, MouseInfo & info)
{
 SYS_DEBUG_MEMBER(DM_QT);

 info.left = buttons & Qt::LeftButton;
 info.middle = buttons & Qt::MiddleButton;
 info.right = buttons & Qt::RightButton;

 double freq = 0.0;

 if (!legend || info.y < legend->rect().top()) {
    double f = xAxis->pixelToCoord(info.x);
    if (f < xAxis->range().lower) {
        // The cursor is at left of the diagram:
        info.section = MouseInfo::DRAW_SECTION_AMPLITUDE;
    } else if (f > xAxis->range().upper) {
        // The cursor is at right of the diagram:
        info.section = MouseInfo::DRAW_SECTION_ANGLE;
    } else {
        // The cursor is within the diagram:
        info.section = MouseInfo::DRAW_SECTION_DIAGRAMS;
        freq = f;
    }
 }

 parent.cursorMove(freq, info);
}

void MyCustomPlot::updateLabels(const char * xLabel, const std::string yLabels[], bool update)
{
 SYS_DEBUG_MEMBER(DM_QT);

 xAxis->setLabel(QString("%1: %2").arg(params.label).arg(xLabel));

 for (unsigned int i = 0; i < params.plots; ++i) {
    auto g = graph(i);
    if (g) {
        std::stringstream os;
        os << params.plot[i].name << ' ' << yLabels[i] << params.plot[i].unit;
        g->setName(os.str().c_str());
    }
 }

 if (update) {
    replot(RefreshPriority::rpQueuedReplot);
 }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MyCustomPlot::MyTicker:                                                   *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MyCustomPlot::MyTicker::MyTicker(void):
    full_size(0.0)
{
 SYS_DEBUG_MEMBER(DM_QT);
}

MyCustomPlot::MyTicker::~MyTicker(void)
{
 SYS_DEBUG_MEMBER(DM_QT);
}

MyCustomPlot::TickerPtr MyCustomPlot::MyTicker::Create(void)
{
 SYS_DEBUG_STATIC(DM_QT);

 return TickerPtr(new MyTicker());
}

QVector<double> MyCustomPlot::MyTicker::createTickVector(double /*tickStep*/, const QCPRange & range)
{
 SYS_DEBUG_MEMBER(DM_QT);

 QVector<double> result;
 double start = log(range.lower)/log(10.0);
 double end = log(range.upper)/log(10.0);
 full_size = end - start;
 full_range = range;

 // cycle from 1 to 100000:
 double previous = 1.0;
 for (int i = 1; i <= 5; ++i) {
    double value = exp10(i);
    createTickVector(result, {previous, value});
    previous = value;
 }

 return result;
}

void MyCustomPlot::MyTicker::createTickVector(QVector<double> & result, const QCPRange & range, int level)
{
 SYS_DEBUG_MEMBER(DM_QT);

 if (range.upper < full_range.lower || range.lower > (2.0*full_range.upper-full_range.lower)) {
    return;
 }

 double ratio;

 {
    double start = log(range.lower)/log(10.0);
    double end = log(range.upper)/log(10.0);
    double size = end - start;
    ratio = size / full_size;
 }

 double region_size = range.upper/range.lower;

 if (ratio < 0.1) {
    // Add only the first tick:
    result.append(range.lower);
    return;
 }

 double previous = range.lower;
 double size = range.upper - previous;

 if (region_size > 9.9) {
    // Logarithmic interpolation:
    if (ratio > 0.5) {
        for (int i = 1; i < 10; ++i) {
            double data = range.lower+size*(double)i/9.0;
            createTickVector(result, {previous, data}, level+1);
            previous = data;
        }
        return;

    } else if (ratio > 0.2) {
        double data1 = previous+(1.0/9.0)*size;
        double data2 = previous+(4.0/9.0)*size;
        createTickVector(result, {previous, data1}, level+1);
        createTickVector(result, {data1, data2}, level+1);
        createTickVector(result, {data2, range.upper}, level+1);
        return;

    }

    double data = previous+(4.0/9.0)*size;
    createTickVector(result, {previous, data}, level+1);
    createTickVector(result, {data, range.upper}, level+1);
    return;

 } else if (region_size > 4.9) {
 } else if (region_size > 2.4) {
 } else {
    if (ratio > 0.6) {
        for (int i = 1; i <= 10; ++i) {
            double data = range.lower+size*(double)i/10.0;
            createTickVector(result, {previous, data}, level+1);
            previous = data;
        }
        return;
    }
 }

 // Add only the first tick:
 result.append(range.lower);
}

QVector<double> MyCustomPlot::MyTicker::createSubTickVector(int /*subTickCount*/, const QVector<double> & ticks)
{
 SYS_DEBUG_MEMBER(DM_QT);

 QVector<double> result;

 if (ticks.size() > 1) {
    double previous = ticks[0];
    for (int i = 1; i < ticks.size(); ++i) {
        double data = ticks[i];
        createSubTickVector(result, previous, data);
        previous = data;
    }
 }

 return result;
}

void MyCustomPlot::MyTicker::createSubTickVector(QVector<double> & result, double from, double to)
{
 SYS_DEBUG_MEMBER(DM_QT);

 double rate = to / from;
 double range = to - from;

 bool even;
 double ratio;

 {
    double start = log(from)/log(10.0);
    double end = log(to)/log(10.0);
    double size = end - start;
    ratio = size / full_size;
    even = (start-floor(start+1e-5)) < 0.5;
 }

 if (ratio < 0.025) {
    return;
 }

 if (rate > 9.9) {
 } else if (rate > 4.9) {
 } else if (rate > 2.4) {
    if (ratio > 0.075) {
        result.append(from + range/3.0);
        result.append(from + 2.0*range/3.0);
    }
 } else {
    if (ratio > 0.1) {
        if (even) {
            for (int i = 1; i < 10; ++i) {
                result.append(from + range*(double)i/10.0);
            }
        } else {
            for (int i = 1; i < 5; ++i) {
                result.append(from + range*(double)i/5.0);
            }
        }
    } else {
        if (even) {
            result.append(from + 0.5*range);
        }
    }
 }
}

QString MyCustomPlot::MyTicker::getTickLabel(double tick, const QLocale & /*locale*/, QChar /*formatChar*/, int /*precision*/)
{
 SYS_DEBUG_MEMBER(DM_QT);

 std::stringstream s;

 if (tick >= 1e3) {
    s << tick/1e3 << 'k';
 } else {
    s << tick;
 }

 return s.str().c_str();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       Stream functions:                                                               *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

std::ostream & operator<<(std::ostream & os, const QCPRange & r)
{
 os << '{' << r.lower << '|' << r.upper << '}';
 return os;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
