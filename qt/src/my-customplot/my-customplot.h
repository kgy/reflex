/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_MY_CUSTOMPLOT_MY_CUSTOMPLOT_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_MY_CUSTOMPLOT_MY_CUSTOMPLOT_H_INCLUDED__

#include <qcustomplot.h>
#include <measures/params.h>
#include <filters/extended-filters.h>
#include <generators/generator-type.h>

#include <Debug/Debug.h>

#include <QVector>
#include <string>
#include <map>
#include <iostream>

SYS_DECLARE_MODULE(DM_QT);

struct MouseInfo;

namespace PLOT
{
    class MeasureWidgetInterface
    {
     public:
        virtual void cursorMove(double freq, const MouseInfo & info) =0;

        virtual void setFilterType(FILTER::FilterTypes)
        {
        }

        virtual void setFFTMode(bool)
        {
        }

        virtual void setGeneratorType(GENERATOR::GeneratorType)
        {
        }

        virtual void setAlignmentMode(FILTER::AlignmentModes)
        {
        }

        virtual void setSmoothFactor(int)
        {
        }


    }; // class PLOT::MeasureWidgetInterface

    class MyCustomPlot: public QCustomPlot
    {
        Q_OBJECT

        using super = QCustomPlot;

     public:
        MyCustomPlot(MeasureWidgetInterface & parent, const Params & params);
        virtual ~MyCustomPlot();

        void doReplot(PLOT::PlotDataPtr & p);
        void cursorMoveLeft(const MouseInfo & info);
        void cursorMoveRight(const MouseInfo & info);
        void updateLabels(const char * xLabel, const std::string yLabels[], bool update);

     protected:
        using TickerPtr = QSharedPointer<QCPAxisTickerLog>;

        /// Override the tick calculations
        /*! In the current version of QCustomPlot (2.0.0) the ticker algorithm is very poor. This class
         *  is intended to improve this feature. The goal is to create readable logarithmic ticks. */
        class MyTicker: public QCPAxisTickerLog
        {
         public:
            virtual ~MyTicker(void);

            static TickerPtr Create(void);

         protected:
            MyTicker(void);

            virtual QVector<double> createTickVector(double tickStep, const QCPRange & range) override;
            virtual QVector<double> createSubTickVector(int subTickCount, const QVector<double> &ticks) override;
            virtual QString getTickLabel(double tick, const QLocale &locale, QChar formatChar, int precision) override;

            void createTickVector(QVector<double> & result, const QCPRange & range, int level = 0);
            void createSubTickVector(QVector<double> & result, double from, double to);

            QCPRange full_range;

            double full_size;

         private:
            SYS_DEFINE_CLASS_NAME("PLOT::MyCustomPlot::MyTicker");

        }; // class PLOT::MyCustomPlot::MyTicker

        virtual void mousePressEvent(QMouseEvent * event) override;
        virtual void mouseMoveEvent(QMouseEvent * event) override;
        virtual void wheelEvent(QWheelEvent *event) override;

        void mouseEvent(Qt::MouseButtons buttons, MouseInfo & info);
        void setupSignalScale(int zoom, int drag);
        void setupAngleScale(int zoom, int drag);

        MeasureWidgetInterface & parent;

        const Params & params;

        std::map<QCPAbstractLegendItem*, QCPGraph*> legend2graph;

        struct DragInfo
        {
            DragInfo(void):
                y(-1)
            {
            }

            struct Limits {
                Limits(void):
                    min(0.0),
                    max(0.0)
                {
                }

                double min;
                double max;

            } signal, angle;

            int y;

        } drag_info;

     signals:
        void Replot(PLOT::PlotDataPtr & p);

     private slots:
        void xAxisChanged(const QCPRange & newRange);
        void legendClicked(QCPLegend *, QCPAbstractLegendItem * item, QMouseEvent *);

     private:
        SYS_DEFINE_CLASS_NAME("MyCustomPlot");

    }; // class PLOT::MyCustomPlot

} // namespace PLOT

std::ostream & operator<<(std::ostream & os, const QCPRange & r);

#endif /* __SPEAKER_TEST_QT_SRC_MY_CUSTOMPLOT_MY_CUSTOMPLOT_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
