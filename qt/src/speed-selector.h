/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_SRC_SPEED_SELECTOR_H_INCLUDED__
#define __SPEAKER_TEST_QT_SRC_SPEED_SELECTOR_H_INCLUDED__

#include <QComboBox>

#include <load-config.h>
#include <Debug/Debug.h>

#include <vector>

SYS_DECLARE_MODULE(DM_QT);

class SpeedSelectorBase;
class InputSpeedSelector;
class OutputSpeedSelector;
class SpeedSelectorInterface;

class SpeedSelectorBase: public QComboBox, public ConfigLoader
{
 protected:
    inline SpeedSelectorBase(SpeedSelectorInterface & parent, const char * config_path):
        ConfigLoader(config_path),
        parent(parent)
    {
    }

    virtual ~SpeedSelectorBase()
    {
    }

    SpeedSelectorInterface & parent;

    std::vector<unsigned int> my_speeds;

 public:
    void setSpeeds(const std::vector<unsigned int> & speeds);
    unsigned int getSelectedRate(void) const;

 private:
    SYS_DEFINE_CLASS_NAME("SpeedSelectorBase");

}; // class SpeedSelectorBase

class InputSpeedSelector: public SpeedSelectorBase
{
 public:
    InputSpeedSelector(SpeedSelectorInterface & parent);
    virtual ~InputSpeedSelector();

    void SaveConfig(void);

 protected:
    virtual void ConfigLoaded(const char * key, const ConfExpression & value) override;

 private:
    SYS_DEFINE_CLASS_NAME("InputSpeedSelector");

}; // class InputSpeedSelector

class OutputSpeedSelector: public SpeedSelectorBase
{
 public:
    OutputSpeedSelector(SpeedSelectorInterface & parent);
    virtual ~OutputSpeedSelector();

    void SaveConfig(void);

 protected:
    virtual void ConfigLoaded(const char * key, const ConfExpression & value) override;

 private:
    SYS_DEFINE_CLASS_NAME("OutputSpeedSelector");

}; // class OutputSpeedSelector

#endif /* __SPEAKER_TEST_QT_SRC_SPEED_SELECTOR_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
