/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "main-window.h"

#include <device-interface.h>
#include <config-scene.h>
#include <config-window.h>
#include <jack-wire.h>
#include <measurement-window.h>
#include <calibration-window.h>
#include <measure-response-fft.h>
#include <measures/params.h>
#include <fft-base.h>
#include <measures/analyze-base.h>

#include <Config/MainConfig.h>

#include <QTabWidget>

#include <string.h>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MainWindow:                                                               *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MainWindow::MainWindow(MyApplication & parent):
    parent(parent),
    config_window(nullptr),
    calibration_window(nullptr),
    measurement_window(nullptr),
    analyze_window(nullptr),
    actual_tab(-1),
    was_exception(false)
{
 SYS_DEBUG_MEMBER(DM_QT);

 connect(this, &MainWindow::externalExceptionSignal, this, &MainWindow::gotException);

 QTabWidget * tab = new QTabWidget;
 setCentralWidget(tab);

 tab->setTabShape(QTabWidget::Triangular);

 measurement_window = new MyMeasurementWindow(*this);
 calibration_window = new MyCalibrationWindow(*this);
 config_window = new MyConfigWindow(*this); // Note: it loads the config and triggers the measurement and calibration tabs
 analyze_window = new QWidget();

 connect(tab, &QTabWidget::currentChanged, this, &MainWindow::tabChanged);

 tab->addTab(config_window, tr("Configuration"));
 tab->addTab(calibration_window, tr("Calibration"));
 tab->addTab(measurement_window, tr("Measurement"));
 tab->addTab(analyze_window, tr("Analyze"));

 statusBar()->showMessage(tr("Welcome to Refleks"));

 qRegisterMetaType<SND::BufferPtr>("SND::BufferPtr");
 qRegisterMetaType<SND::Buffer1DPtr>("SND::Buffer1DPtr");
 qRegisterMetaType<SND::SoundFrameBufferPtr>("SND::SoundFrameBufferPtr");
 qRegisterMetaType<FFT::FFTBuffer>("FFT::FFTBuffer");
 qRegisterMetaType<const int *>("const int *");
 qRegisterMetaType<PLOT::PlotDataPtr>("PLOT::PlotDataPtr");

 SYS_DEBUG(DL_INFO1, "Current config is:" << std::endl << MainConfig::Get());
}

MainWindow::~MainWindow(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 calibration_window->leave();
 measurement_window->leave();

 try {
    MainConfig::Get().SaveConfig();
 } catch (std::exception & ex) {
    std::cerr << "Could not save config file: " << ex.what() << std::endl;
 } catch (...) {
    std::cerr << "Could not save config file due to unknown exception." << std::endl;
 }
}

float MainWindow::getGeneratorLevel(void) const
{
 return calibration_window->getGeneratorLevel();
}

void MainWindow::HardwareTypeSelected(MeasurementTypes type)
{
 SYS_DEBUG_MEMBER(DM_QT);

 PLOT::AnalyzeBase::Select(type);

 calibration_window->HardwareTypeSelected(type);
 measurement_window->HardwareTypeSelected(type);
}

void MainWindow::tabChanged(int index)
{
 SYS_DEBUG_MEMBER(DM_QT);

 // This tab is just left:
 switch (actual_tab) {
    case 0:
        // The config window:
        config_window->leave();
    break;

    case 1:
        // Calibration window:
        calibration_window->leave();
    break;

    case 2:
        // Measurement window:
        measurement_window->leave();
    break;

    case 3:
        // Analyze window:
    break;
 }

 // This tab is just entered:
 switch (index) {
    case 0:
        // The config window:
        config_window->enter();
    break;

    case 1:
        // Calibration window:
        calibration_window->enter();
    break;

    case 2:
        // Measurement window:
        measurement_window->enter();
    break;

    case 3:
        // Analyze window:
    break;
 }

 actual_tab = index;
}

void MainWindow::LoadConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 config_window->LoadConfig();
}

void MainWindow::SaveConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 config_window->SaveConfig();
}

void MainWindow::deviceScanning(const std::string & name)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.initMessage(name);
}

void MainWindow::highlightedDevice(const SND::Device * d)
{
 SYS_DEBUG_MEMBER(DM_QT);

 SYS_DEBUG(DL_INFO1, "highlighted: " << (d ? d->getDescription() : "none"));

 statusBar()->showMessage(QString(d ? d->getDescription().c_str() : ""));
}

/// Called when a thread generated an exception
/*! \param  module  This is the name of the module (thread) generated the exception
 *  \param  message A descriptive message (from ex.what()) to show the problem details. */
void MainWindow::gotException(const char * module, const char * message)
{
 SYS_DEBUG_MEMBER(DM_QT);

 const char * m1 = "";
 const char * m2 = "";

 try {
    measurement_window->leave();
 } catch (...) {
    m1 = "(error in closing measurement)\n";
 }

 try {
    calibration_window->leave();
 } catch (...) {
    m2 = "(error in closing calibration)\n";
 }

 ErrorMessage m(this);

 m.setText(tr("%1%2In module %3: %4").arg(m1, m2, module, message));
 m.exec();
}

/// Exception handler: redirected to \ref gotException()
void MainWindow::externalException(const char * module, const char * message)
{
 SYS_DEBUG_MEMBER(DM_QT);

 std::cerr << "MainWindow::externalException('" << module << "', '" << message << "');" << std::endl;

 if (was_exception) {
    return;
 }

 was_exception = true;

 exception_message = message;

 externalExceptionSignal(module, exception_message.c_str());
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
