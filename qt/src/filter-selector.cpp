/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "filter-selector.h"

#include <QtGlobal>
#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QStateMachine>

static struct {
    const char * name;
    int mode;
} SmoothTable[] = {
    {   "off",           0                              },
    {   "9 samples",     9                              },
    {   "33 samples",   33                              },
    {   "99 samples",   99                              },
    {   "1/30 octave",  FILTER::SMOOTH_OCTAVE_PER_30    },
    {   "1/10 octave",  FILTER::SMOOTH_OCTAVE_PER_10    },
    {   "1/3 octave",   FILTER::SMOOTH_OCTAVE_PER_3     },
    {   nullptr,        -1                              }
};

static struct {
    const char * name;
    FILTER::AlignmentModes mode;
} AlignmentTable[] = {
    {   "1kHz",         FILTER::ALIGNMENT_1K            },
    {   "Peak",         FILTER::ALIGNMENT_PEAK          },
    {   "off",          FILTER::ALIGNMENT_NONE          },
    {   nullptr,        FILTER::ALIGNMENT_NONE          }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class FilterSelectorBase:                                                       *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void FilterSelectorBase::setGeneratorType(int new_type)
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (new_type) {
    case 0:
        parent->setGeneratorType(GENERATOR::GENERATOR_WHITE_NOISE);
    break;

    case 1:
        parent->setGeneratorType(GENERATOR::GENERATOR_SWEEP);
    break;
 }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class FilterSelectorAmplifier:                                                  *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

FilterSelectorAmplifier::FilterSelectorAmplifier(PLOT::MeasureWidgetInterface * parent):
    FilterSelectorBase(parent),
    mode(new QComboBox),
    type(new QComboBox),
    corr(new QComboBox),
    alignment(new QComboBox),
    smoothing(new QComboBox),
    button(new QPushButton),
    st(new QStateMachine),
    s1(new QState),
    s2(new QState),
    inverted(false),
    correction(0U)
{
 connect(mode, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](int t){ setFFTMode(t); });
 connect(type, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](int t){ setGeneratorType(t); });
 connect(corr, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](int t){ correction_selected(t); });
 connect(alignment, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](int t){ parent->setAlignmentMode(AlignmentTable[t].mode); });
 connect(smoothing, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](int t){ parent->setSmoothFactor(SmoothTable[t].mode); });
 connect(s1, &QState::entered, [=](){ inversion(0); });
 connect(s2, &QState::entered, [=](){ inversion(1); });

 mode->addItem(tr("Measure"));
 mode->addItem(tr("Spectrum"));

 type->addItem(tr("Noise"));
 type->addItem(tr("Sweep"));

 corr->addItem(tr("None (linear)"));
 corr->addItem(tr("RIAA"));
 corr->addItem(tr("Pink Noise"));

 for (int i = 0; AlignmentTable[i].name; ++i) {
    alignment->addItem(AlignmentTable[i].name);
 }
 parent->setAlignmentMode(AlignmentTable[0].mode);

 for (int i = 0; SmoothTable[i].name; ++i) {
    smoothing->addItem(SmoothTable[i].name);
 }
 parent->setSmoothFactor(SmoothTable[0].mode);

 QLabel * label0 = new QLabel(tr("Mode:"));
 QLabel * label1 = new QLabel(tr("Type:"));
 QLabel * label2 = new QLabel(tr("Correction:"));
 QLabel * label3 = new QLabel(tr("Alignment:"));
 QLabel * label4 = new QLabel(tr("Smoothing:"));
 QWidget * w = new QWidget;

 button->setVisible(false);

 s1->assignProperty(button, "text", tr("Forward"));
 s1->setObjectName("forward");

 s2->assignProperty(button, "text", tr("Reverse"));
 s2->setObjectName("reverse");

 s1->addTransition(button, SIGNAL(clicked()), s2);
 s2->addTransition(button, SIGNAL(clicked()), s1);

 st->addState(s1);
 st->addState(s2);
 st->setInitialState(s1);
 st->start();

 // Add the objects to the layout:

 addWidget(label0);
 setStretchFactor(label0, 0);
 addWidget(mode);
 setStretchFactor(mode, 0);
 addWidget(label1);
 setStretchFactor(label1, 0);
 addWidget(type);
 setStretchFactor(type, 0);
 addWidget(label2);
 setStretchFactor(label2, 0);
 addWidget(corr);
 setStretchFactor(corr, 0);
 addWidget(button);
 setStretchFactor(button, 0);
 addWidget(label3);
 setStretchFactor(label3, 0);
 addWidget(alignment);
 setStretchFactor(alignment, 0);
 addWidget(label4);
 setStretchFactor(label4, 0);
 addWidget(smoothing);
 setStretchFactor(smoothing, 0);
 addWidget(w);
 setStretchFactor(w, 1);
}

int FilterSelectorAmplifier::getState(void) const
{
 if (st->configuration().contains(s1)) {
    return 1;
 } else if (st->configuration().contains(s2)) {
    return 2;
 }
 return 0;
}

void FilterSelectorAmplifier::correction_selected(int new_correction)
{
 SYS_DEBUG_MEMBER(DM_QT);

 correction = new_correction;
 update_filter();

 switch (new_correction) {
    case 0:
        button->setVisible(false);
    break;

    default:
        button->setVisible(true);
    break;
 }
}

void FilterSelectorAmplifier::inversion(bool is_inverted)
{
 SYS_DEBUG_MEMBER(DM_QT);

 inverted = is_inverted;
 update_filter();
}

void FilterSelectorAmplifier::update_filter(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 FILTER::FilterTypes filter = FILTER::FILTER_NONE;

 switch (correction) {
    case 0:
        filter = FILTER::FILTER_NONE;
    break;
    case 1:
        filter = inverted ? FILTER::FILTER_RIAA_INVERTED : FILTER::FILTER_RIAA_NORMAL;
    break;
    case 2:
        filter = inverted ? FILTER::FILTER_PINK_INVERTED : FILTER::FILTER_PINK_NORMAL;
    break;
 }

 setFilterType(filter);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
