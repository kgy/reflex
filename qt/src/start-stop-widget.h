/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_START_STOP_WIDGET_H_INCLUDED__
#define __QT_SRC_START_STOP_WIDGET_H_INCLUDED__

#include <QHBoxLayout>

#include <Debug/Debug.h>

#include <QLabel>
#include <string>

class MyMeasurementWindow;
class QPushButton;
class QComboBox;

SYS_DECLARE_MODULE(DM_QT);

class StartStopWidget: public QHBoxLayout
{
    Q_OBJECT

 public:
    StartStopWidget(MyMeasurementWindow & parent);
    virtual ~StartStopWidget();

    void Stopped(void);
    void setFocus(void);
    bool isGeneratorNeeded(void) const;
    std::string getInputLoadFileName(void) const;
    std::string getInputSaveFileName(void) const;
    std::string getOutputLoadFileName(void) const;
    std::string getOutputSaveFileName(void) const;

 protected:
    class Text;
    friend class Text;

    void FilenamePress(int mdoe);
    void EnableInputs(bool en);
    void enable_start(void);

    class Text: public QLabel
    {
     public:
        Text(QWidget * widget, StartStopWidget & parent, int mode);

        bool ok(void) const;
        std::string getFilename(void) const;

     protected:
        StartStopWidget & parent;

        int mode;

        virtual void mousePressEvent(QMouseEvent *) override;

     private:
        SYS_DEFINE_CLASS_NAME("StartStopWidget::Text");

        static QString initial_text;

    }; // class StartStopWidget::Text

    struct InOutFiles
    {
        InOutFiles(QWidget * widget, StartStopWidget & parent, int mode);

        QWidget * widget;

        StartStopWidget & parent;

        Text * load_name;

        Text * save_name;

        bool is_save_mode;

        void Load(void);
        void Save(void);
        void Setup(void);
        void setVisible(bool visible);
        std::string getLoadName(void) const;
        std::string getSaveName(void) const;

     private:
        SYS_DEFINE_CLASS_NAME("StartStopWidget::InOutFiles");

    }; // struct StartStopWidget::InOutFiles

    MyMeasurementWindow & parent;

    QLabel * label2;

    QPushButton * start;
    QPushButton * stop;
    QComboBox * input;
    QComboBox * output;

    InOutFiles infile;
    InOutFiles outfile;

 private:
    SYS_DEFINE_CLASS_NAME("StartStopWidget");

 private slots:
    void startButton(void);
    void stopButton(void);
    void inputActivated(int index);
    void outputActivated(int index);

}; // class StartStopWidget

#endif /* __QT_SRC_START_STOP_WIDGET_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
