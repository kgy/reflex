/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_METER_H_INCLUDED__
#define __QT_SRC_METER_H_INCLUDED__

#include <Debug/Debug.h>
#include <Exceptions/Exceptions.h>

#include <QWidget>
#include <QGraphicsView>
#include <QLabel>
#include <QHBoxLayout>

class QGraphicsScene;
class QGraphicsRectItem;

SYS_DECLARE_MODULE(DM_QT);

namespace PLOT
{
    class AnalyzeBase;
}

/// VU-meter class
class Meter: public QGraphicsView
{
    Q_OBJECT;

    using super = QGraphicsView;

 public:
    Meter(unsigned int w, unsigned int h, float low, float high, float step, QWidget * parent = nullptr);
    virtual ~Meter();

    void set(float level);

 protected:
    Meter(unsigned int w, unsigned int h, float low, float high, QWidget * parent = nullptr);

    struct draw_color {
        float position;
        unsigned char r;
        unsigned char g;
        unsigned char b;
        unsigned char a;
    };

    void Initialize(float step, const draw_color * color_table);

    QGraphicsScene * scene;

    QGraphicsRectItem * mask;

    float low;

    float high;

    unsigned int width;

    unsigned int height;

 signals:
    void setPosition(float position);

 private slots:
    void doSetPosition(float position);

 private:
    SYS_DEFINE_CLASS_NAME("Meter");

    static const draw_color init_data[];

}; // class Meter

/// Like \ref Meter but can be adjusted by mouse drag
class PotMeter: public Meter
{
 public:
    PotMeter(unsigned int w, unsigned int h, float low, float high, float step, QWidget * parent = nullptr);
    virtual ~PotMeter();

    inline float getValue(void) const
    {
        return currentValue;
    }

 protected:
    virtual void valueChanged(float) const
    {
    }

    float currentValue;

 private:
    SYS_DEFINE_CLASS_NAME("PotMeter");

    static const draw_color init_data[];

    void mousePressEvent(QMouseEvent * event) override;
    void mouseMoveEvent(QMouseEvent * event) override;
    void setNewPosition(int position);

}; // class PotMeter

class MyMeters: public QHBoxLayout
{
    Q_OBJECT;

    using super = QHBoxLayout;

 public:
    MyMeters(QWidget * parent = nullptr);
    virtual ~MyMeters();

    void addStretchArea(void);
    void MeasurementTypeSelected(PLOT::AnalyzeBase * analyzer);

    Meter & operator[](unsigned int index)
    {
        SYS_DEBUG_MEMBER(DM_QT);
        ASSERT_DBG(index < 3, "index out of range");
        return *meters[index];
    }

 protected:
    Meter * meters[3];

    QLabel * labels[3];

 private:
    SYS_DEFINE_CLASS_NAME("MyMeters");

}; // class MyMeters

#endif /* __QT_SRC_METER_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
