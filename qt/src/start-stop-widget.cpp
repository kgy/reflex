/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "start-stop-widget.h"

#include <measurement-window.h>

#include <QPushButton>
#include <QComboBox>
#include <QFileDialog>

QString StartStopWidget::Text::initial_text = "no file selected yet";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *      class StartStopWidget:                                                           *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

StartStopWidget::StartStopWidget(MyMeasurementWindow & parent):
    parent(parent),
    label2(nullptr),
    start(nullptr),
    stop(nullptr),
    input(nullptr),
    output(nullptr),
    infile(&parent, *this, 2),
    outfile(&parent, *this, 0)
{
 SYS_DEBUG_MEMBER(DM_QT);

 start = new QPushButton("Start", &parent);
 addWidget(start);
 setStretchFactor(start, 0);
 start->setEnabled(true);

 stop = new QPushButton("Stop", &parent);
 addWidget(stop);
 setStretchFactor(stop, 0);
 stop->setEnabled(false);

 auto label = new QLabel("Output:");
 addWidget(label);
 setStretchFactor(label, 0);

 output = new QComboBox();
 output->insertItem(0, "Measure only");
 output->insertItem(1, "Measure and save to file:");
 output->insertItem(2, "Analyze from file:");
 addWidget(output);
 setStretchFactor(output, 0);

 outfile.Setup();

 label2 = new QLabel("Input:");
 addWidget(label2);
 setStretchFactor(label2, 0);

 input = new QComboBox();
 input->insertItem(0, "Internal generator");
 input->insertItem(1, "Internal generator and save to file:");
 input->insertItem(2, "External signal source");
 input->insertItem(3, "Read from file:");
 addWidget(input);
 setStretchFactor(input, 0);

 infile.Setup();

 QWidget * qw = new QWidget();
 addWidget(qw);
 setStretchFactor(qw, 1);

 start->setFocus(Qt::MouseFocusReason);

 connect(start, &QPushButton::released, this, &StartStopWidget::startButton);
 connect(stop, &QPushButton::released, this, &StartStopWidget::stopButton);
 // I do not use QOverload, because it is only in QT >= 5.7
 connect(output, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), this, &StartStopWidget::outputActivated);
 connect(input, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), this, &StartStopWidget::inputActivated);
}

StartStopWidget::~StartStopWidget()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

bool StartStopWidget::isGeneratorNeeded(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (input->currentIndex()) {
    case 0:
    case 1:
        return true;
    break;
 }

 return false;
}

void StartStopWidget::setFocus(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 start->setFocus(Qt::ActiveWindowFocusReason);
}

void StartStopWidget::EnableInputs(bool en)
{
 SYS_DEBUG_MEMBER(DM_QT);

 label2->setVisible(en);
 input->setVisible(en);

 if (en) {
    switch (input->currentIndex()) {
        case 1:
            infile.load_name->setVisible(false);
            infile.save_name->setVisible(true);
        break;

        case 3:
            infile.save_name->setVisible(false);
            infile.load_name->setVisible(true);
        break;

        default:
            infile.load_name->setVisible(false);
            infile.save_name->setVisible(false);
        break;
    }
 } else {
    infile.load_name->setVisible(false);
    infile.save_name->setVisible(false);
 }
}

void StartStopWidget::startButton(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 bool ok = parent.startButton();
 stop->setEnabled(ok);
 start->setEnabled(!ok);
 if (ok) {
    stop->setFocus(Qt::ActiveWindowFocusReason);
 } else {
    start->setFocus(Qt::ActiveWindowFocusReason);
 }
}

void StartStopWidget::stopButton(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.stopButton();
}

void StartStopWidget::Stopped(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 stop->setEnabled(false);
 start->setEnabled(true);
 start->setFocus(Qt::ActiveWindowFocusReason);
}

void StartStopWidget::FilenamePress(int mode)
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (mode) {
    case 0:
    {
        // Load from the file:
        QString name = QFileDialog::getOpenFileName(&parent, tr("Load Out from Waveform File"), "out.flac", tr("Free Lossless Audio Codec (*.flac)"));
        const char * filename = name.toUtf8().data();
        outfile.load_name->setText(filename);
    }
    break;

    case 1:
    {
        // Save to the file:
        QString name = QFileDialog::getSaveFileName(&parent, tr("Save Out to Waveform File"), "in.flac", tr("Free Lossless Audio Codec (*.flac)"));
        const char * filename = name.toUtf8().data();
        outfile.save_name->setText(filename);
    }
    break;

    case 2:
    {
        // Load from the file:
        QString name = QFileDialog::getOpenFileName(&parent, tr("Load Input from Waveform File"), "out.flac", tr("Free Lossless Audio Codec (*.flac)"));
        const char * filename = name.toUtf8().data();
        infile.load_name->setText(filename);
    }
    break;

    case 3:
    {
        // Save to the file:
        QString name = QFileDialog::getSaveFileName(&parent, tr("Save Input to Waveform File"), "in.flac", tr("Free Lossless Audio Codec (*.flac)"));
        const char * filename = name.toUtf8().data();
        infile.save_name->setText(filename);
    }
    break;

    default:
        return;
    break;
 }

 enable_start();
}

void StartStopWidget::inputActivated(int index)
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (index) {
    case 0:
        infile.Save();
        infile.setVisible(false);
    break;

    case 1:
        infile.Save();
        infile.setVisible(true);
    break;

    case 2:
        infile.Save();
        infile.setVisible(false);
    break;

    case 3:
        infile.Load();
        infile.setVisible(true);
    break;

    default:
        return;
    break;
 }

 enable_start();
}

void StartStopWidget::outputActivated(int index)
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (index) {
    case 0:
        outfile.Save();
        outfile.setVisible(false);
        EnableInputs(true);
    break;

    case 1:
        outfile.Save();
        outfile.setVisible(true);
        EnableInputs(true);
    break;

    case 2:
        outfile.Load();
        outfile.setVisible(true);
        EnableInputs(false);
    break;

    default:
        return;
    break;
 }

 enable_start();
}

void StartStopWidget::enable_start(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 if (stop->isEnabled()) {
    return;
 }

 switch (output->currentIndex()) {
    case 0:
        // Nothing to do here
    break;

    case 1:
        if (!outfile.save_name->ok()) {
            start->setEnabled(false);
            return;
        }
    break;

    case 2:
        start->setEnabled(outfile.load_name->ok());
        return;
    break;

    default:
        start->setEnabled(false);
        return;
    break;
 }

 switch (input->currentIndex()) {
    case 0:
        start->setEnabled(true);
    break;

    case 1:
        start->setEnabled(infile.save_name->ok());
    break;

    case 2:
        start->setEnabled(true);
    break;

    case 3:
        start->setEnabled(infile.load_name->ok());
    break;

    default:
        start->setEnabled(false);
    break;
 }
}

std::string StartStopWidget::getInputLoadFileName(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (input->currentIndex()) {
    case 3:
        return infile.getLoadName();
    break;
 }

 return "";
}

std::string StartStopWidget::getInputSaveFileName(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (input->currentIndex()) {
    case 1:
        return infile.getSaveName();
    break;
 }

 return "";
}

std::string StartStopWidget::getOutputLoadFileName(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (output->currentIndex()) {
    case 2:
        return outfile.getLoadName();
    break;
 }

 return "";
}

std::string StartStopWidget::getOutputSaveFileName(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (output->currentIndex()) {
    case 1:
        return outfile.getSaveName();
    break;
 }

 return "";
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *      class StartStopWidget::Text:                                                     *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

StartStopWidget::Text::Text(QWidget * widget, StartStopWidget & parent, int mode):
    QLabel(widget),
    parent(parent),
    mode(mode)
{
 SYS_DEBUG_MEMBER(DM_QT);

 setFrameStyle(QFrame::Panel | QFrame::Sunken);
 setWordWrap(true);
 setTextFormat(Qt::PlainText);
 setText(initial_text);
}

void StartStopWidget::Text::mousePressEvent(QMouseEvent *)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.FilenamePress(mode);
}

bool StartStopWidget::Text::ok(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 QString t = text();

 return !t.isEmpty() && t != initial_text;
}

std::string StartStopWidget::Text::getFilename(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 std::string st;

 if (ok()) {
    QString t = text();
    st = t.toStdString();
 }

 return st;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *      class StartStopWidget::InOutFiles:                                               *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

StartStopWidget::InOutFiles::InOutFiles(QWidget * widget, StartStopWidget & parent, int mode):
    widget(widget),
    parent(parent),
    load_name(new Text(widget, parent, mode)),
    save_name(new Text(widget, parent, mode+1)),
    is_save_mode(false)
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void StartStopWidget::InOutFiles::Setup(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 parent.addWidget(load_name);
 parent.setStretchFactor(load_name, 100);

 parent.addWidget(save_name);
 parent.setStretchFactor(save_name, 100);

 setVisible(false);
}

void StartStopWidget::InOutFiles::setVisible(bool visible)
{
 SYS_DEBUG_MEMBER(DM_QT);

 if (!visible) {
    load_name->setVisible(false);
    save_name->setVisible(false);
    return;
 }

 if (is_save_mode) {
    load_name->setVisible(false);
    save_name->setVisible(true);
 } else {
    load_name->setVisible(true);
    save_name->setVisible(false);
 }
}

void StartStopWidget::InOutFiles::Load(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 is_save_mode = false;
 load_name->setVisible(true);
 save_name->setVisible(false);
}

void StartStopWidget::InOutFiles::Save(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 is_save_mode = true;
 load_name->setVisible(false);
 save_name->setVisible(true);
}

std::string StartStopWidget::InOutFiles::getLoadName(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 std::string st;

 if (!is_save_mode) {
    st = load_name->getFilename();
 }

 return st;
}

std::string StartStopWidget::InOutFiles::getSaveName(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 std::string st;

 if (is_save_mode) {
    st = save_name->getFilename();
 }

 return st;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
