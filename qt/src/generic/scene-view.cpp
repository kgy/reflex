/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "scene-view.h"

#include <QGraphicsScene>
#include <QGraphicsView>

SceneView::SceneView(QObject * parent):
    scene(new QGraphicsScene(parent)),
    view(new QGraphicsView(scene))
{
 SYS_DEBUG_MEMBER(DM_QT);

 view->show();
}

SceneView::~SceneView()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
