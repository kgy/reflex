/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_GENERIC_SCENE_VIEW_H_INCLUDED__
#define __QT_SRC_GENERIC_SCENE_VIEW_H_INCLUDED__

#include <Debug/Debug.h>

SYS_DECLARE_MODULE(DM_QT);

class QObject;
class QGraphicsScene;
class QGraphicsView;

class SceneView
{
 public:
    SceneView(QObject * parent = nullptr);
    virtual ~SceneView();

    QGraphicsScene * getScene(void)
    {
        return scene;
    }

 protected:
    QGraphicsScene * scene;

    QGraphicsView * view;

 private:
    SYS_DEFINE_CLASS_NAME("SceneView");

}; // class SceneView

#endif /* __QT_SRC_GENERIC_SCENE_VIEW_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
