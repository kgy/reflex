/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "meter.h"

#include <measures/params.h>
#include <measures/analyze-base.h>

#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QGraphicsRectItem>
#include <QLinearGradient>
#include <QBrush>
#include <QPen>
#include <QMouseEvent>

#include <math.h>

#include <sstream>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class Meter:                                                                    *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const Meter::draw_color Meter::init_data[] = {
    /* Position: R:   G:   B:   A:  */
    {   0.000,  150, 150, 150, 255  },
    {   0.080,  180, 180, 180, 255  },
    {   0.110,  120, 120, 255, 255  },
    {   0.390,  150, 150, 255, 255  },
    {   0.420,   20, 200,  20, 255  },
    {   0.700,    0, 190,   0, 255  },
    {   0.740,  200, 230,  20, 255  },
    {   0.820,  230, 230,  20, 255  },
    {   0.860,  255,  80,  80, 255  },
    {   1.000,  255,   0,   0, 255  },
    {  -1.000,    0,   0,   0,   0  }
};

Meter::Meter(unsigned int w, unsigned int h, float low, float high, float step, QWidget * parent):
    Meter(w, h, low, high, parent)
{
 SYS_DEBUG_MEMBER(DM_QT);

 Initialize(step, init_data);
}

Meter::Meter(unsigned int w, unsigned int h, float low, float high, QWidget * parent):
    QGraphicsView(new QGraphicsScene(parent)),
    scene(super::scene()),
    mask(nullptr),
    low(low),
    high(high),
    width(w),
    height(h)
{
 SYS_DEBUG_MEMBER(DM_QT);

 // This object must have fixed size, in all cases:
 setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
 setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
 setFixedSize(w, h);
 setSceneRect(QRectF(0, 0, w, h));

 QObject::connect(this, &Meter::setPosition, this, &Meter::doSetPosition);
}

Meter::~Meter()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void Meter::Initialize(float step, const draw_color * color_table)
{
 SYS_DEBUG_MEMBER(DM_QT);

 QLinearGradient g(QPointF(0,0), QPointF(width,0));
 for (unsigned int i = 0; color_table[i].position >= 0.0f; ++i) {
    const draw_color & p = color_table[i];
    g.setColorAt(p.position, QColor(p.r, p.g, p.b, p.a));
 }
 scene->setBackgroundBrush(QBrush(g));

 mask = scene->addRect(QRectF(0,0,width,height), QColor(255,255,255,255), QBrush(QColor(255,255,255,255)));

 float start = floorf(low/step) * step;
 for (float n = start; n <= high; n += step) {
    float rel_pos = (n-low) / (high-low);
    if (rel_pos < 0.1f || rel_pos > 0.9f) {
        continue; // Do not display it too close to edges
    }
    unsigned int position = (unsigned int)(rel_pos * (float)width + 0.5f);
    QString s = QString("%1").arg(fabsf(n));
    auto t = scene->addText(s, QFont("Arial", 8));
    auto rect = t->boundingRect();
    QColor text_color(0,0,0,255);
    t->setDefaultTextColor(text_color);
    t->setPos(position-rect.width()/2, height/2-rect.height()/2);
    QPen p(text_color);
    scene->addLine(position, 0, position, 5, p);
    scene->addLine(position, height, position, height-7, p);
 }
}

void Meter::doSetPosition(float position)
{
 SYS_DEBUG_MEMBER(DM_QT);

 if (!mask) {
    return;
 }

 mask->setRect(position, 0.0, width-position, height);
 mask->update(0, 0, width, height);
}

void Meter::set(float level)
{
 SYS_DEBUG_MEMBER(DM_QT);

 float position = (float)width * (level-low) / (high-low);
 setPosition(position);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class PotMeter:                                                                 *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const Meter::draw_color PotMeter::init_data[] = {
    /* Position: R:   G:   B:   A:  */
    {   0.000,  120, 120, 255, 255  },
    {   0.100,  150, 150, 255, 255  },
    {   0.125,   20, 200,  20, 255  },
    {   0.875,    0, 190,   0, 255  },
    {   0.900,  200, 230,  20, 255  },
    {   1.000,  230, 230,  20, 255  },
    {  -1.000,    0,   0,   0,   0  }
};

PotMeter::PotMeter(unsigned int w, unsigned int h, float low, float high, float step, QWidget * parent):
    Meter(w, h, low, high, parent),
    currentValue(low)
{
 SYS_DEBUG_MEMBER(DM_QT);

 Initialize(step, init_data);
}

PotMeter::~PotMeter()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void PotMeter::mousePressEvent(QMouseEvent * event)
{
 SYS_DEBUG_MEMBER(DM_QT);

 setNewPosition(event->x());
}

void PotMeter::mouseMoveEvent(QMouseEvent * event)
{
 SYS_DEBUG_MEMBER(DM_QT);

 if (event->buttons() & Qt::LeftButton) {
    setNewPosition(event->x());
 }
}

void PotMeter::setNewPosition(int position)
{
 SYS_DEBUG_MEMBER(DM_QT);

 currentValue = low + (high-low) * (float)position / (float)width;
 if (currentValue < low) {
    currentValue = low;
 } else if (currentValue > high) {
    currentValue = high;
 }

 set(currentValue);
 valueChanged(currentValue);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MyMeters:                                                                 *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MyMeters::MyMeters(QWidget * parent):
    super(parent)
{
 SYS_DEBUG_MEMBER(DM_QT);

 for (int i = 0; i < 3; ++i) {
    labels[i] = new QLabel("???");
    addWidget(labels[i]);
    setStretchFactor(labels[i], 0);
    meters[i] = new Meter(240, 36, -40.0f, 0.0f, 6.0f);
    addWidget(meters[i]);
    setStretchFactor(meters[i], 0);
 }
}

MyMeters::~MyMeters()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MyMeters::addStretchArea(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 QWidget * w = new QWidget;
 addWidget(w);
 setStretchFactor(w, 1);
}

void MyMeters::MeasurementTypeSelected(PLOT::AnalyzeBase * analyzer)
{
 SYS_DEBUG_MEMBER(DM_QT);

 const PLOT::Params & params = analyzer->getPlotParameters();

 for (int i = 0; i < 3; ++i) {
    if (params.channels[i] == SOUND_CONNECTION_UNUSED) {
        continue;
    }
    std::stringstream s;
    s << params.channels[i] << ":";
    labels[i]->setText(s.str().c_str());
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
