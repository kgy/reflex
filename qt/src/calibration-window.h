/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_CALIBRATION_WINDOW_H_INCLUDED__
#define __QT_SRC_CALIBRATION_WINDOW_H_INCLUDED__

#include <sound-io-connection.h>
#include <meter.h>
#include <measures/analyze-base.h>
#include <measure-controller.h>
#include <my-customplot.h>
#include <Debug/Debug.h>

#include <measurement-widget.h>

#include <QWidget>

class MainWindow;
class QRadioButton;
class QLineEdit;

class MyCalibrationWindow: public MeasureController
{
    Q_OBJECT

 public:
    MyCalibrationWindow(MainWindow & parent);
    virtual ~MyCalibrationWindow();

    void HardwareTypeSelected(MeasurementTypes type);
    void enter();
    void leave();

    virtual float getGeneratorLevel(void) const override
    {
        return generator_level;
    }

 protected:
    class MyMeasurementWidget: public MeasurementWidget
    {
     public:
        MyMeasurementWidget(PLOT::AnalyzeBase & mb, MeasureController & parent);

     private:
        SYS_DEFINE_CLASS_NAME("MyCalibrationWindow::MyMeasurementWidget");

    }; // class MyCalibrationWindow::MyMeasurementWidget

    class MyPoti;
    friend class MyPoti;

    void generatorLevelChanged(float dBLevel);

    class MyPoti: public PotMeter
    {
     public:
        MyPoti(MyCalibrationWindow & parent):
            PotMeter(240, 36, -40.0f, 0.0f, 6.0f),
            parent(parent)
        {
        }

     protected:
        MyCalibrationWindow & parent;

        virtual void valueChanged(float level) const override
        {
            parent.generatorLevelChanged(level);
        }

     private:
        SYS_DEFINE_CLASS_NAME("MyCalibrationWindow::MyPoti");

    }; // class MyCalibrationWindow::MyPoti

    virtual SND::DeviceInfo & getDeviceInfo(void) override;
    virtual GENERATOR::GeneratorType getGeneratorType(void) const override;
    virtual void MeasureStarted(void) override;
    virtual void MeasureStopped(void) override;
    virtual void externalException(const char * module, const char * message) override;
    virtual void ProcessInput(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[]) override;
    virtual void CalculateResult(PLOT::PlotData & p, unsigned int sampling_rate) override;
    virtual void MeasurementTypeSelected(void) override;

    void generatorSwitch(bool on);

    MainWindow & parent;

    QRadioButton * generator_button;

    MyMeters * meters;

    QLineEdit * line;

    float generator_level;

 private:
    SYS_DEFINE_CLASS_NAME("MyCalibrationWindow");

    void changedResistorValue(const char * text);

}; // class MyCalibrationWindow

#endif /* __QT_SRC_CALIBRATION_WINDOW_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
