/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __QT_SRC_DEVICE_SELECTOR_H_INCLUDED__
#define __QT_SRC_DEVICE_SELECTOR_H_INCLUDED__

#include <QComboBox>

#include <load-config.h>

#include <device-interface.h>
#include <Debug/Debug.h>

SYS_DECLARE_MODULE(DM_QT);

Q_DECLARE_METATYPE(SND::Device*);

class DevSelectorBase;
class InputSelector;
class OutputSelector;
class DevSelectorInterface;

class DevSelectorBase: public QComboBox, public ConfigLoader
{
 protected:
    DevSelectorBase(DevSelectorInterface & parent, const char * config_path);
    virtual ~DevSelectorBase();

    virtual void focusOutEvent(QFocusEvent * event) override;

    Auton<SND::DeviceHandler> dev;

    DevSelectorInterface & parent;

 private:
    SYS_DEFINE_CLASS_NAME("DevSelectorBase");

}; // class DevSelectorBase

class InputSelector: public DevSelectorBase
{
 public:
    InputSelector(DevSelectorInterface & parent);
    virtual ~InputSelector();

    void SaveConfig(void);

 protected:
    virtual void ConfigLoaded(const char * key, const ConfExpression & value) override;

 private:
    SYS_DEFINE_CLASS_NAME("InputSelector");

}; // class InputSelector

class OutputSelector: public DevSelectorBase
{
 public:
    OutputSelector(DevSelectorInterface & parent);
    virtual ~OutputSelector();

    void SaveConfig(void);

 protected:
    virtual void ConfigLoaded(const char * key, const ConfExpression & value) override;

 private:
    SYS_DEFINE_CLASS_NAME("OutputSelector");

}; // class OutputSelector

#endif /* __QT_SRC_DEVICE_SELECTOR_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
