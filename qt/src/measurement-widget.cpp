/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measurement-widget.h"

#include <measure-controller.h>
#include <measures/mouse-info.h>

#include <QVBoxLayout>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MeasurementWidget:                                                        *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MeasurementWidget::MeasurementWidget(PLOT::AnalyzeBase & mb, MeasureController & parent):
    mb(mb),
    parent(parent),
    myplot(new PLOT::MyCustomPlot(*this, mb.getPlotParameters()))
{
 SYS_DEBUG_MEMBER(DM_QT);
}

MeasurementWidget::~MeasurementWidget()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MeasurementWidget::Initialize(QVBoxLayout * layout)
{
 SYS_DEBUG_MEMBER(DM_QT);

 layout->addWidget(myplot);
 layout->setStretchFactor(myplot, 0);

 setLayout(layout);
}

void MeasurementWidget::On(PLOT::AnalyzeBase * measure, bool isOn)
{
 SYS_DEBUG_MEMBER(DM_QT);

 setVisible(isOn);
 if (isOn) {
    parent.SelectType(measure, this);
 }
}

void MeasurementWidget::cursorMove(double freq, const MouseInfo & info)
{
 SYS_DEBUG_MEMBER(DM_QT);

 switch (info.section) {
    case MouseInfo::DRAW_SECTION_AMPLITUDE:
        myplot->cursorMoveLeft(info);
    break;

    case MouseInfo::DRAW_SECTION_DIAGRAMS:
        parent.cursorMove(freq, info);
    break;

    case MouseInfo::DRAW_SECTION_ANGLE:
        myplot->cursorMoveRight(info);
    break;

    default:
    break;
 }
}

void MeasurementWidget::setFilterType(FILTER::FilterTypes t)
{
 SYS_DEBUG_MEMBER(DM_QT);

 mb.setFilterType(t);
 parent.Replot();
}

void MeasurementWidget::setAlignmentMode(FILTER::AlignmentModes m)
{
    mb.setAlignmentMode(m);
    parent.Replot();
}

void MeasurementWidget::setSmoothFactor(int s)
{
 SYS_DEBUG_MEMBER(DM_QT);

 mb.setSmoothFactor(s);
 parent.Replot();
}

void MeasurementWidget::setGeneratorType(GENERATOR::GeneratorType t)
{
 SYS_DEBUG_MEMBER(DM_QT);

 mb.setGeneratorType(t);
}

void MeasurementWidget::setFFTMode(bool mode)
{
 SYS_DEBUG_MEMBER(DM_QT);

 mb.setFFTMode(mode);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
