/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "calibration-window.h"

#include <Base/Parser.h>

#include <main-window.h>
#include <measures/analyze-base.h>
#include <measure-response-fft.h>

#include <QVBoxLayout>
#include <QLabel>
#include <QRadioButton>
#include <QLineEdit>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MyCalibrationWindow:                                                      *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MyCalibrationWindow::MyCalibrationWindow(MainWindow & parent):
    MeasureController(parent),
    parent(parent),
    generator_button(nullptr),
    generator_level(0.0f)
{
 SYS_DEBUG_MEMBER(DM_QT);

 QVBoxLayout * layout = new QVBoxLayout;

 {
    QHBoxLayout * l1 = new QHBoxLayout;

    QLabel * label1 = new QLabel(tr("Generator Output Level:"));
    l1->addWidget(label1);
    l1->setStretchFactor(label1, 0);

    auto p = new MyPoti(*this);
    l1->addWidget(p);
    l1->setStretchFactor(p, 0);

    generator_button = new QRadioButton("On", this);
    l1->addWidget(generator_button);
    l1->setStretchFactor(generator_button, 0);

    connect(generator_button, &QRadioButton::toggled, [=](bool state){ generatorSwitch(state); });

    auto w = new QWidget;
    l1->addWidget(w);
    l1->setStretchFactor(w, 1);

    layout->addLayout(l1);
    layout->setStretchFactor(l1, 0);
 }

 {
    QHBoxLayout * l2 = new QHBoxLayout;

    meters = new MyMeters;
    meters->addStretchArea();
    l2->addLayout(meters);
    l2->setStretchFactor(meters, 0);

    layout->addLayout(l2);
    layout->setStretchFactor(l2, 0);
 }

 {
    QHBoxLayout * l3 = new QHBoxLayout;

    QLabel * label1 = new QLabel(tr("Reference Resistor:"));

    l3->addWidget(label1);
    l3->setStretchFactor(label1, 0);

    line = new QLineEdit;
    l3->addWidget(line);
    l3->setStretchFactor(line, 0);
    connect(line, &QLineEdit::textChanged, [=](const QString & text) {
        changedResistorValue(text.toUtf8().data());
    });

    QWidget * w = new QWidget;
    l3->addWidget(w);
    l3->setStretchFactor(w, 1);

    layout->addLayout(l3);
    layout->setStretchFactor(l3, 0);
 }

 {
    auto f = [=](PLOT::AnalyzeBase & mb) {
        MyMeasurementWidget * plot = new MyMeasurementWidget(mb, *this);
        mb.addWindow(plot);
        layout->addWidget(plot);
        layout->setStretchFactor(plot, 1);
    };
    PLOT::AnalyzeBase::Scan(f); // Assigns MeasurementWidget instance for each existing measurement type
 }

 setLayout(layout);
}

MyCalibrationWindow::~MyCalibrationWindow()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MyCalibrationWindow::changedResistorValue(const char * text)
{
 SYS_DEBUG_MEMBER(DM_QT);

 double value = 1.0;

 try {
    if (*text) {
        value = Parser::StrtodSafe(text);
    }

    QPalette palette;
    palette.setColor(QPalette::Base, Qt::green);
    palette.setColor(QPalette::Text, Qt::black);
    line->setPalette(palette);

 } catch (...) {
    QPalette palette;
    palette.setColor(QPalette::Base, Qt::red);
    palette.setColor(QPalette::Text, Qt::black);
    line->setPalette(palette);
 }

 PLOT::AnalyzeBase::setReferenceResistorValue(value);
}

void MyCalibrationWindow::MeasurementTypeSelected(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 meters->MeasurementTypeSelected(measure_analyzer);
}

void MyCalibrationWindow::HardwareTypeSelected(MeasurementTypes /*type*/)
{
 SYS_DEBUG_MEMBER(DM_QT);
}

/// This function is called when the Calibration Window is just entered
void MyCalibrationWindow::enter(void)
{
 SYS_DEBUG_MEMBER(DM_QT);
}

/// This function is called when the Calibration Window is just left
void MyCalibrationWindow::leave(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 generator_button->setChecked(false);   // Switch off the generator
}

void MyCalibrationWindow::generatorSwitch(bool on)
{
 SYS_DEBUG_MEMBER(DM_QT);

 if (on) {
    if (!MeasureStart(true)) {
        generator_button->setChecked(false);   // Switch off the generator
    }
 } else {
    MeasureStop();
 }
}

void MyCalibrationWindow::generatorLevelChanged(float dBLevel)
{
 SYS_DEBUG_MEMBER(DM_QT);

 generator_level = exp10f(dBLevel/20.0f);

 if (actual_measure) {
    actual_measure->setOutputLevel(generator_level);
 }
}

SND::DeviceInfo & MyCalibrationWindow::getDeviceInfo(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 return parent.getDeviceInfo();
}

GENERATOR::GeneratorType MyCalibrationWindow::getGeneratorType(void) const
{
 SYS_DEBUG_MEMBER(DM_QT);

 return GENERATOR::GENERATOR_WHITE_NOISE;   // Always uses white noise
}

void MyCalibrationWindow::MeasureStarted(void)
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void MyCalibrationWindow::MeasureStopped(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 generator_button->setChecked(false);   // Switch off the generator
}

void MyCalibrationWindow::ProcessInput(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[])
{
 measure_analyzer->DoCalibrate(data, p, channel_map);
}

void MyCalibrationWindow::CalculateResult(PLOT::PlotData & p, unsigned int sampling_rate)
{
 measure_analyzer->CalibrationResult(p, sampling_rate);

 float volumes[3];
 measure_analyzer->getVolumes(volumes, 3U);

 (*meters)[0].set(volumes[0]);
 (*meters)[1].set(volumes[1]);
 (*meters)[2].set(volumes[2]);
}

void MyCalibrationWindow::externalException(const char * module, const char * message)
{
 SYS_DEBUG_MEMBER(DM_QT);

 std::cerr << "Calibration: exception in module '" << module << "': '" << message << "'" << std::endl;

 parent.externalException(module, message);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *       class MyCalibrationWindow::MyMeasurementWidget:                                 *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MyCalibrationWindow::MyMeasurementWidget::MyMeasurementWidget(PLOT::AnalyzeBase & mb, MeasureController & parent):
    MeasurementWidget(mb, parent)
{
 SYS_DEBUG_MEMBER(DM_QT);

 Initialize();  // Nothing added
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
