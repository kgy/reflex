/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "device-selector.h"

#include <selector-interface.h>

SYS_DECLARE_MODULE(DM_QT);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class DevSelectorBase:                                                        *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

DevSelectorBase::DevSelectorBase(DevSelectorInterface & parent, const char * config_path):
    ConfigLoader(config_path),
    parent(parent)
{
 SYS_DEBUG_MEMBER(DM_QT);

 connect(this, static_cast<void(QComboBox::*)(int)>(&QComboBox::highlighted), [&](int index)
 {
    parent.highlightedDevice(itemData(index).value<SND::Device*>());
 });
}

DevSelectorBase::~DevSelectorBase()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void DevSelectorBase::focusOutEvent(QFocusEvent * event)
{
 SYS_DEBUG_MEMBER(DM_QT);

 QWidget::focusOutEvent(event);
 parent.highlightedDevice(nullptr);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class InputSelector:                                                          *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

InputSelector::InputSelector(DevSelectorInterface & parent):
    DevSelectorBase(parent, "/HW/soundcard/input/name")
{
 SYS_DEBUG_MEMBER(DM_QT);

 connect(this, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int index)
 {
    parent.selectedRecorderDevice(itemData(index).value<SND::Device*>());
 });

 for (int i = 0; i < dev->getRecorders(); ++i) {
    auto p = dev->getRecorder(i);
    addItem(QString(p->getName().c_str()), QVariant::fromValue(p));
 }

 SYS_DEBUG(DL_INFO1, "Having " << dev->getRecorders() << " input sound devices.");
}

InputSelector::~InputSelector()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void InputSelector::SaveConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 SaveConfigValue(currentText().toUtf8().data());
}

void InputSelector::ConfigLoaded(const char *, const ConfExpression & value)
{
 SYS_DEBUG_MEMBER(DM_QT);

 const char * conf = value.GetString().c_str();
 setCurrentText(conf);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class OutputSelector:                                                         *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

OutputSelector::OutputSelector(DevSelectorInterface & parent):
    DevSelectorBase(parent, "/HW/soundcard/output/name")
{
 SYS_DEBUG_MEMBER(DM_QT);

 connect(this, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int index)
 {
    parent.selectedPlayerDevice(itemData(index).value<SND::Device*>());
 });

 for (int i = 0; i < dev->getPlayers(); ++i) {
    auto p = dev->getPlayer(i);
    addItem(QString(p->getName().c_str()), QVariant::fromValue(p));
 }

 SYS_DEBUG(DL_INFO1, "Having " << dev->getPlayers() << " output sound devices.");
}

OutputSelector::~OutputSelector()
{
 SYS_DEBUG_MEMBER(DM_QT);
}

void OutputSelector::SaveConfig(void)
{
 SYS_DEBUG_MEMBER(DM_QT);

 SaveConfigValue(currentText().toUtf8().data());
}

void OutputSelector::ConfigLoaded(const char *, const ConfExpression & value)
{
 SYS_DEBUG_MEMBER(DM_QT);

 const char * conf = value.GetString().c_str();
 setCurrentText(conf);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
