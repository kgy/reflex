/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_QT_INCLUDE_SELECTOR_INTERFACE_H_INCLUDED__
#define __SPEAKER_TEST_QT_INCLUDE_SELECTOR_INTERFACE_H_INCLUDED__

#include <device-selector.h>
#include <speed-selector.h>

class DevSelectorBase;
class InputSelector;
class OutputSelector;
class SpeedSelectorBase;
class InputSpeedSelector;
class OutputSpeedSelector;

namespace SND
{
    class Device;

} // namespace SND

class DevSelectorInterface
{
    friend class DevSelectorBase;
    friend class InputSelector;
    friend class OutputSelector;

    virtual void highlightedDevice(const SND::Device * dev) =0;
    virtual void selectedPlayerDevice(SND::Device * dev) =0;
    virtual void selectedRecorderDevice(SND::Device * dev) =0;

}; // class DevSelectorInterface

class SpeedSelectorInterface
{
    friend class SpeedSelectorBase;
    friend class InputSpeedSelector;
    friend class OutputSpeedSelector;

    virtual void InputSpeedSelected(unsigned int speed) =0;
    virtual void OutputSpeedSelected(unsigned int speed) =0;

    SYS_DEFINE_CLASS_NAME("SpeedSelectorInterface");

}; // class SpeedSelectorInterface

class SelectorInterface: public DevSelectorInterface, public SpeedSelectorInterface
{
}; // class SelectorInterface

#endif /* __SPEAKER_TEST_QT_INCLUDE_SELECTOR_INTERFACE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
