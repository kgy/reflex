#   Root makefile for building Refleks
#

export EXE_NAME             =   reflex

# =======================================================================================

export BUILD_LIB_SO         =   0

export OPERATING_SYSTEM     =   unix

NATIVE_SUBPROJECTS          =   

export MY_CFLAGS            =   -Wall

export MY_CXXFLAGS          =   -Wall

export MY_LFLAGS            =   -lasound

COMMON_FLAGS                =   

export MY_UNDEFS            =   _load_alsa_implementation _load_config_implementation \
                                _load_speaker_measurement _load_amplifier_measurement

# =======================================================================================

include makesys/scripts/make.qt

makesys/scripts/make.qt:
	git submodule update --init --recursive

# * * * * * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * * * * * * *
