/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "main-config.h"

#include <File/Ostream.h>

char _load_config_implementation;

const char * ConfigReflex::configName = "~/.config/Reflex.conf";

AUTON_IMPLEMENT(ConfigReflex, ConfigData);

void ConfigReflex::ParseConfig(ConfigStore & conf)
{
 SYS_DEBUG_MEMBER(DM_CONFIG);

 try {
    FILES::FileMap_char configFile(configName);

    SYS_DEBUG(DL_INFO1, "Main config file: size=" << configFile.GetSize());

    ConfDriver parser(configFile, conf);

    if (parser.parse()) {
        SYS_DEBUG(DL_ERROR, "Error parsing config file " << configName << ", some settings may be incorrect.");
    }

    SYS_DEBUG(DL_INFO1, "Config file '" << configName << "' parsed.");

 } catch (std::exception & ex) {
    std::cerr << "Could not parse config file: " << ex.what() << " (ignored)" << std::endl;
 } catch (...) {
    std::cerr << "Could not parse config file '" << configName << "' due to unknown exception" << std::endl;
 }
}

void ConfigReflex::SaveConfig(ConfigStore & conf)
{
 SYS_DEBUG_MEMBER(DM_CONFIG);

 FILES::Ostream os(configName);
 os << conf;

 SYS_DEBUG(DL_INFO1, "Config file '" << configName << "' saved.");
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
