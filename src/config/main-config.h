/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_CONFIG_MAIN_CONFIG_H_INCLUDED__
#define __SRC_CONFIG_MAIN_CONFIG_H_INCLUDED__

#include <Config/MainConfig.h>

class ConfigReflex: public ConfigData
{
 public:
    virtual ~ConfigReflex()
    {
    }

 private:
    SYS_DEFINE_CLASS_NAME("ConfigReflex");

 protected:
    virtual void ParseConfig(ConfigStore & conf);
    virtual void SaveConfig(ConfigStore & conf);

    static const char * configName;

}; // class ConfigReflex

#endif /* __SRC_CONFIG_MAIN_CONFIG_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
