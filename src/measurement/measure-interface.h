/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_MEASURE_INTERFACE_H_INCLUDED__
#define __SRC_MEASUREMENT_MEASURE_INTERFACE_H_INCLUDED__

#include <generators/generator-type.h>

#include <string>

namespace SND
{
    struct DeviceInfo;
}

namespace PLOT
{
    struct Params;
}

namespace FFT
{
    class FFTBuffer;
}

class MeasureInterface
{
 public:
    virtual SND::DeviceInfo & getDeviceInfo(void) =0;
    virtual GENERATOR::GeneratorType getGeneratorType(void) const =0;
    virtual float getGeneratorLevel(void) const =0;
    virtual void MeasureStarted(void) =0;
    virtual void MeasureStopped(void) =0;
    virtual void calculateAudioFrame(const FFT::FFTBuffer & data, const int * channel_map) =0;
    virtual void externalException(const char * module, const char * message) =0;
    virtual const PLOT::Params * getPlotParameters(void) const =0;
    virtual std::string getInputLoadFileName(void) const =0;
    virtual std::string getInputSaveFileName(void) const =0;
    virtual std::string getOutputLoadFileName(void) const =0;
    virtual std::string getOutputSaveFileName(void) const =0;
    virtual bool MeasureStart(bool is_calibration) =0;
    virtual void MeasureStop(void) =0;

}; // class MeasureInterface

#endif /* __SRC_MEASUREMENT_MEASURE_INTERFACE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
