/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "analyze-base.h"

#include <Exceptions/Exceptions.h>

SYS_DEFINE_MODULE(DM_ANALYZE);

using PLOT::AnalyzeBase;

AnalyzeBase * AnalyzeBase::first = nullptr;

AnalyzeBase::AnalyzeBase(void):
    filter(nullptr),
    filter_type(FILTER::FILTER_NONE),
    generator_type(GENERATOR::GENERATOR_WHITE_NOISE),
    is_FFT_mode(false)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);

 next = first;
 first = this;
}

AnalyzeBase::~AnalyzeBase()
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);
}

// static
void AnalyzeBase::Select(MeasurementTypes type)
{
 for (AnalyzeBase * i = first; i; i = i->next) {
    i->SelectType(type);
 }
}

void AnalyzeBase::updateFilter(unsigned int samples, unsigned int rate)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);

 if (!filters || filters->getSize() != samples || filters->getSamplingRate() != rate) {
    filters = FILTER::FilterHolder::Create(samples, rate);
 }
 const FILTER::FilterBase * f = &(*filters)[filter_type];
 if (f != filter) {
    filterChanged(f);
    filter = f;
 }
}

// static:
void AnalyzeBase::Smoothing(float * dst, const float * src, unsigned int samples, unsigned int size)
{
 SYS_DEBUG_STATIC(DM_ANALYZE);

 ASSERT_DBG(samples > size, "wrong parameters for " << __FUNCTION__);

 dst[0] = src[0];   // The DC component is excluded from filtering

 float m1 = 1.0f / (float)size;
 float m2 = 1.0f - m1;
 unsigned int i = 1;
 float tmp[samples];

 float data = src[i];
 tmp[i] = data;

 while (++i < samples) {
    data = data * m2 + src[i] * m1;
    tmp[i] = data;
 }

 --i;
 data = src[i];
 dst[i] = (tmp[i] + data) * 0.5f;
 while (--i) {
    data = data * m2 + src[i] * m1;
    dst[i] = (tmp[i] + data) * 0.5f;
 }
}

// static:
void AnalyzeBase::Smoothing(float * dst, const float * src, unsigned int samples, unsigned int sampling_rate, float width)
{
 SYS_DEBUG_STATIC(DM_ANALYZE);

 dst[0] = src[0];   // The DC component is excluded from filtering

 static constexpr int ENTRIES = 1000U;

 float m1 = 0.0f;
 float m2 = 0.0f;
 int i = 0;
 unsigned int j = 0;
 int k = 0;
 unsigned int w[ENTRIES];
 float tmp[samples];
 float data = 0.0f;

 for ( ; (unsigned int)i < samples; ++i, --j) {
    if (!j) {
        ASSERT_DBG(k < ENTRIES, "too many filter entries calculated (internal error)");
        // Calculate the bandwidth parameters:
        int size = (int)((float)i * width + 0.5f);
        j = size < 1 ? 1 : (unsigned int)size;
        m1 = 1.0f / (float)j;
        m2 = 1.0f - m1;
        w[k] = j;
        ++k;
    }
    data = data * m2 + src[i] * m1;
    tmp[i] = data;
 }

 for (j = w[--k]-j; --i >= 0; --j) {
    if (!j) {
        ASSERT_DBG(k > 0, "too many filter entries calculated (internal error)");
        j = w[--k];
        // Use the stored bandwidth parameters:
        m1 = 1.0f / (float)j;
        m2 = 1.0f - m1;
    }
    data = data * m2 + src[i] * m1;
    dst[i] = (tmp[i] + data) * 0.5f;
 }
}

// static:
void AnalyzeBase::Smoothing(float * dst, const float * src, const std::vector<double> & x, float size)
{
 SYS_DEBUG_STATIC(DM_ANALYZE);

 if (x.empty()) {
    return;
 }

 float tmp[x.size()];

 int prev = 0;
 float value = src[0];
 for (int i = 0; i < (int)x.size(); ++i) {
    float difi = fabsf(x[prev]-x[i]) / (size*x[i]);
    float k1 = difi / (1.0f + difi);
    float k2 = 1.0f - k1;

    value = k1 * src[i] + k2 * value;
    tmp[i] = value;

    prev = i;
 }

 value = tmp[x.size()-1];
 for (int i = (int)x.size()-1; i >= 0; --i) {
    float difi = fabsf(x[prev]-x[i]) / (size*x[i]);
    float k1 = difi / (1.0f + difi);
    float k2 = 1.0f - k1;

    value = k1 * tmp[i] + k2 * value;
    dst[i] = value;

    prev = i;
 }
}

void AnalyzeBase::Start(bool is_calibration)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);

 spectrum.clear();
 resetFilters();

 doStart(is_calibration);
}

/// Default behavior for the X coordinates:
void AnalyzeBase::FillX(std::vector<double> & x, unsigned int size, unsigned int bandwidth)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);

 if (x.empty()) {
    x.resize(size);
    for (unsigned int i = 0; i < size; ++i) {
        x[i] = bandwidth * (double)i / (double)size;
    }
 }
}

void AnalyzeBase::DoCalibrate(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[])
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);

 static constexpr unsigned int channels = 3;

 FFT::logSpectrumArray my_fft;
 my_fft.resize(channels);

 const SoundIoConnection * my_channels = getPlotParameters().channels;

 for (unsigned int ch = 0; ch < channels; ++ch) {
    int channel_source = channel_map[my_channels[ch]];
    if (channel_source < 0) {
        SYS_DEBUG(DL_INFO2, "No input connection for channel " << ch << " (" << my_channels[ch] << "), clearing data...");
        my_fft[ch] = FFT::logSpectrum::Create(p.fft_size);
        my_fft[ch]->Clear();
    } else {
        SYS_DEBUG(DL_INFO2, "FFT[" << ch << "] on channel " << my_channels[ch] << "...");
        my_fft[ch] = FFT::logSpectrum::Create(*data.complexFFT((unsigned int)channel_source));
    }
 }

 if (spectrum.empty()) {
    spectrum.resize(channels);
 }

 for (unsigned int i = 0; i < channels; ++i) {
    spectrum[i] = my_fft[i];
 }
}

void AnalyzeBase::getVolumes(float * volumes, unsigned int size)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);

 for (unsigned int i = 0; i < spectrum.size(); ++i) {
    if (i >= size) {
        break;
    }
    volumes[i] = spectrum[i]->dBgetFullPower();
 }
}

void AnalyzeBase::doStart(bool /*is_calibration*/)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);
}

void AnalyzeBase::setReferenceResistor(float /*resistance*/)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);
}

void AnalyzeBase::CalibrationResult(PLOT::PlotData & /*p*/, unsigned int /*sampling_rate*/)
{
 SYS_DEBUG_MEMBER(DM_ANALYZE);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
