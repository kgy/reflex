/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_MEASURES_PARAMS_H_INCLUDED__
#define __SRC_MEASUREMENT_MEASURES_PARAMS_H_INCLUDED__

#include <sound-io-connection.h>
#include <filters/extended-filters.h>
#include <device-interface.h>

#include <Memory/Memory.h>

#include <stdint.h>
#include <vector>
#include <string>
#include <iostream>

enum MeasurementWindowTypes
{
    MEASURE_WINDOW_SPEAKER,
    MEASURE_WINDOW_AMPLIFIER,
    _MEASURE_WINDOW_SIZE
};

namespace PLOT
{
    enum class Unit
    {
        UNIT_UNUSED,
        UNIT_DECIBEL,
        UNIT_VOLT,
        UNIT_AMPER,
        UNIT_OHM,
        UNIT_DEGREE,
        UNIT_PERCENT,
        _UNIT_SIZE

    }; // enum PLOT::Unit

    using GraphData = std::vector<std::vector<double> >;

    class AnalyzeBase;

    struct PlotData
    {
        PlotData(unsigned int graphs, unsigned int fft_size);

        float getPosition(float freq) const;

        /// Returns plot data in string format
        std::string getLabel(unsigned int graph, float freq, PLOT::Unit unit) const;

        /// Number of graphs
        unsigned int graphs;

        /// The current size of the FFT
        unsigned int fft_size;

        /// Number of elements to be displayed
        /*! If this is -1, then the whole array is displayed. */
        int displayed_size;

        /// The X positions
        std::vector<double> x;

        /// The Y values for all graphs
        GraphData y;

    }; // struct PLOT::PlotData

    using PlotDataPtr = MEM::shared_ptr<PlotData>;

    /// Parameters for class \ref MyCustomPlot
    struct Params
    {
        MeasurementWindowTypes type;
        const char * label;
        bool logarithmic;
        bool legend;
        double left;
        double right;

        SoundIoConnection channels[32];

        unsigned int plots;

        struct info {
            bool visible;
            bool subgrid;
            const char * label;
            Unit unit;
            double min;
            double max;

        } y[2];

        struct data {
            const char * name;
            struct {
                uint8_t  r,g,b,a;
            } colour;
            int yAxis;
            Unit unit;

        } plot[9];  // For dumb compilers (including some versions of gcc) this index must be given

    }; // struct PLOT::Params

    /// Interface for class \ref MyPlot
    class Window
    {
     public:
        virtual void On(AnalyzeBase *, bool) =0;
        virtual void Replot(PLOT::PlotDataPtr &) =0;
        virtual void updateLabels(const char *, const std::string [], bool) =0;

    }; // class PLOT::Window

} // namespace PLOT

std::ostream & operator<<(std::ostream & os, PLOT::Unit u);

#endif /* __SRC_MEASUREMENT_MEASURES_PARAMS_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
