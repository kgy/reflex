/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_MEASURES_ANALYZE_BASE_H_INCLUDED__
#define __SRC_MEASUREMENT_MEASURES_ANALYZE_BASE_H_INCLUDED__

#include <sound-io-connection.h>
#include <fft-base.h>
#include <measures/params.h>
#include <filters/extended-filters.h>
#include <generators/generator-type.h>
#include <Memory/Memory.h>
#include <Debug/Debug.h>

#include <string>
#include <vector>

SYS_DECLARE_MODULE(DM_ANALYZE);

namespace PLOT
{
    class Params;

    class AnalyzeBase
    {
     public:
        AnalyzeBase(void);
        virtual ~AnalyzeBase();

        static void Select(MeasurementTypes type);

        inline void addWindow(PLOT::Window * win)
        {
            myWindow.push_back(win);
        }

        inline void Selected(bool ok)
        {
            for (auto i: myWindow) {
                i->On(this, ok);
            }
        }

        inline void resetFilters(void)
        {
            filters.reset();
            filter = nullptr;
        }

        inline void setFilterType(FILTER::FilterTypes t)
        {
            filter_type = t;
        }

        inline void setGeneratorType(GENERATOR::GeneratorType t)
        {
            generator_type = t;
        }

        inline void setFFTMode(bool mode)
        {
            is_FFT_mode = mode;
        }

        inline void setSmoothFactor(int s)
        {
            smooth_factor = s;
        }

        inline void setAlignmentMode(FILTER::AlignmentModes m)
        {
            alignment = m;
        }

        inline GENERATOR::GeneratorType getGeneratorType(void) const
        {
            return generator_type;
        }

        inline bool getFFTMode(void) const
        {
            return is_FFT_mode;
        }

        template <typename F>
        static inline void Scan(F f)
        {
            for (AnalyzeBase * i = first; i; i = i->next) {
                f(*i);
            }
        }

        static inline void setReferenceResistorValue(float resistance)
        {
            for (AnalyzeBase * i = first; i; i = i->next) {
                i->setReferenceResistor(resistance);
            }
        }

        void Start(bool is_calibration);

        virtual const Params & getPlotParameters(void) const =0;
        virtual void DoMeasure(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[]) =0;
        virtual void MeasurementResult(PLOT::PlotData & p, unsigned int sampling_rate) =0;
        virtual void SelectType(MeasurementTypes type) =0;

        virtual void FillX(std::vector<double> & x, unsigned int size, unsigned int bandwidth);
        virtual void DoCalibrate(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[]);
        virtual void CalibrationResult(PLOT::PlotData & p, unsigned int sampling_rate);
        virtual void getVolumes(float * volumes, unsigned int size);
        virtual void doStart(bool is_calibration);
        virtual void setReferenceResistor(float resistance);

     protected:
        /// Smooth the result plot with given size
        static void Smoothing(float * dst, const float * src, unsigned int samples, unsigned int size);
        static void Smoothing(float * dst, const float * src, unsigned int samples, unsigned int sampling_rate, float size);
        static void Smoothing(float * dst, const float * src, const std::vector<double> & x, float size);

        virtual void filterChanged(const FILTER::FilterBase * /*new_filter*/)
        {
            // Nothing to do by default
        }

        void updateFilter(unsigned int samples, unsigned int rate);

        static AnalyzeBase * first;

        AnalyzeBase * next;

        std::vector<PLOT::Window*> myWindow;

        FILTER::FilterHolderPtr filters;

        const FILTER::FilterBase * filter;

        FILTER::FilterTypes filter_type;

        GENERATOR::GeneratorType generator_type;

        FILTER::AlignmentModes alignment;

        FFT::logSpectrumArray spectrum;

        int smooth_factor;

        bool is_FFT_mode;

     private:
        SYS_DEFINE_CLASS_NAME("PLOT::AnalyzeBase");

    }; // class PLOT::AnalyzeBase

} // namespace PLOT

#endif /* __SRC_MEASUREMENT_MEASURES_ANALYZE_BASE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
