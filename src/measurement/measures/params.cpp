/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "params.h"

#include <third-order.h>

#include <math.h>

using PLOT::PlotData;

PlotData::PlotData(unsigned int graphs, unsigned int fft_size):
    graphs(graphs),
    fft_size(fft_size),
    displayed_size(-1)
{
 y.resize(graphs);

 for (unsigned int i = 0; i < graphs; ++i) {
    y[i].resize(fft_size);
 }
}

float PlotData::getPosition(float freq) const
{
 for (int i = 1; i < (int)x.size(); ++i) {
    if (x[i] >= freq) {
        return (float)(i-1) + (freq - (float)x[i-1]) / ((float)x[i] - (float)x[i-1]);
    }
 }

 return -1.0f;
}

std::string PlotData::getLabel(unsigned int graph, float freq, PLOT::Unit unit) const
{
 if (graph >= graphs) {
    return "--";
 }

 float position = getPosition(freq);

 if (position < 3.0f) {
    return "--";
 }

 int index = (int)position - 1;

 ThirdOrderFunction<double> f3(&y[graph][index]);
 float value = f3.get(position - (float)index);

 std::ostringstream label;

 label.unsetf(std::ios::floatfield);
 label.width(5);
 label.precision(4);

 switch (unit) {
    case PLOT::Unit::UNIT_OHM:
        label << exp10f(value/20.0f);
    break;

    case PLOT::Unit::UNIT_PERCENT:
        label << exp10f(value/20.0f) * 100.0f;
    break;

    default:
        label << value;
    break;
 }

 return label.str();
}

std::ostream & operator<<(std::ostream & os, PLOT::Unit u)
{
 switch (u) {
    case PLOT::Unit::UNIT_UNUSED:
        // Do nothing here
    break;
    case PLOT::Unit::UNIT_DECIBEL:
        os << "dB";
    break;
    case PLOT::Unit::UNIT_VOLT:
        os << 'V';
    break;
    case PLOT::Unit::UNIT_AMPER:
        os << 'A';
    break;
    case PLOT::Unit::UNIT_OHM:
        os << "Ω";
    break;
    case PLOT::Unit::UNIT_DEGREE:
        os << "°";
    break;
    case PLOT::Unit::UNIT_PERCENT:
        os << '%';
    break;
    default:
        os << "unknown";
    break;
 }
 return os;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
