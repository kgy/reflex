/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measure-amplifier.h"

#include <math.h>
#include <sstream>

#include <alloca.h>

SYS_DEFINE_MODULE(DM_MEASURE_AMP);

using PLOT::MeasureAmplifier;

MeasureAmplifier::MeasureAmplifier(void):
    peak(0.0f),
    freq_now(0U),
    k_size{0U,0U}
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);
}

MeasureAmplifier::~MeasureAmplifier(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);
}

void MeasureAmplifier::SelectType(MeasurementTypes type)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 Selected(type == MEASUREMENT_AMPLIFIER);
}

void MeasureAmplifier::doStart(bool is_calibration)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 freq_now = 0U;
 k_size[0] = 0U;
 k_size[1] = 0U;
}

void MeasureAmplifier::allocateSpectrumArray(unsigned int size, unsigned int fft_size)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 spectrum.resize(size);
 for (unsigned int i = 0; i < size; ++i) {
    if (!spectrum[i]) {
        spectrum[i] = FFT::logSpectrum::Create(fft_size);
        spectrum[i]->Clear();
    }
 }
}

void MeasureAmplifier::DoMeasure(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[])
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 // The storage order on my_fft[] is the same:
 const SoundIoConnection * my_channels = plot_params.channels;

 FFT::logSpectrumArray my_fft;
 my_fft.resize(3);

 for (unsigned int ch = 0; ch < 3; ++ch) {
    int channel_source = channel_map[my_channels[ch]];
    if (channel_source < 0) {
        SYS_DEBUG(DL_INFO2, "No input connection for channel " << ch << " (" << my_channels[ch] << "), clearing data...");
        continue;
    }
    SYS_DEBUG(DL_INFO2, "FFT[" << ch << "] on channel " << my_channels[ch] << "...");
    my_fft[ch] = FFT::logSpectrum::Create(*data.complexFFT((unsigned int)channel_source));
 }

 ASSERT_DBG(my_fft[0], "channel " << my_channels[0] << " is not connected");
 ASSERT_DBG(my_fft[1], "channel " << my_channels[1] << " is not connected");

 if (getFFTMode()) {
    spectrum.resize(2);
    spectrum[0] = my_fft[0];
    spectrum[1] = my_fft[1];
    return;
 }

 allocateSpectrumArray(5, p.fft_size);

 if (my_fft[2]) {
    my_fft.resize(5);

    // Output signal:
    my_fft[3] = my_fft[0]->Copy();
    *my_fft[3] -= *my_fft[2];

    // Crosstalk signal:
    my_fft[4] = my_fft[1]->Copy();
    *my_fft[4] -= *my_fft[2];

    DoMeasureWithReference(my_fft, p, data.rate());
 } else {
    DoMeasureWithoutReference(my_fft, p, data.rate());
 }
}

void MeasureAmplifier::DoMeasureWithReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 switch (getGeneratorType()) {
    case GENERATOR::GENERATOR_WHITE_NOISE:
        spectrum[0]->Average(*fft[3]);
        spectrum[1]->Average(*fft[4]);
    break;

    case GENERATOR::GENERATOR_SWEEP:
        CalculateSweepWithReference(fft, p, sampling_rate);
    break;

    default:
        // We cannot get here
    break;
 }
}

void MeasureAmplifier::DoMeasureWithoutReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 switch (getGeneratorType()) {
    case GENERATOR::GENERATOR_WHITE_NOISE:
        spectrum[0]->Average(*fft[0]);
        spectrum[1]->Average(*fft[1]);
    break;

    case GENERATOR::GENERATOR_SWEEP:
        CalculateSweepWithoutReference(fft, p, sampling_rate);
    break;

    default:
        // We cannot get here
    break;
 }
}

static float f(unsigned int pos, unsigned int size, unsigned int rate)
{
 return (float)pos * (float)(rate/2) / (float)size;
}

/*! This static function is used for sweep measurenemt. Because the sample length is given for FFT, and the speed of the generator is
 *  constant (logarithmic), we must collect all the power generated duruing the current sample. The bandwidth can be calculated from
 *  the sweep speed and the sample length.
 *  \param  sampling_rate   The input sampling rate [samples/second]
 *  \param  fft_size        Number of samples in the FFT result (half size of the FFT, 65536 here)
 *  \param  position        The center position (sample index) in the FFT result
 *  \retval bandwidth       The number of samples in the half bandwidth. The corresponding power can be calculating by adding power from
 *                          position-bandwidth to position+bandwidth */
// static
unsigned int MeasureAmplifier::calculateBandwidth(unsigned int sampling_rate, unsigned int fft_size, unsigned int position, float speed)
{
 SYS_DEBUG_STATIC(DM_MEASURE_AMP);

 float sample_time = 2.0f * (float)fft_size / (float)sampling_rate;
 float change = sample_time * speed;

 unsigned int result = (unsigned int)((float)sampling_rate / 20000.0f + (float)position * change * 0.5f + 0.5f);

 SYS_DEBUG(DL_INFO3, "Bandwidth " << result << " (from " << f(position-result, fft_size, sampling_rate) << " to " << f(position+result, fft_size, sampling_rate) << ") for " << f(position, fft_size, sampling_rate) << "Hz at " << position << " (time=" << sample_time << ", change=" << change << ")");

 return result;
}

void MeasureAmplifier::CalculateSweepWithReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 if (p.displayed_size < 0) {
    p.displayed_size = 1;   // Just insert a fake element in the x[] array to prevent its automatic fill
    p.x[0] = 0.0f;          // Note that the index 0 is never displayed
 }

 float inPower = 0.0f;
 float dBinPowerRel = 0.0f;
 float dBcrossPowerRel = 0.0f;

 static constexpr float sweep_speed = 0.065;    // Octave/second

 while(true) {
    static constexpr float limit = 1.0f;
    float exPower = 0.0f;

    if (freq_now == 0U) {
        // Try to find a peak:
        unsigned int position = (float)fft[0]->getPeakPosition(0);
        SYS_DEBUG(DL_INFO3, "initial peak at " << position << ": " << f(position, p.fft_size, sampling_rate) << "Hz");
        unsigned int width2 = calculateBandwidth(sampling_rate, p.fft_size, position, sweep_speed);
        if (width2 > position) {
            width2 = position;
        } else if (position+width2 > p.fft_size) {
            width2 = p.fft_size-position;
        }
        unsigned int start = position - width2;
        unsigned int end = position + width2;
        unsigned int peak_position = p.fft_size;
        if (end >= p.fft_size) {
            end = p.fft_size-1U;
        }

        inPower = fft[0]->getPowerDensityIn(start, end, &peak_position);
        exPower = fft[0]->getPowerDensityEx(start, end);

        if (inPower < exPower / limit) {
            // This is not a peak
            SYS_DEBUG(DL_INFO3, sampling_rate << " - no peak (" << log10f(inPower)*10.0f << "/" << log10f(exPower)*10.0f << ") at " << (float)peak_position * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz (" << peak_position << ", " << freq_now << "): bandwidth=" << (float)(width2*2U)  * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz");
            return;
        }

        SYS_DEBUG(DL_INFO3, sampling_rate << " - new peak (" << log10f(inPower)*10.0f << "/" << log10f(exPower)*10.0f << ") at " << (float)peak_position * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz (" << peak_position << ", " << freq_now << "): bandwidth=" << (float)(width2*2U)  * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz");

        freq_now = peak_position;
        dBinPowerRel = (*fft[3])[0][peak_position];
        dBcrossPowerRel = (*fft[4])[0][peak_position];

        break;

    } else {
        unsigned int width2 = calculateBandwidth(sampling_rate, p.fft_size, freq_now, sweep_speed);
        if (width2 > freq_now) {
            width2 = freq_now;
        } else if (freq_now+width2 > p.fft_size) {
            width2 = p.fft_size-freq_now;
        }
        unsigned int start = freq_now - width2;
        unsigned int end = freq_now + width2;
        unsigned int peak_position = p.fft_size;
        if (end >= p.fft_size) {
            end = p.fft_size-1U;
        }

        inPower = fft[0]->getPowerDensityIn(start, end, &peak_position);
        exPower = fft[0]->getPowerDensityEx(start, end);

        if (inPower > exPower / limit) {
            // This is a real peak
            SYS_DEBUG(DL_INFO3, sampling_rate << " - peak (" << log10f(inPower)*10.0f << "/" << log10f(exPower)*10.0f << ") at " << (float)peak_position * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz (" << peak_position << ", " << freq_now << "): bandwidth=" << (float)(width2*2U)  * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz");
            freq_now = peak_position;
            dBinPowerRel = (*fft[3])[0][peak_position];
            dBcrossPowerRel = (*fft[4])[0][peak_position];
            break;
        }

        freq_now = 0U;  // Try to find another peak
    }
 };

 // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 float dBpower = log10f(inPower)*10.0f;

 float k2 = -500.0f;
 float k3 = -500.0f;

 {
    bool k2on = true;
    bool k3on = true;
    unsigned int kp = freq_now * 2U;    // 2nd harmonic
    if (k2on) {
        if (kp >= p.fft_size) {
            k2on = false;
        } else {
            try {
                unsigned int width2 = calculateBandwidth(sampling_rate, p.fft_size, kp, sweep_speed);
                if (width2 > kp) {
                    width2 = kp;
                }
                k2 = fft[0]->dBgetPowerDensityIn(kp - width2, kp + width2) - dBpower;
                kp = freq_now * 3U;     // 3rd harmonic
                if (k3on) {
                    if (kp >= p.fft_size) {
                        k3on = false;
                    } else {
                        try {
                            width2 = calculateBandwidth(sampling_rate, p.fft_size, kp, sweep_speed);
                            if (width2 > kp) {
                                width2 = kp;
                            }
                            k3 = fft[0]->dBgetPowerDensityIn(kp - width2, kp + width2) - dBpower;
                        } catch (...) {
                            k3on = false;
                        }
                    }
                }
            } catch (...) {
                k2on = false;
            }
        }
    }

    SYS_DEBUG(DL_INFO3, "           k2=" << k2 << "dB (" << exp10f(k2/20.0f)*100.0f << "%), k3=" << k3 << "dB (" << exp10f(k3/20.0f)*100.0f << "%)");
 }

 if (p.displayed_size >= (int)p.fft_size) {
    return; // Just for safety
 }

 p.x[p.displayed_size] = freq_now * (float)(sampling_rate/2U) / (float)p.fft_size;

 (*spectrum[0])[1][p.displayed_size] = (*fft[3])[1][freq_now];
 (*spectrum[0])[0][p.displayed_size] = dBinPowerRel;
 (*spectrum[1])[0][p.displayed_size] = dBcrossPowerRel - dBinPowerRel;
 (*spectrum[2])[0][p.displayed_size] = k2;
 (*spectrum[3])[0][p.displayed_size] = k3;

 ++p.displayed_size;

 if (k2 > -499.0) {
    k_size[0] = p.displayed_size;
 }
 if (k3 > -499.0) {
    k_size[1] = p.displayed_size;
 }
}

void MeasureAmplifier::CalculateSweepWithoutReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 if (p.displayed_size < 0) {
    p.displayed_size = 1;   // Just insert a fake element in the x[] array to prevent its automatic fill
    p.x[0] = 0.0f;          // Note that the index 0 is never displayed
 }

 float inPower = 0.0f;
 float crossPower = 0.0f;

 static constexpr float sweep_speed = 0.065;    // Octave/second

 while(true) {
    static constexpr float limit = 1.0f;
    float exPower = 0.0f;

    if (freq_now == 0U) {
        // Try to find a peak:
        unsigned int position = (float)fft[0]->getPeakPosition(0);
        SYS_DEBUG(DL_INFO3, "initial peak at " << position << ": " << f(position, p.fft_size, sampling_rate) << "Hz");
        unsigned int width2 = calculateBandwidth(sampling_rate, p.fft_size, position, sweep_speed);
        if (width2 > position) {
            width2 = position;
        } else if (position+width2 > p.fft_size) {
            width2 = p.fft_size-position;
        }
        unsigned int start = position - width2;
        unsigned int end = position + width2;
        unsigned int peak_position = p.fft_size;
        if (end >= p.fft_size) {
            end = p.fft_size-1U;
        }

        inPower = fft[0]->getPowerDensityIn(start, end, &peak_position);
        exPower = fft[0]->getPowerDensityEx(start, end);

        if (inPower < exPower / limit) {
            // This is not a peak
            SYS_DEBUG(DL_INFO3, sampling_rate << " - no peak (" << log10f(inPower)*10.0f << "/" << log10f(exPower)*10.0f << ") at " << (float)peak_position * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz (" << peak_position << ", " << freq_now << "): bandwidth=" << (float)(width2*2U)  * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz");
            return;
        }

        SYS_DEBUG(DL_INFO3, sampling_rate << " - new peak (" << log10f(inPower)*10.0f << "/" << log10f(exPower)*10.0f << ") at " << (float)peak_position * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz (" << peak_position << ", " << freq_now << "): bandwidth=" << (float)(width2*2U)  * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz");

        freq_now = peak_position;
        crossPower = fft[1]->getPowerDensityIn(start, end);

        break;

    } else {
        unsigned int width2 = calculateBandwidth(sampling_rate, p.fft_size, freq_now, sweep_speed);
        if (width2 > freq_now) {
            width2 = freq_now;
        } else if (freq_now+width2 > p.fft_size) {
            width2 = p.fft_size-freq_now;
        }
        unsigned int start = freq_now - width2;
        unsigned int end = freq_now + width2;
        unsigned int peak_position = p.fft_size;
        if (end >= p.fft_size) {
            end = p.fft_size-1U;
        }

        inPower = fft[0]->getPowerDensityIn(start, end, &peak_position);
        exPower = fft[0]->getPowerDensityEx(start, end);

        if (inPower > exPower / limit) {
            // This is a real peak
            SYS_DEBUG(DL_INFO3, sampling_rate << " - peak (" << log10f(inPower)*10.0f << "/" << log10f(exPower)*10.0f << ") at " << (float)peak_position * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz (" << peak_position << ", " << freq_now << "): bandwidth=" << (float)(width2*2U)  * (float)(sampling_rate/2U) / (float)p.fft_size << "Hz");
            freq_now = peak_position;
            crossPower = fft[1]->getPowerDensityIn(start, end);
            break;
        }

        freq_now = 0U;  // Try to find another peak
    }
 };

 // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 float dBpower = log10f(inPower)*10.0f;

 float k2 = -500.0f;
 float k3 = -500.0f;

 {
    bool k2on = true;
    bool k3on = true;
    unsigned int kp = freq_now * 2U;    // 2nd harmonic
    if (k2on) {
        if (kp >= p.fft_size) {
            k2on = false;
        } else {
            try {
                unsigned int width2 = calculateBandwidth(sampling_rate, p.fft_size, kp, sweep_speed);
                if (width2 > kp) {
                    width2 = kp;
                }
                k2 = fft[0]->dBgetPowerDensityIn(kp - width2, kp + width2) - dBpower;
                kp = freq_now * 3U;     // 3rd harmonic
                if (k3on) {
                    if (kp >= p.fft_size) {
                        k3on = false;
                    } else {
                        try {
                            width2 = calculateBandwidth(sampling_rate, p.fft_size, kp, sweep_speed);
                            if (width2 > kp) {
                                width2 = kp;
                            }
                            k3 = fft[0]->dBgetPowerDensityIn(kp - width2, kp + width2) - dBpower;
                        } catch (...) {
                            k3on = false;
                        }
                    }
                }
            } catch (...) {
                k2on = false;
            }
        }
    }

    SYS_DEBUG(DL_INFO3, "           k2=" << k2 << "dB (" << exp10f(k2/20.0f)*100.0f << "%), k3=" << k3 << "dB (" << exp10f(k3/20.0f)*100.0f << "%)");
 }

 if (p.displayed_size >= (int)p.fft_size) {
    return; // Just for safety
 }

 p.x[p.displayed_size] = freq_now * (float)(sampling_rate/2U) / (float)p.fft_size;
 (*spectrum[0])[1][p.displayed_size] = 0.0f;  // No angle calculation is possible here
 (*spectrum[0])[0][p.displayed_size] = dBpower;
 (*spectrum[1])[0][p.displayed_size] = log10f(crossPower)*10.0f - dBpower;
 (*spectrum[2])[0][p.displayed_size] = k2;
 (*spectrum[3])[0][p.displayed_size] = k3;

 ++p.displayed_size;

 if (k2 > -499.0) {
    k_size[0] = p.displayed_size;
 }
 if (k3 > -499.0) {
    k_size[1] = p.displayed_size;
 }
}

void MeasureAmplifier::MeasurementResult(PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 updateFilter(p.fft_size, sampling_rate);

 if (getFFTMode()) {
    for (unsigned int i = 0; i < p.fft_size; ++i) {
        p.y[0][i] = (*spectrum[0])[0][i] + (*filter)[i];
        p.y[1][i] = (*spectrum[1])[0][i];
        p.y[2][i] = (*spectrum[0])[1][i] * (180.0f / (float)M_PI);
        p.y[3][i] = 0.0f;
        p.y[4][i] = 0.0f;
    }
    return;
 }

 switch (getGeneratorType()) {
    case GENERATOR::GENERATOR_WHITE_NOISE:
    {
        const float * db[] = {
            (*spectrum[0])[0],  // Output Signal
            (*spectrum[1])[0],  // Crosstalk Signal
            (*spectrum[0])[1]   // Output Phase
        };

        ResultNoise(db, p, sampling_rate);
    }
    break;

    case GENERATOR::GENERATOR_SWEEP:
    {
        const float * db[] = {
            (*spectrum[0])[0],  // Output Signal
            (*spectrum[1])[0],  // Crosstalk Signal
            (*spectrum[0])[1],  // Output Phase
            (*spectrum[2])[0],  // k2
            (*spectrum[3])[0]   // k3
        };

        ResultSweep(db, p, sampling_rate);
    }
    break;

    default:
        // Just to make gcc happy
    break;
 }
}

void MeasureAmplifier::ResultNoise(const float * const * db, PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 unsigned int samples = p.fft_size;

 float r1[samples]; // 1/4 MiB size on stack
 float s1[samples]; // 1/4 MiB size on stack
 for (unsigned int i = 0; i < samples; ++i) {
    r1[i] = db[0][i] + (*filter)[i];
    s1[i] = db[1][i];
 }

 float * r2 = r1;
 float * s2 = s1;

 if (smooth_factor) {
    r2 = (float*)alloca(sizeof(float[samples]));    // 1/4 MiB size on stack
    s2 = (float*)alloca(sizeof(float[samples]));    // 1/4 MiB size on stack
    switch (smooth_factor) {
        case (int)FILTER::SMOOTH_OCTAVE_PER_30:
            Smoothing(r2, r1, samples, sampling_rate, 1.0f/30.0f);
            Smoothing(s2, s1, samples, sampling_rate, 1.0f/30.0f);
        break;

        case (int)FILTER::SMOOTH_OCTAVE_PER_10:
            Smoothing(r2, r1, samples, sampling_rate, 1.0f/10.0f);
            Smoothing(s2, s1, samples, sampling_rate, 1.0f/10.0f);
        break;

        case (int)FILTER::SMOOTH_OCTAVE_PER_3:
            Smoothing(r2, r1, samples, sampling_rate, 1.0f/3.0f);
            Smoothing(s2, s1, samples, sampling_rate, 1.0f/3.0f);
        break;

        default:
            Smoothing(r2, r1, samples, smooth_factor);
            Smoothing(s2, s1, samples, smooth_factor);
        break;
    }
 }

 switch (alignment) {
    case FILTER::ALIGNMENT_1K:
    {
        unsigned int pos_1k = (1e3 * samples) / (sampling_rate/2);
        peak = r2[pos_1k];
    }
    break;

    case FILTER::ALIGNMENT_PEAK:
        peak = -1e9;
        for (unsigned int i = 0; i < samples; ++i) {
            if (r2[i] > peak) {
                peak = r2[i];
            }
        }
    break;

    case FILTER::ALIGNMENT_NONE:    // No break
    default:                        // just for safety
        peak = 0.0f;
    break;
 }

 for (unsigned int i = 0; i < samples; ++i) {
    p.y[0][i] = r2[i] - peak;
    p.y[1][i] = s2[i] - r2[i];
    p.y[2][i] = db[2][i] * (180.0f / (float)M_PI);
 }
}

void MeasureAmplifier::ResultSweep(const float * const * db, PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_AMP);

 unsigned int samples = p.fft_size;

 p.y[3].resize(k_size[0]);
 p.y[4].resize(k_size[1]);

 float r1[samples]; // 1/4 MiB size on stack
 float s1[samples]; // 1/4 MiB size on stack
 for (unsigned int i = 0; i < samples; ++i) {
    r1[i] = db[0][i] + (*filter)[i];
    s1[i] = db[1][i];
    p.y[2][i] = db[2][i] * (180.0f / (float)M_PI);
    if (i < k_size[0]) {
        p.y[3][i] = db[3][i];
    }
    if (i < k_size[1]) {
        p.y[4][i] = db[4][i];
    }
 }

 float * r2 = r1;
 float * s2 = s1;

 do {
    if (smooth_factor) {
        if (p.x.empty()) {
            break;
        }
        r2 = (float*)alloca(sizeof(float[p.x.size()]));
        s2 = (float*)alloca(sizeof(float[p.x.size()]));
        switch (smooth_factor) {
            case (int)FILTER::SMOOTH_OCTAVE_PER_30:
                Smoothing(r2, r1, p.x, 1.0f/30.0f);
                Smoothing(s2, s1, p.x, 1.0f/30.0f);
            break;

            case (int)FILTER::SMOOTH_OCTAVE_PER_10:
                Smoothing(r2, r1, p.x, 1.0f/10.0f);
                Smoothing(s2, s1, p.x, 1.0f/10.0f);
            break;

            case (int)FILTER::SMOOTH_OCTAVE_PER_3:
                Smoothing(r2, r1, p.x, 1.0f/3.0f);
                Smoothing(s2, s1, p.x, 1.0f/3.0f);
            break;

            default:
                r2 = r1;    // Note: This shall not be selected by GUI.
                s2 = s1;    //       It is left here for safety only.
            break;
        }
    }

 } while(false);

 switch (alignment) {
    case FILTER::ALIGNMENT_1K:
        if (p.x.empty()) {
            peak = 0.0f;
        } else {
            float difi = fabsf(p.x[0] - 1e3);
            peak = r2[0];
            for (unsigned int i = 1; i < p.x.size(); ++i) {
                float d = fabsf(p.x[i] - 1e3);
                if (d < difi) {
                    difi = d;
                    peak = r2[i];
                }
            }
        }
    break;

    case FILTER::ALIGNMENT_PEAK:
        peak = -1e9;
        for (unsigned int i = 1; i < p.x.size(); ++i) {
            if (r2[i] > peak) {
                peak = r2[i];
            }
        }
    break;

    case FILTER::ALIGNMENT_NONE:    // No break
    default:                        // just for safety
        peak = 0.0f;
    break;
 }

 for (unsigned int i = 0; i < samples; ++i) {
    p.y[1][i] = s2[i];
    p.y[0][i] = r2[i] - peak;
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
