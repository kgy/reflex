/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_MEASURES_MEASURE_AMPLIFIER_H_INCLUDED__
#define __SRC_MEASUREMENT_MEASURES_MEASURE_AMPLIFIER_H_INCLUDED__

#include <measures/params.h>
#include <measures/analyze-base.h>

#include <Debug/Debug.h>

SYS_DECLARE_MODULE(DM_MEASURE_AMP);

namespace PLOT
{
    class MeasureAmplifier: public AnalyzeBase
    {
     public:
        MeasureAmplifier(void);
        virtual ~MeasureAmplifier();

     protected:
        static Params plot_params;

        float peak;

        unsigned int freq_now;

        unsigned int k_size[2];

     private:
        SYS_DEFINE_CLASS_NAME("PLOT::MeasureAmplifier");

        virtual const Params & getPlotParameters(void) const override
        {
            return plot_params;
        }

        virtual void doStart(bool is_calibration) override;
        virtual void DoMeasure(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[]) override;
        virtual void MeasurementResult(PLOT::PlotData & p, unsigned int sampling_rate) override;
        virtual void SelectType(MeasurementTypes type) override;

        void DoMeasureWithReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate);
        void DoMeasureWithoutReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate);
        void CalculateSweepWithReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate);
        void CalculateSweepWithoutReference(const FFT::logSpectrumArray & fft, PLOT::PlotData & p, unsigned int sampling_rate);
        void ResultNoise(const float * const * db, PLOT::PlotData & p, unsigned int sampling_rate);
        void ResultSweep(const float * const * db, PLOT::PlotData & p, unsigned int sampling_rate);
        void allocateSpectrumArray(unsigned int size, unsigned int fft_size);

        /// Calculates the peak measurement bandwidth
        static unsigned int calculateBandwidth(unsigned int sampling_rate, unsigned int fft_size, unsigned int position, float speed);

    }; // class PLOT::MeasureAmplifier

} // namespace PLOT

#endif /* __SRC_MEASUREMENT_MEASURES_MEASURE_AMPLIFIER_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
