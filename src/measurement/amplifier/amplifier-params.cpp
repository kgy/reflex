/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measure-amplifier.h"

char _load_amplifier_measurement;

static PLOT::MeasureAmplifier _measure_instance;

PLOT::Params PLOT::MeasureAmplifier::plot_params = {
    type: MEASURE_WINDOW_AMPLIFIER,
    label: "frequency",
    logarithmic: true,
    legend: true,
    left: 5.0,
    right: 1e5,

    channels: {
        SOUND_IN_AMPLIFIER_OUT,
        SOUND_IN_CROSSTALK,
        SOUND_IN_FEEDBACK,
    },

    plots: 5,

    y: {
        {
            visible: true,
            subgrid: true,
            label: "signal strength",
            unit: PLOT::Unit::UNIT_DECIBEL,
            min: -84.0,
            max: 24.0
        }, {
            visible: true,
            subgrid: false,
            label: "phase",
            unit: PLOT::Unit::UNIT_DEGREE,
            min: -180.0,
            max: 180.0
        }
    },

    plot: {
        {
            name: "CH1",
            colour: {   0,  60,   0, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_DECIBEL,
        }, {
            name: "CH2 (crosstalk)",
            colour: {   0, 150,   0, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_DECIBEL,
        }, {
            name: "Phase",
            colour: { 150,   0, 150, 128 },
            yAxis: 1,
            unit: PLOT::Unit::UNIT_DEGREE,
        }, {
            name: "k2",
            colour: { 150,   0,   0, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_PERCENT,
        }, {
            name: "k3",
            colour: { 150,  90,   0, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_PERCENT,
        }
    }
};

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
