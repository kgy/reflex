/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measure-speaker.h"

char _load_speaker_measurement;

static PLOT::MeasureSpeaker _measure_instance;

PLOT::Params PLOT::MeasureSpeaker::plot_params = {
    type: MEASURE_WINDOW_SPEAKER,
    label: "frequency",
    logarithmic: true,
    legend: true,
    left: 5.0,
    right: 1e5,

    channels: {
        SOUND_IN_MICROPHONE,
        SOUND_IN_SPEAKER_VOLTAGE,
        SOUND_IN_SPEAKER_CURRENT,
    },

    plots: 5,

    y: {
        {
            visible: true,
            subgrid: true,
            label: "signal strength",
            unit: PLOT::Unit::UNIT_DECIBEL,
            min: -84.0,
            max: 24.0
        }, {
            visible: true,
            subgrid: false,
            label: "phase",
            unit: PLOT::Unit::UNIT_DEGREE,
            min: -180.0,
            max: 180.0
        }
    },

    plot: {
        {
            name: "Microphone",
            colour: { 150,   0,   0, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_DECIBEL
        }, {
            name: "Voltage",
            colour: {   0, 150,   0, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_DECIBEL
        }, {
            name: "Current",
            colour: {   0,   0, 150, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_DECIBEL
        }, {
            name: "Impedance",
            colour: {   0,   0, 150, 128 },
            yAxis: 0,
            unit: PLOT::Unit::UNIT_OHM
        }, {
            name: "Phase",
            colour: { 150,   0, 150, 128 },
            yAxis: 1,
            unit: PLOT::Unit::UNIT_DEGREE
        }
    }
};

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
