/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_MEASURES_MEASURE_SPEAKER_H_INCLUDED__
#define __SRC_MEASUREMENT_MEASURES_MEASURE_SPEAKER_H_INCLUDED__

#include <measures/params.h>
#include <measures/analyze-base.h>

#include <Debug/Debug.h>

SYS_DECLARE_MODULE(DM_MEASURE_SPK);

namespace PLOT
{
    class MeasureSpeaker: public AnalyzeBase
    {
     public:
        MeasureSpeaker(void);
        virtual ~MeasureSpeaker();

        /// The calibration can be set using this function
        /*! The reference resistor (in ohms) must be set externally. */
        double & calibration(void)
        {
            return calibration_value;
        }

     protected:
        static Params plot_params;

        double peak;

        double calibration_value;

        FFT::logSpectrumPtr calibration_info;

     private:
        SYS_DEFINE_CLASS_NAME("PLOT::MeasureSpeaker");

        virtual const Params & getPlotParameters(void) const override
        {
            return plot_params;
        }

        virtual void DoMeasure(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[]) override;
        virtual void MeasurementResult(PLOT::PlotData & p, unsigned int sampling_rate) override;
        virtual void SelectType(MeasurementTypes type) override;
        virtual void CalibrationResult(PLOT::PlotData & p, unsigned int sampling_rate) override;
        virtual void doStart(bool is_calibration) override;
        virtual void setReferenceResistor(float resistance) override;

    }; // class PLOT::MeasureSpeaker

} // namespace PLOT

#endif /* __SRC_MEASUREMENT_MEASURES_MEASURE_SPEAKER_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
