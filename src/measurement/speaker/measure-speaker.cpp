/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measure-speaker.h"

#include <math.h>
#include <sstream>

SYS_DEFINE_MODULE(DM_MEASURE_SPK);

using PLOT::MeasureSpeaker;

MeasureSpeaker::MeasureSpeaker(void):
    peak(0.0),
    calibration_value(0.0)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);
}

MeasureSpeaker::~MeasureSpeaker(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);
}

void MeasureSpeaker::SelectType(MeasurementTypes type)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);

 Selected(type == MEASUREMENT_SPEAKER_1 || type == MEASUREMENT_SPEAKER_2);
}

void MeasureSpeaker::doStart(bool is_calibration)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);

 if (is_calibration) {
    calibration_info.reset();
 }
}

void MeasureSpeaker::setReferenceResistor(float resistance)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);

 calibration_value = log10f(resistance) * 20.0f;
}

static constexpr float average_limit = 3.0f;

void MeasureSpeaker::DoMeasure(const FFT::FFTBuffer & data, PLOT::PlotData & p, const int channel_map[])
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);

 static constexpr unsigned int input_channels = 3;          // Channels from the input stream
 static constexpr unsigned int channels = input_channels+1; // The impedance response is stored too

 FFT::logSpectrumArray my_fft;
 my_fft.resize(input_channels);

 const SoundIoConnection * my_channels = plot_params.channels;

 for (unsigned int ch = 0; ch < input_channels; ++ch) {
    int channel_source = channel_map[my_channels[ch]];
    if (channel_source < 0) {
        SYS_DEBUG(DL_INFO2, "No input connection for channel " << channel_map[ch] << ", clearing data...");
        my_fft[ch] = FFT::logSpectrum::Create(p.fft_size);
        my_fft[ch]->Clear();
    } else {
        SYS_DEBUG(DL_INFO2, "FFT on channel " << channel_map[ch] << "...");
        my_fft[ch] = FFT::logSpectrum::Create(*data.complexFFT((unsigned int)channel_source));
    }
 }

 if (spectrum.empty()) {
    spectrum.resize(channels);
    for (unsigned int i = 0; i < channels; ++i) {
        spectrum[i] = FFT::logSpectrum::Create(p.fft_size);
        spectrum[i]->Clear();
    }
 }

 for (unsigned int i = 0; i < input_channels; ++i) {
    spectrum[i]->AverageLog(*my_fft[i]);
 }

 FFT::logSpectrumPtr impedance = my_fft[1]->Copy();
 *impedance -= *my_fft[2];
 if (calibration_info) {
    *impedance -= *calibration_info;
 }
 spectrum[3]->AverageLog(*impedance, average_limit);
}

void MeasureSpeaker::MeasurementResult(PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);

 const float * db[] = {     //   See PLOT::Params::channels:
    (*spectrum[0])[0], // - Microphone Signal
    (*spectrum[1])[0], // - Speaker Voltage Level
    (*spectrum[2])[0], // - Speaker Current Level
    (*spectrum[3])[0], // - Speaker Impedance Value
    (*spectrum[3])[1]  // - Speaker Impedance Phase
 };

 double max = -1e9;
 for (unsigned int i = 0; i < p.y[0].size(); ++i) {
    p.y[0][i] = db[0][i];
    p.y[1][i] = db[1][i];
    p.y[2][i] = db[2][i];
    p.y[3][i] = db[3][i] + calibration_value;
    p.y[4][i] = db[4][i] * (180.0f/(float)M_PI);

    if (p.y[0][i] > max) {
        max = p.y[0][i];
    }
 }

 peak = max;

 for (unsigned int i = 0; i < p.y[0].size(); ++i) {
    p.y[0][i] -= max;
 }
}

void MeasureSpeaker::CalibrationResult(PLOT::PlotData & p, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);

 const float * db[] = {
    (*spectrum[0])[0], // - Microphone Signal
    (*spectrum[1])[0], // - Speaker Voltage
    (*spectrum[2])[0], // - Speaker Current
 };

 if (!calibration_info) {
    calibration_info = FFT::logSpectrum::Create(p.fft_size);    // Value and Phase
    calibration_info->Clear();
 }

 FFT::logSpectrumPtr impedance = spectrum[1]->Copy();
 *impedance -= *spectrum[2];  // Speaker Impedance (Voltage/Current)
 calibration_info->AverageLog(*impedance, average_limit);

 for (unsigned int i = 0; i < p.y[0].size(); ++i) {
    // Fill the plot values:
    p.y[0][i] = db[0][i];
    p.y[1][i] = db[1][i];
    p.y[2][i] = db[2][i];
    p.y[3][i] = (*calibration_info)[0][i];
    p.y[4][i] = (*calibration_info)[1][i] * (180.0f/(float)M_PI);
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
