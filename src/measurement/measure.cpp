/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <measures/params.h>
#include <sound-io-connection.h>

#include "measure.h"

SYS_DEFINE_MODULE(DM_MEASURE);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class Measure:                                                                *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

Measure::~Measure()
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 {
    RPtr r = recorder;
    if (r) {
        r->Stop(false);
    }
 }

 {
    GPtr g = generator;
    if (g) {
        g->Stop(true);
    }
 }

 {
    RPtr r = recorder;
    if (r) {
        r->Join();
    }
 }
}

void Measure::Start(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 generator.reset();
 recorder.reset();

 SND::DeviceInfo & params = caller.getDeviceInfo();

 std::string load_file = caller.getOutputLoadFileName();
 if (!load_file.empty()) {
    recorder.reset(new RecorderFromFile(*this, params.recorder, load_file));
 } else {
    recorder.reset(new RecorderFromDevice(*this, params.recorder, caller.getOutputSaveFileName()));
    switch (caller.getGeneratorType()) {
        case GENERATOR::GENERATOR_WHITE_NOISE:
            generator.reset(new NoiseGenerator(*this, params.generator));
        break;

        case GENERATOR::GENERATOR_SWEEP:
            generator.reset(new SweepGenerator(*this, params.generator));
        break;

        case GENERATOR::GENERATOR_EXTERNAL:
            // Continue without generator created
        break;

        default:    // Just for safety
            recorder.reset();
            return;
        break;
    }
 }

 if (generator) {
    setOutputLevel(caller.getGeneratorLevel());
    generator->Start();
 }

 recorder->Start();

 MeasureStarted();
}

void Measure::Stop(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 StopGenerator(true);
 StopRecorder();
}

void Measure::GeneratorStopped(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

void Measure::PlayerStopped(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 StopRecorder();
}

void Measure::AnalyzerStopped(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 StopGenerator(false);
}

void Measure::RecorderStopped(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 MeasureStopped();
}

void Measure::StopRecorder(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 RPtr p;
 p.swap(recorder);

 if (p) {
    p->Stop(true);
 }
}

void Measure::StopGenerator(bool immediate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 GPtr p;
 p.swap(generator);

 if (p) {
    if (immediate) {
        p->StopImmediately(true);
    } else {
        p->Stop(true);
    }
 }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class MeasureBase::GeneratorBase:                                             *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MeasureBase::GeneratorBase::GeneratorBase(MeasureBase & parent, SND::DeviceInfo::Parameters & dev):
    SND::Player(dev),
    parent(parent),
    dev(dev),
    amplitude(0.0f)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

MeasureBase::GeneratorBase::~GeneratorBase()
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

void MeasureBase::GeneratorBase::processorStopped(const std::string & name)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.GeneratorStopped();
}

void MeasureBase::GeneratorBase::deviceStopped(const std::string & name)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.PlayerStopped();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class MeasureBase::FrameGeneratorBase:                                        *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

bool MeasureBase::FrameGeneratorBase::fillFrame(SND::SoundFrameBufferPtr & buf)
{
 int out_channel = dev.channels[SOUND_OUT_SIGNAL];
 if (out_channel < 0) {
    return false;
 }

 bool to_be_continued = fillFrame((*buf)[out_channel], buf->samples());

 for (unsigned int i = 0; i < buf->channels(); ++i) {
    if ((int)i != out_channel) {
        buf->Clear(i);
    }
 }

 return to_be_continued;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class MeasureBase::RecorderFromDevice:                                        *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MeasureBase::RecorderFromDevice::RecorderFromDevice(MeasureBase & parent, SND::DeviceInfo::Parameters & dev, const std::string & save_filename):
    SND::Recorder(dev),
    MeasureBase::RecorderBase(parent, dev),
    frame_counter(0U),
    saver(new FileSaver(*this, save_filename, parent.getPlotParameters(), dev.sampling_rate))
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 FileSaver::Start(saver);
}

MeasureBase::RecorderFromDevice::~RecorderFromDevice()
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

bool MeasureBase::RecorderFromDevice::frameCaptured(SND::SoundFrameBufferPtr & buf)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (++frame_counter < 5) {
    // Just skip the first some frames:
    return true;
 }

 saver->SaveFrame(buf);

 return parent.frameCaptured(buf);
}

void MeasureBase::RecorderFromDevice::error(std::exception * ex)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.except("file_saver_thread", ex ? ex->what() : "unknown exception occured");
}

void MeasureBase::RecorderFromDevice::except(const char * module, const char * reason)
{
 parent.except(module, reason);
}

void MeasureBase::RecorderFromDevice::Stop(bool is_join)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 saver->Stop(is_join);

 SND::Recorder::Stop(is_join);
}

void MeasureBase::RecorderFromDevice::Start(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 SND::Recorder::Start();
}

void MeasureBase::RecorderFromDevice::Join(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 saver->Join();

 SND::Recorder::Join();
}

void MeasureBase::RecorderFromDevice::processorStopped(const std::string & name)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.AnalyzerStopped();
}

void MeasureBase::RecorderFromDevice::deviceStopped(const std::string & name)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.RecorderStopped();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class MeasureBase::RecorderFromDevice::FileSaver:                             *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MeasureBase::RecorderFromDevice::FileSaver::FileSaver(RecorderFromDevice & parent, const std::string & filename, const PLOT::Params * params, unsigned int sampling_rate):
    Threads::Thread("flac_saver"),
    parent(parent),
    filename(filename),
    params(params),
    sampling_rate(sampling_rate),
    channels(0U)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (params) {
    for (unsigned int i = 0; params->channels[i] != SOUND_CONNECTION_UNUSED; ++i) {
        ++channels;
    }
 }
}

MeasureBase::RecorderFromDevice::FileSaver::~FileSaver()
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

// static
void MeasureBase::RecorderFromDevice::FileSaver::Start(FileSaverPtr & s)
{
 SYS_DEBUG_STATIC(DM_MEASURE);

 if (s->filename.empty() || !s->params || s->channels == 0) {
    // Do not start it if not enough information available:
    return;
 }

 Threads::Thread::Start<FileSaver>(s, 10*1024*1024);
}

void MeasureBase::RecorderFromDevice::FileSaver::SaveFrame(SND::SoundFrameBufferPtr & buf)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 data.push_drop(buf);
}

int MeasureBase::RecorderFromDevice::FileSaver::main(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 open();

 while (!IsFinished()) {
    auto p = data.pop();
    if (!p) {
        break;
    }

    save(p);
 }

 close();

 return 0;
}

void MeasureBase::RecorderFromDevice::FileSaver::open(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 encoder.set_verify(false);
 encoder.set_compression_level(8);
 encoder.set_channels(channels);
 encoder.set_bits_per_sample(24);
 encoder.set_sample_rate(sampling_rate);
 encoder.set_total_samples_estimate(0);

 ASSERT_FLAC_ENCODER_INIT(encoder, init(filename), "FLAC::init(\"" << filename << "\") failed: ");
}

void MeasureBase::RecorderFromDevice::FileSaver::save(SND::SoundFrameBufferPtr & buf)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (channels == 0) {
    // Sanity check:
    return;
 }

 FLAC__int32 buffer[channels][buf->size()];

 for (unsigned int i = 0; i < channels; ++i) {
    for (unsigned int j = 0; j < buf->size(); ++j) {
        buffer[i][j] = (FLAC__int32)((*buf)[i][j] * 8388607.0f);
    }
 }

 FLAC__int32 * data[channels];
 for (unsigned int i = 0; i < channels; ++i) {
    data[i] = buffer[i];
 }

 encoder.process(data, buf->size());
}

void MeasureBase::RecorderFromDevice::FileSaver::close(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 ASSERT_DBG(encoder.finish(), "FLAC::finish() failed");
}

void MeasureBase::RecorderFromDevice::FileSaver::Stop(bool is_join)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 data.finish();

 Kill(is_join);
}

void MeasureBase::RecorderFromDevice::FileSaver::error(std::exception * ex)
{
 parent.error(ex);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class MeasureBase::RecorderFromFile:                                          *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MeasureBase::RecorderFromFile::RecorderFromFile(MeasureBase & parent, SND::DeviceInfo::Parameters & dev, const std::string & filename):
    MeasureBase::RecorderBase(parent, dev),
    loader(new FileLoader(*this, filename, parent.getPlotParameters(), dev.sampling_rate))
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 FileLoader::Start(loader);
}

MeasureBase::RecorderFromFile::~RecorderFromFile()
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

void MeasureBase::RecorderFromFile::Stop(bool is_join)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 loader->Kill(is_join);
}

void MeasureBase::RecorderFromFile::Start(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);
}

void MeasureBase::RecorderFromFile::Join(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 loader->Join();
}

void MeasureBase::RecorderFromFile::error(std::exception * ex)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.except("file_loader_thread", ex ? ex->what() : "unknown exception occured");
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class MeasureBase::RecorderFromFile::FileLoader:                              *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MeasureBase::RecorderFromFile::FileLoader::FileLoader(RecorderFromFile & parent, const std::string & filename, const PLOT::Params * params, unsigned int & sampling_rate):
    Threads::Thread("flac_reader"),
    parent(parent),
    filename(filename),
    params(params),
    sampling_rate(sampling_rate),
    channels(0U),
    actual_position(0U),
    decoder(*this)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (params) {
    for (unsigned int i = 0; params->channels[i] != SOUND_CONNECTION_UNUSED; ++i) {
        ++channels;
    }
 }
}

// static
void MeasureBase::RecorderFromFile::FileLoader::Start(FileLoaderPtr & s)
{
 SYS_DEBUG_STATIC(DM_MEASURE);

 if (s->filename.empty() || !s->params || s->channels == 0) {
    // Do not start it if no enough information available:
    return;
 }

 Threads::Thread::Start<FileLoader>(s, 10*1024*1024);
}

int MeasureBase::RecorderFromFile::FileLoader::main(void)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 ASSERT_FLAC_DECODER_INIT(decoder, init(filename), "FLAC::init(\"" << filename << "\") failed: ");

 bool status = decoder.process_until_end_of_stream();

 if (!status) {
    DEBUG_OUT("FLAC decoder finished with status " << decoder.get_state().resolved_as_cstring(decoder));
 }

 return 0;
}

bool MeasureBase::RecorderFromFile::FileLoader::got_new_frame(const FLAC__int32 * const * data, unsigned int channels, unsigned int samples, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 SND::SoundFrameBufferPtr p = actual_frame;

 if (!p) {
    p = SND::MemFrameBuffer::Create(2048U, channels, sampling_rate);
 }

 for (unsigned int i = 0; i < samples; ++i) {
    for (unsigned int j = 0; j < channels; ++j) {
        (*p)[j][actual_position] = (float)data[j][i] / 8388607.0f;
    }
    if (ToBeFinished()) {
        return false;
    }
    if (++actual_position >= 2048) {
        actual_position = 0;
        if (!parent.frameCaptured(p)) {
            return false;
        }
        if (!p) {
            p = SND::MemFrameBuffer::Create(2048, channels, sampling_rate);
        }
    }
 }

 actual_frame = p;

 return true;
}

void MeasureBase::RecorderFromFile::FileLoader::flac_error(const char * error)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 std::cout << "flac_error: " << error << std::endl;
}

void MeasureBase::RecorderFromFile::FileLoader::error(std::exception * ex)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.error(ex);
}

void MeasureBase::RecorderFromFile::FileLoader::exited(int /*status*/)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.eof();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                                       *
 *         Class MeasureBase::RecorderFromFile::FileLoader::FlacDecoder:                 *
 *                                                                                       *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

FLAC__StreamDecoderWriteStatus MeasureBase::RecorderFromFile::FileLoader::FlacDecoder::write_callback(const FLAC__Frame * frame, const FLAC__int32 * const * data)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 if (frame->header.channels < 1) {
    DEBUG_OUT("number of channels: " << frame->header.channels);
    return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
 }

 if (frame->header.bits_per_sample != 24) {
    DEBUG_OUT("the input has " << frame->header.bits_per_sample << " bits per sample, only 24 bit is supported");
    return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
 }

 parent.sampling_rate = frame->header.sample_rate;

 if (!parent.got_new_frame(data, frame->header.channels, frame->header.blocksize, frame->header.sample_rate)) {
    return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
 }

 return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

void MeasureBase::RecorderFromFile::FileLoader::FlacDecoder::error_callback(FLAC__StreamDecoderErrorStatus status)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 parent.flac_error(get_state().resolved_as_cstring(*this));
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
