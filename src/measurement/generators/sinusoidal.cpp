/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "sinusoidal.h"

#include <math.h>

SinusoidalGenerator::SinusoidalGenerator(unsigned int sampling_rate):
    position(0.0),
    step((2.0*M_PI)/(float)sampling_rate)
{
}

SinusoidalGenerator::~SinusoidalGenerator()
{
}

/// Produces sinusoidal result
/*! \param  freq    This is the frequency, in Hz.<br>
 *                  Note that it can be fixed or change sample by sample, so it can generate
 *                  fixed frequency, continuous sweep, or frequency modulated output. */
float SinusoidalGenerator::getSinusValue(float freq)
{
 position += freq * step;

 if (position > 2.0*M_PI) {
    position -= 2.0*M_PI;
 }

 return sin(position);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
