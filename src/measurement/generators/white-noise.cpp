/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "white-noise.h"

#include <ctime>

WhiteNoiseGenerator::WhiteNoiseGenerator(void):
    random_generator(std::time(nullptr))
{
}

WhiteNoiseGenerator::~WhiteNoiseGenerator()
{
}

/// Generates a random number between +/- 0.99999994 (practically +/- 1.0)
float WhiteNoiseGenerator::getRandomValue(void)
{
 return ((float)(random_generator() & 0xffffff) - 8388607.5f) / 8388608.0f;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
