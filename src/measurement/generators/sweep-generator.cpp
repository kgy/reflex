/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <measure.h>

MeasureBase::SweepGenerator::SweepGenerator(MeasureBase & parent, SND::DeviceInfo::Parameters & dev):
    super(parent, dev),
    SinusoidalGenerator(dev.sampling_rate),
    freq(16.0f),
    sweep(step/100.0f),
    limit(0.9f*(float)(dev.sampling_rate/2))
{
 SYS_DEBUG_MEMBER(DM_GENERATOR);
}

MeasureBase::SweepGenerator::~SweepGenerator()
{
 SYS_DEBUG_MEMBER(DM_GENERATOR);
}

inline bool MeasureBase::SweepGenerator::fillFrame(float * frame, unsigned int size)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 for (unsigned int i = size; i; --i, ++frame) {
    *frame = getSinusValue(freq) * amplitude;
    freq += freq * sweep;
 }

 return freq < limit;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
