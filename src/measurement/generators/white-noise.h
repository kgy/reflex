/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_GENERATORS_WHITE_NOISE_H_INCLUDED__
#define __SRC_MEASUREMENT_GENERATORS_WHITE_NOISE_H_INCLUDED__

#include "generator-type.h"

#include <Debug/Debug.h>

#include <random>

class WhiteNoiseGenerator
{
 public:
    WhiteNoiseGenerator(void);
    virtual ~WhiteNoiseGenerator();

    float getRandomValue(void);

 protected:
    std::mt19937 random_generator;

 private:
    SYS_DEFINE_CLASS_NAME("WhiteNoiseGenerator");

}; // class WhiteNoiseGenerator

#endif /* __SRC_MEASUREMENT_GENERATORS_WHITE_NOISE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
