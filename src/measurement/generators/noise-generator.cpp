/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <measure.h>

SYS_DEFINE_MODULE(DM_GENERATOR);

MeasureBase::NoiseGenerator::NoiseGenerator(MeasureBase & parent, SND::DeviceInfo::Parameters & dev):
    super(parent, dev)
{
 SYS_DEBUG_MEMBER(DM_GENERATOR);
}

MeasureBase::NoiseGenerator::~NoiseGenerator()
{
 SYS_DEBUG_MEMBER(DM_GENERATOR);
}

inline bool MeasureBase::NoiseGenerator::fillFrame(float * frame, unsigned int size)
{
 SYS_DEBUG_MEMBER(DM_MEASURE);

 for (unsigned int i = size; i; --i, ++frame) {
    *frame = getRandomValue() * amplitude;
 }

 return true;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
