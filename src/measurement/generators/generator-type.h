/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_GENERATORS_GENERATOR_TYPE_H_INCLUDED__
#define __SRC_MEASUREMENT_GENERATORS_GENERATOR_TYPE_H_INCLUDED__

namespace GENERATOR
{
    enum GeneratorType
    {
        GENERATOR_NONE,
        GENERATOR_WHITE_NOISE,
        GENERATOR_SWEEP,
        GENERATOR_EXTERNAL,
        _GENERATOR_SIZE

    }; // enum GENERATOR::GeneratorType

} // namespace GENERATOR

#endif /* __SRC_MEASUREMENT_GENERATORS_GENERATOR_TYPE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
