/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_GENERATORS_SINUSOIDAL_H_INCLUDED__
#define __SRC_MEASUREMENT_GENERATORS_SINUSOIDAL_H_INCLUDED__

#include "generator-type.h"

#include <Debug/Debug.h>

class SinusoidalGenerator
{
 public:
    SinusoidalGenerator(unsigned int sampling_rate);
    virtual ~SinusoidalGenerator();

    float getSinusValue(float freq);

 protected:
    float position;

    float step;

 private:
    SYS_DEFINE_CLASS_NAME("SinusoidalGenerator");

}; // class SinusoidalGenerator

#endif /* __SRC_MEASUREMENT_GENERATORS_SINUSOIDAL_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
