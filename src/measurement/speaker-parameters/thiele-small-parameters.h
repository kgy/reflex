/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_SPEAKER_PARAMETERS_THIELE_SMALL_PARAMETERS_H_INCLUDED__
#define __SRC_MEASUREMENT_SPEAKER_PARAMETERS_THIELE_SMALL_PARAMETERS_H_INCLUDED__

#include <Debug/Debug.h>
#include <Memory/Memory.h>

SYS_DECLARE_MODULE(DM_MEASURE_SPK);

class SpeakerParameters;
class ThieleSmall;

using ThieleSmallPtr = MEM::shared_ptr<ThieleSmall>;

class ThieleSmall
{
 protected:
    ThieleSmall(const SpeakerParameters & p1, const SpeakerParameters & p2, float grAddedMass);

 public:
    static inline ThieleSmallPtr Create(const SpeakerParameters & p1, const SpeakerParameters & p2, float grAddedMass)
    {
        return new ThieleSmall(p1, p2, grAddedMass);
    }

    virtual ~ThieleSmall();

 private:
    SYS_DEFINE_CLASS_NAME("ThieleSmall");

}; // class ThieleSmall

#endif /* __SRC_MEASUREMENT_SPEAKER_PARAMETERS_THIELE_SMALL_PARAMETERS_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
