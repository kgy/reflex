/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_SPEAKER_PARAMETERS_MEASURE_SPEAKER_PARAMETERS_H_INCLUDED__
#define __SRC_MEASUREMENT_SPEAKER_PARAMETERS_MEASURE_SPEAKER_PARAMETERS_H_INCLUDED__

#include <Debug/Debug.h>
#include <Memory/Memory.h>

SYS_DECLARE_MODULE(DM_MEASURE_SPK);

namespace FFT
{
    class logSpectrum;
}

class SpeakerParameters;

using SpeakerParamsPtr = MEM::shared_ptr<SpeakerParameters>;

/// Calculate basic Speaker Parameters from its impedance response
class SpeakerParameters
{
 protected:
    SpeakerParameters(const FFT::logSpectrum & spectrum, unsigned int sampling_freq);

    unsigned int sampling_freq;

    float resonance_freq;

    float resistance_at_DC;

    float resistance_at_resonance;

    float Q_at_resonance;

 public:
    static inline SpeakerParamsPtr Create(const FFT::logSpectrum & spectrum, unsigned int sampling_freq)
    {
        return new SpeakerParameters(spectrum, sampling_freq);
    }

    virtual ~SpeakerParameters();

 private:
    SYS_DEFINE_CLASS_NAME("SpeakerParameters");

}; // class SpeakerParameters

#endif /* __SRC_MEASUREMENT_SPEAKER_PARAMETERS_MEASURE_SPEAKER_PARAMETERS_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
