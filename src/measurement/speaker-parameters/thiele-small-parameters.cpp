/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "thiele-small-parameters.h"

ThieleSmall::ThieleSmall(const SpeakerParameters & p1, const SpeakerParameters & p2, float grAddedMass)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);
}

ThieleSmall::~ThieleSmall()
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
