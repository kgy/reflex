/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "measure-speaker-parameters.h"

#include <fft-base.h>

SpeakerParameters::SpeakerParameters(const FFT::logSpectrum & spectrum, unsigned int sampling_freq):
    sampling_freq(sampling_freq),
    resonance_freq(0.0f),
    resistance_at_DC(0.0f),
    resistance_at_resonance(0.0f),
    Q_at_resonance(0.0f)
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);

 unsigned int peak_position = spectrum.getPeakPosition(0);
 resonance_freq = spectrum.rGetZeroPhase(peak_position);

 resistance_at_resonance = exp10f(spectrum[0][peak_position]/20.0f) * cosf(spectrum[1][peak_position]);

 unsigned int pos_5Hz = (10U * spectrum.samples()) / sampling_freq;
 resistance_at_DC = exp10f(spectrum[0][pos_5Hz]/20.0f) * cosf(spectrum[1][pos_5Hz]);

 if (resistance_at_DC > 0.0f) {
    Q_at_resonance = resistance_at_resonance / resistance_at_DC;
 } else {
    DEBUG_OUT("WARNING: Zero D.C. resistance calculated");
 }
}

SpeakerParameters::~SpeakerParameters()
{
 SYS_DEBUG_MEMBER(DM_MEASURE_SPK);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
