/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_MEASUREMENT_MEASURE_H_INCLUDED__
#define __SRC_MEASUREMENT_MEASURE_H_INCLUDED__

#include <measure-interface.h>
#include <device-interface.h>
#include <generators/white-noise.h>
#include <generators/sinusoidal.h>
#include <filters/extended-filters.h>
#include <Threads/Threads.h>
#include <Threads/DataPipe.h>
#include <Memory/Memory.h>
#include <Debug/Debug.h>

#include <FLAC++/encoder.h>

#include <string>

SYS_DECLARE_MODULE(DM_MEASURE);
SYS_DECLARE_MODULE(DM_GENERATOR);

class Measure;

using MeasurePtr = MEM::shared_ptr<Measure>;

/// A generator and a recorder together
class MeasureBase
{
 protected:

    inline MeasureBase(void)
    {
        SYS_DEBUG_MEMBER(DM_MEASURE);
    }

    virtual ~MeasureBase()
    {
        SYS_DEBUG_MEMBER(DM_MEASURE);
    }

    class GeneratorBase: public SND::Player
    {
     public:
        GeneratorBase(MeasureBase & parent, SND::DeviceInfo::Parameters & dev);
        virtual ~GeneratorBase();

        inline void setOutputLevel(float level)
        {
            amplitude = level;
        }

     protected:
        virtual void except(const char * module, const char * reason) override
        {
            parent.except(module, reason);
        }

        MeasureBase & parent;

        SND::DeviceInfo::Parameters & dev;

        float amplitude;

     private:
        SYS_DEFINE_CLASS_NAME("MeasureBase::GeneratorBase");

        virtual void processorStopped(const std::string & name) override;
        virtual void deviceStopped(const std::string & name) override;

    }; // class MeasureBase::GeneratorBase

    friend class GeneratorBase;

    class FrameGeneratorBase: public GeneratorBase
    {
     public:
        inline FrameGeneratorBase(MeasureBase & parent, SND::DeviceInfo::Parameters & dev):
            GeneratorBase(parent, dev)
        {
            SYS_DEBUG_MEMBER(DM_MEASURE);
        }

     protected:
        virtual bool fillFrame(SND::SoundFrameBufferPtr & buf) override;

     private:
        SYS_DEFINE_CLASS_NAME("MeasureBase::FrameGeneratorBase");

        virtual bool fillFrame(float * frame, unsigned int size) =0;

    }; // class MeasureBase::GeneratorBase

    class NoiseGenerator: public FrameGeneratorBase, protected WhiteNoiseGenerator
    {
        using super = FrameGeneratorBase;

     public:
        NoiseGenerator(MeasureBase & parent, SND::DeviceInfo::Parameters & dev);
        virtual ~NoiseGenerator();

     protected:
        virtual bool fillFrame(float * frame, unsigned int size) override;

     private:
        SYS_DEFINE_CLASS_NAME("MeasureBase::NoiseGenerator");

    }; // class MeasureBase::NoiseGenerator

    class SweepGenerator: public FrameGeneratorBase, protected SinusoidalGenerator
    {
        using super = FrameGeneratorBase;

     public:
        SweepGenerator(MeasureBase & parent, SND::DeviceInfo::Parameters & dev);
        virtual ~SweepGenerator();

     protected:
        virtual bool fillFrame(float * frame, unsigned int size) override;

        float freq;

        float sweep;

        float limit;

     private:
        SYS_DEFINE_CLASS_NAME("MeasureBase::SweepGenerator");

    }; // class MeasureBase::SweepGenerator

    class RecorderBase
    {
     public:
        inline RecorderBase(MeasureBase & parent, SND::DeviceInfo::Parameters & dev):
            parent(parent),
            dev(dev)
        {
            SYS_DEBUG_MEMBER(DM_MEASURE);
        }

        virtual ~RecorderBase()
        {
            SYS_DEBUG_MEMBER(DM_MEASURE);
        }

        virtual void Start(void) =0;
        virtual void Stop(bool is_join) =0;
        virtual void Join(void) =0;

     protected:
        MeasureBase & parent;

        SND::DeviceInfo::Parameters & dev;

     private:
        SYS_DEFINE_CLASS_NAME("MeasureBase::RecorderBase");

    }; // class MeasureBase::RecorderBase

    friend class RecorderBase;

    class RecorderFromDevice: public SND::Recorder, public RecorderBase
    {
     protected:
        class FileSaver;
        friend class FileSaver;

     public:
        RecorderFromDevice(MeasureBase & parent, SND::DeviceInfo::Parameters & dev, const std::string & save_filename);
        virtual ~RecorderFromDevice();

     protected:
        using FileSaverPtr = MEM::shared_ptr<FileSaver>;

        class FileSaver: public Threads::Thread
        {
            friend class RecorderFromDevice;

         protected:
            FileSaver(RecorderFromDevice & parent, const std::string & filename, const PLOT::Params * params, unsigned int sampling_rate);

            static void Start(FileSaverPtr & s);
            void SaveFrame(SND::SoundFrameBufferPtr & buf);
            void save(SND::SoundFrameBufferPtr & buf);
            void open();
            void close();
            void Stop(bool is_join);

            virtual int main(void) override;
            virtual void error(std::exception * ex) override;

            RecorderFromDevice & parent;

            std::string filename;

            const PLOT::Params * params;

            unsigned int sampling_rate;

            unsigned int channels;

            Threads::DataPipe<SND::SoundFrameBufferPtr, 1000> data;

            FLAC::Encoder::File encoder;

         public:
            virtual ~FileSaver();

         private:
            SYS_DEFINE_CLASS_NAME("MeasureBase::RecorderFromDevice::FileSaver");

        }; // class MeasureBase::RecorderFromDevice::FileSaver

        void error(std::exception * ex);

        virtual bool frameCaptured(SND::SoundFrameBufferPtr & buf) override;
        virtual void except(const char * module, const char * reason) override;
        virtual void Stop(bool is_join) override;
        virtual void Start(void) override;
        virtual void Join(void) override;

        unsigned int frame_counter;

        FileSaverPtr saver;

     private:
        SYS_DEFINE_CLASS_NAME("MeasureBase::RecorderFromDevice");

        virtual void processorStopped(const std::string & name) override;
        virtual void deviceStopped(const std::string & name) override;

    }; // class MeasureBase::RecorderFromDevice

    class RecorderFromFile: public RecorderBase
    {
     public:
        RecorderFromFile(MeasureBase & parent, SND::DeviceInfo::Parameters & dev, const std::string & filename);
        virtual ~RecorderFromFile();

        inline bool frameCaptured(SND::SoundFrameBufferPtr & buf)
        {
            return parent.frameCaptured(buf);
        }

     protected:
        class FileLoader;
        friend class FileLoader;

        using FileLoaderPtr = MEM::shared_ptr<FileLoader>;

        inline void eof(void)
        {
            SYS_DEBUG_MEMBER(DM_MEASURE);
            parent.MeasureStopped();
        }

        class FileLoader: public Threads::Thread
        {
         public:
            FileLoader(RecorderFromFile & parent, const std::string & filename, const PLOT::Params * params, unsigned int & sampling_rate);

            static void Start(FileLoaderPtr & s);

         protected:
            class FlacDecoder;
            friend class FlacDecoder;

            bool got_new_frame(const FLAC__int32 * const * data, unsigned int channels, unsigned int samples, unsigned int sampling_rate);
            void flac_error(const char * error);

            virtual int main(void) override;
            virtual void exited(int) override;
            virtual void error(std::exception * ex) override;

            class FlacDecoder: public FLAC::Decoder::File
            {
                friend class FileLoader;

             protected:
                inline FlacDecoder(FileLoader & parent):
                    parent(parent)
                {
                    SYS_DEBUG_MEMBER(DM_MEASURE);
                }

                virtual FLAC__StreamDecoderWriteStatus write_callback(const FLAC__Frame * frame, const FLAC__int32 * const * data) override;
                virtual void error_callback(FLAC__StreamDecoderErrorStatus status) override;

                FileLoader & parent;

             private:
                SYS_DEFINE_CLASS_NAME("MeasureBase::RecorderFromFile::FileLoader::FlacDecoder");

            }; // class MeasureBase::RecorderFromFile::FileLoader::FlacDecoder

            RecorderFromFile & parent;

            std::string filename;

            const PLOT::Params * params;

            unsigned int & sampling_rate;

            unsigned int channels;

            SND::SoundFrameBufferPtr actual_frame;

            unsigned int actual_position;

            FlacDecoder decoder;

         private:
            SYS_DEFINE_CLASS_NAME("MeasureBase::RecorderFromFile::FileLoader");

        }; // class MeasureBase::RecorderFromFile::FileLoader

        bool got_new_frame(const FLAC__int32 * const * data, unsigned int channels, unsigned int samples);

        void error(std::exception * ex);
        virtual void Stop(bool is_join) override;
        virtual void Start(void) override;
        virtual void Join(void) override;

        FileLoaderPtr loader;

     private:
        SYS_DEFINE_CLASS_NAME("MeasureBase::RecorderFromFile");

    }; // class MeasureBase::RecorderFromFile

    virtual void GeneratorStopped(void)
    {
    }

    virtual void PlayerStopped(void)
    {
    }

    virtual void AnalyzerStopped(void)
    {
    }

    virtual void RecorderStopped(void)
    {
    }

    virtual void MeasureStopped(void) =0;
    virtual void MeasureStarted(void) =0;
    virtual bool frameCaptured(SND::SoundFrameBufferPtr &) =0;
    virtual const PLOT::Params * getPlotParameters(void) const =0;

    virtual void except(const char *, const char *)
    {
        // Can be overriden if necessary
    }

    using GPtr = MEM::shared_ptr<GeneratorBase>;
    using RPtr = MEM::shared_ptr<RecorderBase>;

 private:
    SYS_DEFINE_CLASS_NAME("MeasureBase");

}; // class MeasureBase

class Measure: public MeasureBase
{
 public:
    virtual ~Measure();

    void Start(void);
    void Stop(void);

    inline void setOutputLevel(float level)
    {
        if (generator) {
            generator->setOutputLevel(level);
        }
    }

 protected:
    inline Measure(MeasureInterface & caller):
        caller(caller)
    {
        SYS_DEBUG_MEMBER(DM_MEASURE);
    }

    MeasureInterface & caller;

    GPtr generator;

    RPtr recorder;

    void StopRecorder(void);
    void StopGenerator(bool immediate = false);

    virtual void GeneratorStopped(void) override;
    virtual void PlayerStopped(void) override;
    virtual void AnalyzerStopped(void) override;
    virtual void RecorderStopped(void) override;
    virtual void MeasureStopped(void) =0;

    virtual const PLOT::Params * getPlotParameters(void) const override
    {
        return caller.getPlotParameters();
    }

 private:
    SYS_DEFINE_CLASS_NAME("Measure");

}; // class Measure

#define ASSERT_FLAC_ENCODER_INIT(encoder, function, message) \
{ \
    ::FLAC__StreamEncoderInitStatus __status = encoder.function; \
    ASSERT_DBG(__status == ::FLAC__STREAM_ENCODER_INIT_STATUS_OK, message << ::FLAC__StreamEncoderInitStatusString[__status]); \
}

#define ASSERT_FLAC_DECODER_INIT(decoder, function, message) \
{ \
    ::FLAC__StreamDecoderInitStatus __status = decoder.function; \
    ASSERT_DBG(__status == ::FLAC__STREAM_DECODER_INIT_STATUS_OK, message << ::FLAC__StreamDecoderInitStatusString[__status]); \
}

#endif /* __SRC_MEASUREMENT_MEASURE_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
