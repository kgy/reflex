/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "filters.h"

#include <math.h>

using FILTER::Lowpass;

Lowpass::Lowpass(unsigned int size, unsigned int sampling_rate, double corner_frequency):
    FilterBase(size, sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 double multiplier = (double)sampling_rate / (double)(2*size);

 for (unsigned int i = 0; i < size; ++i) {
    double freq = (double)i * multiplier;
    double rate = freq / corner_frequency;
    (*this)[i] = -10.0 * log10(1.0 + rate*rate);
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
