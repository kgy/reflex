/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "filters.h"

#include <math.h>

using FILTER::Integrator;

Integrator::Integrator(unsigned int size, unsigned int sampling_rate, double corner_frequency):
    FilterBase(size, sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 data.get()[0] = 0.0;

 double multiplier = (double)sampling_rate / (double)(2*size);

 for (unsigned int i = 1; i < size; ++i) {
    double freq = (double)i * multiplier;
    (*this)[i] = 20.0 * log10(corner_frequency / freq);
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
