/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_FILTERS_EXTENDED_FILTERS_H_INCLUDED__
#define __SRC_FILTERS_EXTENDED_FILTERS_H_INCLUDED__

#include "filters.h"

namespace FILTER
{
    enum FilterTypes
    {
        FILTER_NONE,
        FILTER_PINK_NORMAL,
        FILTER_PINK_INVERTED,
        FILTER_RIAA_NORMAL,
        FILTER_RIAA_INVERTED,

        _FILTER_TYPE_SIZE
    };

    enum SmoothModes
    {
        SMOOTH_LIMIT = 10000,
        SMOOTH_OCTAVE_PER_30,
        SMOOTH_OCTAVE_PER_10,
        SMOOTH_OCTAVE_PER_3
    };

    enum AlignmentModes
    {
        ALIGNMENT_NONE,
        ALIGNMENT_1K,
        ALIGNMENT_PEAK,

        _ALIGNMENT_MODE_SIZE
    };

    class FilterHolder;

    using FilterHolderPtr = MEM::shared_ptr<FilterHolder>;

    class FilterHolder
    {
     protected:
        FilterHolder(unsigned int size, unsigned int sampling_rate);

     public:
        virtual ~FilterHolder();

        static inline FilterHolderPtr Create(unsigned int size, unsigned int sampling_rate)
        {
            return FilterHolderPtr(new FilterHolder(size, sampling_rate));
        }

        inline const FilterBase & operator[](unsigned int index) const
        {
            SYS_DEBUG_MEMBER(DM_FILTER);
            ASSERT_DBG(index < _FILTER_TYPE_SIZE, "filter index is out of range: " << index << ">=" << (int)_FILTER_TYPE_SIZE);
            return *filters[index];
        }

        inline unsigned int getSize(void) const
        {
            return filters[0]->getSize();
        }

        inline unsigned int getSamplingRate(void) const
        {
            return filters[0]->getSamplingRate();
        }

     protected:
        FilterBasePtr filters[_FILTER_TYPE_SIZE];

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::FilterHolder");

    }; // class FILTER::FilterHolder

    class Pink: public FilterBase
    {
     protected:
        Pink(unsigned int size, unsigned int sampling_rate, double corner_frequency);

     public:
        static inline FilterBasePtr Create(unsigned int size, unsigned int sampling_rate, double corner_frequency = 1e3)
        {
            return FilterBasePtr(new Pink(size, sampling_rate, corner_frequency));
        }

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::Pink");

    }; // class FILTER::Pink

    class RIAA: public FilterBase
    {
     protected:
        RIAA(unsigned int size, unsigned int sampling_rate, bool has_nyquist_freq = true, double normal_freq = 1e3);

     public:
        static inline FilterBasePtr Create(unsigned int size, unsigned int sampling_rate)
        {
            return FilterBasePtr(new RIAA(size, sampling_rate));
        }

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::RIAA");

    }; // class FILTER::RIAA

} // namespace FILTER

#endif /* __SRC_FILTERS_EXTENDED_FILTERS_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
