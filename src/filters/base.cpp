/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "filters.h"

#include <Exceptions/Exceptions.h>

#include <string.h>

using FILTER::FilterBase;
using FILTER::FilterBasePtr;

SYS_DEFINE_MODULE(DM_FILTER);

FilterBase::FilterBase(unsigned int size, unsigned int sampling_rate, double init_value):
    size(size),
    sampling_rate(sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 data.reset(new float[size]);

 for (unsigned int i = 0; i < size; ++i) {
    (*this)[i] = init_value;
 }
}

FilterBase::FilterBase(const FilterBase & other):
    FilterBase(other.size, other.sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 data.reset(new float[size]);
 memcpy(data.get(), other.data.get(), size);
}

FilterBase::FilterBase(FilterBase && other):
    FilterBase(other.size, other.sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 data.swap(other.data);
}

FilterBase & FilterBase::operator=(const FilterBase & other)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 size = other.size;
 sampling_rate = other.sampling_rate;
 data.reset(new float[size]);
 memcpy(data.get(), other.data.get(), size);

 return *this;
}

FilterBase & FilterBase::operator+=(const FilterBase & other)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 ASSERT_DBG(size == other.size, "filter sizes are differ");
 ASSERT_DBG(sampling_rate == other.sampling_rate, "filter sampling rates are differ");

 for (unsigned int i = 0; i < size; ++i) {
    (*this)[i] += other[i];
 }

 return *this;
}

FilterBase & FilterBase::operator-=(const FilterBase & other)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 ASSERT_DBG(size == other.size, "filter sizes are differ");
 ASSERT_DBG(sampling_rate == other.sampling_rate, "filter sampling rates are differ");

 for (unsigned int i = 0; i < size; ++i) {
    (*this)[i] -= other[i];
 }

 return *this;
}

FilterBasePtr FilterBase::operator+(const FilterBase & other)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 FilterBasePtr result(new FilterBase(*this));
 *result += other;
 return result;
}

FilterBasePtr FilterBase::operator-(const FilterBase & other)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 FilterBasePtr result(new FilterBase(*this));
 *result -= other;
 return result;
}

FilterBasePtr FilterBase::operator-()
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 FilterBasePtr result(new FilterBase(size, sampling_rate));
 *result -= *this;
 return result;
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
