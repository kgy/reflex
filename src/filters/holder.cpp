/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "extended-filters.h"

using FILTER::FilterHolder;

FilterHolder::FilterHolder(unsigned int size, unsigned int sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 filters[FILTER_NONE]           =   FilterBase::Create(size, sampling_rate);    // Linear filter (none)
 filters[FILTER_RIAA_NORMAL]    =         RIAA::Create(size, sampling_rate);    // RIAA
 filters[FILTER_RIAA_INVERTED]  =       -*RIAA::Create(size, sampling_rate);    // Inverted RIAA
 filters[FILTER_PINK_NORMAL]    =         Pink::Create(size, sampling_rate);    // Pink Noise
 filters[FILTER_PINK_INVERTED]  =       -*Pink::Create(size, sampling_rate);    // Inverted Pink Noise
}

FilterHolder::~FilterHolder()
{
 SYS_DEBUG_MEMBER(DM_FILTER);
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
