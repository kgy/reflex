/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "extended-filters.h"

using FILTER::RIAA;

static constexpr double F1 = 50.0;
static constexpr double F2 = 500.0;
static constexpr double F3 = 2120.0;
static constexpr double F4 = 50000.0;

RIAA::RIAA(unsigned int size, unsigned int sampling_rate, bool has_nyquist_freq, double normal_freq):
    FilterBase(size, sampling_rate)
{
 SYS_DEBUG_MEMBER(DM_FILTER);

 *this += *Lowpass::Create(size, sampling_rate, F1);
 *this -= *Lowpass::Create(size, sampling_rate, F2);
 *this += *Lowpass::Create(size, sampling_rate, F3);
 if (has_nyquist_freq) {
    *this -= *Lowpass::Create(size, sampling_rate, F4);
 }

 if (normal_freq <= 0.0 || normal_freq >= (double)(sampling_rate/2)) {
    SYS_DEBUG(DL_WARNING, "freq out of range: " << normal_freq << ", RIAA curve is not normalized");
    return;
 }

 int index = (int)(0.5 + normal_freq * ((double)size / (double)(sampling_rate/2)));
 if (index < 0 || (unsigned int)index > size) { // sanity check
    return;
 }
 double correction = (*this)[(unsigned int)index];
 for (unsigned int i = 0; i < size; ++i) {
    (*this)[i] -= correction;
 }
}

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
