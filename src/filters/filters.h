/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SRC_FILTERS_FILTERS_H_INCLUDED__
#define __SRC_FILTERS_FILTERS_H_INCLUDED__

#include <Memory/Memory.h>
#include <Debug/Debug.h>
#include <Exceptions/Exceptions.h>

SYS_DECLARE_MODULE(DM_FILTER);

namespace FILTER
{
    class FilterBase;

    using FilterBasePtr = MEM::shared_ptr<FilterBase>;

    class FilterBase
    {
     protected:
        FilterBase(unsigned int size, unsigned int sampling_rate, double init_value = 0.0);
        FilterBase(const FilterBase & other);
        FilterBase(FilterBase && other);

        unsigned int size;

        unsigned int sampling_rate;

        MEM::shared_ptr<float> data;

        inline float & operator[](unsigned int index)
        {
            ASSERT(index < size, "filter index is too high: " << index << ">=" << size);
            return data.get()[index];
        }

     public:
        static inline FilterBasePtr Create(unsigned int size, unsigned int sampling_rate)
        {
            return FilterBasePtr(new FilterBase(size, sampling_rate));
        }

        static inline FilterBasePtr Create(const FilterBase & other)
        {
            return FilterBasePtr(new FilterBase(other));
        }

        inline unsigned int getSize(void) const
        {
            return size;
        }

        inline unsigned int getSamplingRate(void) const
        {
            return sampling_rate;
        }

        inline float operator[](unsigned int index) const
        {
            ASSERT(index < size, "filter index is too high: " << index << ">=" << size);
            return data.get()[index];
        }

        FilterBase & operator=(const FilterBase & other);
        FilterBase & operator+=(const FilterBase & other);
        FilterBase & operator-=(const FilterBase & other);
        FilterBasePtr operator+(const FilterBase & other);
        FilterBasePtr operator-(const FilterBase & other);
        FilterBasePtr operator-();

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::FilterBase");

    }; // class FILTER::FilterBase

    class Differentiator: public FilterBase
    {
     protected:
        Differentiator(unsigned int size, unsigned int sampling_rate, double corner_frequency);

     public:
        static inline FilterBasePtr Create(unsigned int size, unsigned int sampling_rate, double corner_frequency)
        {
            return FilterBasePtr(new Differentiator(size, sampling_rate, corner_frequency));
        }

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::Differentiator");

    }; // class FILTER::Differentiator

    class Integrator: public FilterBase
    {
     protected:
        Integrator(unsigned int size, unsigned int sampling_rate, double corner_frequency);

     public:
        static inline FilterBasePtr Create(unsigned int size, unsigned int sampling_rate, double corner_frequency)
        {
            return FilterBasePtr(new Integrator(size, sampling_rate, corner_frequency));
        }

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::Integrator");

    }; // class FILTER::Integrator

    class Lowpass: public FilterBase
    {
     protected:
        Lowpass(unsigned int size, unsigned int sampling_rate, double corner_frequency);

     public:
        static inline FilterBasePtr Create(unsigned int size, unsigned int sampling_rate, double corner_frequency)
        {
            return FilterBasePtr(new Lowpass(size, sampling_rate, corner_frequency));
        }

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::Lowpass");

    }; // class FILTER::Lowpass

    class Highpass: public FilterBase
    {
     protected:
        Highpass(unsigned int size, unsigned int sampling_rate, double corner_frequency);

     public:
        static inline FilterBasePtr Create(unsigned int size, unsigned int sampling_rate, double corner_frequency)
        {
            return FilterBasePtr(new Highpass(size, sampling_rate, corner_frequency));
        }

     private:
        SYS_DEFINE_CLASS_NAME("FILTER::Highpass");

    }; // class FILTER::Highpass

} // namespace FILTER

#endif /* __SRC_FILTERS_FILTERS_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
