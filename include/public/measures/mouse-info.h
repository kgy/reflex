/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __INCLUDE_PUBLIC_MEASURES_MOUSE_INFO_H_INCLUDED__
#define __INCLUDE_PUBLIC_MEASURES_MOUSE_INFO_H_INCLUDED__

struct MouseInfo
{
    enum DrawSections
    {
        /// The section is not selected (undefined)
        DRAW_SECTION_UNDEFINED,

        /// Represents the amplitude scale (at left)
        DRAW_SECTION_AMPLITUDE,

        /// Represents the diagram area (at middle)
        DRAW_SECTION_DIAGRAMS,

        /// Represents the angle scale (at right)
        DRAW_SECTION_ANGLE

    }; // enum DrawSections

    inline MouseInfo(void):
        x(0),
        y(0),
        wheel(0),
        left(false),
        middle(false),
        right(false),
        section(DRAW_SECTION_UNDEFINED)
    {
    }

    /// The X position of the cursor (in pixels)
    int x;

    /// The Y position of the cursor (in pixels)
    int y;

    /// The current wheel action
    /*! It can have the following values:
     *  - <0:   Wheel scroll down
     *  - >0:   Wheel scroll up
     *  -  0:   No wheel action */
    int wheel;

    /// True if the left button is pressed
    bool left;

    /// True if the middle button is pressed
    bool middle;

    /// True if the right button is pressed
    bool right;

    /// Shows which section the cursor is in
    DrawSections section;

}; // struct MouseInfo

#endif /* __INCLUDE_PUBLIC_MEASURES_MOUSE_INFO_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
