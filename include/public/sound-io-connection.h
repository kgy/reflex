/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * Project:     The project Refleks
 * Purpose:     Measurements on speakers
 * Author:      Kövesdi György <kgy@etiner.hu>
 * License:     GPL (see file 'COPYING' in the project root for more details)
 * Comments:    
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef __SPEAKER_TEST_INCLUDE_PUBLIC_SOUND_IO_CONNECTION_H_INCLUDED__
#define __SPEAKER_TEST_INCLUDE_PUBLIC_SOUND_IO_CONNECTION_H_INCLUDED__

#include <iostream>

enum SoundIoConnection
{
    /// Unused
    SOUND_CONNECTION_UNUSED = 0,

    /// Generator output for measurement
    SOUND_OUT_SIGNAL,

    /// Feedback of the generator output
    SOUND_IN_FEEDBACK,

    /// The speaker voltage input
    SOUND_IN_SPEAKER_VOLTAGE,

    /// The speaker current input
    SOUND_IN_SPEAKER_CURRENT,

    /// Microphone input
    SOUND_IN_MICROPHONE,

    /// The output of the other amplifier channel
    SOUND_IN_CROSSTALK,

    /// The output of the amplifier to be measured
    SOUND_IN_AMPLIFIER_OUT,

    /// The number of states in this enum
    _SOUND_CONNECTION_SIZE
};

enum MeasurementTypes
{
    MEASUREMENT_SPEAKER_1,
    MEASUREMENT_SPEAKER_2,
    MEASUREMENT_AMPLIFIER,
    _MEASUREMENT_SIZE
};

static inline std::ostream & operator<<(std::ostream & os, SoundIoConnection conn)
{
 static const char * names[] = {
    "Unused",
    "Output",
    "Feedback",
    "Speaker Voltage",
    "Speaker Current",
    "Microphone",
    "CH2",
    "CH1"
 };

 if ((int)conn >= _SOUND_CONNECTION_SIZE) {
    os << "Unknown";
 } else {
    os << names[conn];
 }

 return os;
}

#endif /* __SPEAKER_TEST_INCLUDE_PUBLIC_SOUND_IO_CONNECTION_H_INCLUDED__ */

/* * * * * * * * * * * * * End - of - File * * * * * * * * * * * * * * */
